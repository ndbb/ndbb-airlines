import React from 'react';
import { Container, Row, Col, }from 'reactstrap';

export default function AuthenticatingLayout({ children }) {
    return (<div id='AuthenticatingLayout'>
        <Container>
            {children}
        </Container>
    </div>);
}
