import classNames from 'classnames';
import React, { useEffect, useRef } from 'react';

export default function Input(inputProps) {
    const {
        type = 'text',
        name,
        value,
        className,
        autoComplete,
        required,
        isFocused,
        handleChange,
        handleBlur,
        placeholder,
        isValid,
        invalidMsg,
        validMsg,
        label,
        errors,
    } = inputProps;

    const input = useRef();

    useEffect(() => {
        if (isFocused) {
            input.current.focus();
        }
    }, []);

    console.log(inputProps);
    return (<div className='form-group'>
        {(label) &&
        <label htmlFor={name}>label</label>
        }
        <input
            type={type}
            name={name}
            value={value}
            className={className || 'form-control'}
            ref={input}
            autoComplete={autoComplete}
            required={required}
            placeholder={placeholder}
            onChange={(e) => handleChange(e)}
            onBlur={handleBlur}
        />
        {(isValid)
            ? (<div className={classNames({ 'valid-feedback': isValid })}>{validMsg || 'Looks good!'}</div>)
            : (<div className={classNames({ 'invalid-feedback': !isValid })}>{invalidMsg || 'Pleace enter a valid input.'}</div>)
        }
    </div>);
}
