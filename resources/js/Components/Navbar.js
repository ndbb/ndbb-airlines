<Navbar bg={(isLight) ? 'light' : 'primary'} variant={(isLight) ? 'light' : 'dark'} expand="lg">
    <Container fluid>
        <Navbar.Brand>

        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Navigation menu={Menu} auth={auth} />
            <Nav className="justify-content-end">
                {(auth && auth.user && isAdmin) &&
                    <NavDropdown title={<FontAwesomeIcon icon={faCogs} />} id="admin-nav-dropdown">
                        <NavDropdown.Item href={route('users.list')} active={route().current('users.list')}>User Management</NavDropdown.Item>
                        <NavDropdown.Item href={route('config.show')} active={route().current('config.show')}>Config</NavDropdown.Item>
                    </NavDropdown>
                }
                {(auth && auth.user) &&
                    <NavDropdown title={`Hi ${auth.user.username}`} id="basic-nav-dropdown">
                        <NavDropdown.Item href={route('profile')}>My Profile</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href={route('logout')}>Logout</NavDropdown.Item>
                    </NavDropdown>
                }
                { (!auth.user) &&
                <>
                    <Nav.Link href={route('login')}>Login</Nav.Link>
                    <Nav.Link href={route('register')}>Register</Nav.Link>
                </>
                }
                    <Nav.Link onClick={changeColor} alt={`Switch to ${mode} mode`}>
                        <ColorSwitcher isLight={!isLight} />
                    </Nav.Link>
            </Nav>
        </Navbar.Collapse>
    </Container>
</Navbar>


