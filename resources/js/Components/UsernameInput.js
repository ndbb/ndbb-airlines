import React, { useState, useEffect, useRef } from 'react';

export default function UsernameInput({
    type = 'text',
    name,
    value,
    className,
    autoComplete,
    required,
    isFocused,
    onHandleChange,
}) {
    const input = useRef();
    const [ inputValue, setInputValue ] = useState(null);

    useEffect(() => {
        if (isFocused) {
            input.current.focus();
        }
    }, []);

    return (
        <div className="flex flex-col items-start">
            <input
                type={type}
                name={name}
                value={inputValue}
                className={
                    `border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm ` +
                    className
                }
                ref={input}
                autoComplete={autoComplete}
                required={required}
                onChange={(e) => setInputValue(e.target.value)}
            />
        </div>
    );
}
