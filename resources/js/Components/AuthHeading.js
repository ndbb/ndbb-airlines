import React from 'react'
import classNames from 'classnames';

export default function AuthHeading({ className, children }) {
    return (<div className={classNames('authHeading', className)}>
        <h4 className='h4 mb-3 font-weight-normal'>{children}</h4>
    </div>)
}
