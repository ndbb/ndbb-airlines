return (
    <table className='table table-striped'>
        <thead>
            <tr>
                <th><strong>ID</strong></th>
                <th><strong>TYPE</strong></th>
                <th><strong>POS</strong></th>
                <th><strong>STATUS</strong></th>
                <th><strong>MAX PAYLOAD</strong></th>
                <th><strong>PRACTICAL RANGE</strong></th>
                <th><strong>SPEED</strong></th>
                <th><strong>SEAT CFG</strong></th>
                <th><strong>AIRFRAME</strong></th>
                <th><strong>100H IN</strong></th>
                <th><strong></strong></th>
            </tr>
        </thead>
        {!data &&
        <tbody>
            <tr>
                <td colSpan='6'>
                    <p className='text-center'>No Aircraft</p>
                </td>
            </tr>
        </tbody>
        }
        {(data && data.length > 0) &&
        <tbody>
            {data.map((v, i) => (<tr key={i}>
                <td><a href={route('aircraft.detail', { id: v.id })}>{v.identifier}</a></td>
                <td>{(v.aircraft_type.aircraft_class) && v.aircraft_type.aircraft_class.short_name}</td>
                <td className='text-center'>
                    <CurrentAirport airport={v.current_airport} />
                </td>
                <td className='text-center'>
                    <AircraftStatus status={v.status} />
                </td>
                <td>MAX PAYLOAD</td>
                <td>PRACTICAL RANGE</td>
                <td>{(v.aircraft_type) && (<span>{`${v.aircraft_type.design_speed_vc} Kts`}</span>)}</td>
                <td>SEAT CFG</td>
                <td>{v.airframe_hours}</td>
                <td>{v.hours_before_100h_inspection}</td>
                <td></td>
            </tr>))}
        </tbody>
        }
    </table>
)
}
