import React from 'react'
import {
    Card,
    CardColumns,
    Row,
    Col,
} from 'react-bootstrap'

export default function CardList({ aircraft }) {
    let count = 0;
    const cardWidth = 4;

    return (<>
        <Row>
            <CardColumns style={{flexDirection: 'row', display: 'flex'}}>
                {aircraft.map((a, k) => {
                    count++;
                    return (
                        <Card key={k} style={{flex: 1}}>
                            <Card.Img variant='top' src={(a.img) ? a.img : placeholderImg} />
                            <Card.Body>
                                <Card.Title>{a.identifier}</Card.Title>
                            </Card.Body>
                        </Card>
                    );
                })}
            </CardColumns>

    }
    </>)
}
