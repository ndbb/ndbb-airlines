import React from 'react';
import { useTable, } from 'react-table';
import {
    Badge, ButtonGroup,
}from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faCheckSquare, faPlane, faPlaneDeparture, faSpellCheck } from '@fortawesome/free-solid-svg-icons';
import { Table, } from '@Components';

const AircraftStatus = ({ status, pill = false }) => {
    let variant = 'light';

    switch (status.id) {
        default:
        case 0:
            variant = 'success';
        break;
        case 1:
            variant = 'danger';
        break;
        case 2:
            variant = 'warning';
        break;
        case 3:
            variant = 'info';
        break;
    }

    return (<Badge pill={pill} variant={variant}>
        {status.name}
    </Badge>)
}

const CurrentAirport = ({ airport }) => {
    if (airport) {
        return (<span>{airport.icao}</span>);
    } else {
        return (<span className='text-muted'>N/A</span>)
    }
}


export default function AircraftTable({ data, DoUpdateNickName, }) {

    const columns = React.useMemo(
        () => [
            {
                Header: 'ID',
                id: 'identifier',
                sortType: 'alphanumeric',
                isSorted: true,
                Cell: ({ row, }) => (<span>
                    <a href={route('aircraft.detail', { id: row.original.id })}>{row.original.identifier}</a>
                </span>)
            },
            {
                Header: 'Nickname',
                id: 'nickname',
                sortType: 'alphanumeric',
                isSorted: true,
                className: 'text-center',
                Cell: ({ row, }) => (<span>
                    {(row.original.nickname)
                    ? row.original.nickname
                    : 'Click me to set a nickname!'
                    }
                </span>)
            },
            {
                Header: 'TYPE',
                id: 'type',
                sortType: 'alphanumeric',
                isSorted: true,
                className: 'text-center',
                Cell: ({ row, }) => (<span>
                    {row.original.aircraft_type.aircraft_class.short_name}
                </span>)
            },
            {
                Header: 'POS',
                id: 'current_airport',
                sortType: 'alphanumeric',
                isSorted: true,
                Cell: ({ row, }) => (<span>
                    <CurrentAirport airport={row.original.current_airport} />
                </span>)
            },
            {
                Header: 'STATUS',
                id: 'status',
                sortType: 'alphanumeric',
                isSorted: true,
                className: 'text-center',
                Cell: ({ row, }) => (<span>
                    <AircraftStatus status={row.original.status} />
                </span>)
            },
            {
                Header: 'MAX PAYLOAD',
                id: 'max-payload',
                sortType: 'alphanumeric',
                isSorted: true,
                Cell: ({ row, }) => (<span>
                    {`${row.original.aircraft_type.computed_max_payload} lbs`}
                </span>)
            },
            {
                Header: 'MAX RANGE',
                id: 'max-range',
                sortType: 'alphanumeric',
                isSorted: true,
                Cell: ({ row, }) => (<span>
                    {`${row.original.aircraft_type.maximum_range_in_nm} NM`}
                </span>)
            },
            {
                Header: 'MAX SPEED',
                id: 'speed',
                sortType: 'alphanumeric',
                isSorted: true,
                Cell: ({ row, }) => (<span>
                    {`${row.original.aircraft_type.design_speed_vc} kts`}
                </span>)
            },
            {
                Header: 'SEATS',
                id: 'seats',
                sortType: 'alphanumeric',
                isSorted: true,
                headerClassName: 'text-center',
                className: 'text-center',
                Cell: ({ row, }) => (<span>
                    {(row.original.config_seats_eco) ? (<span>{row.original.config_seats_eco}</span>) : (<span>0</span>)}/
                    {(row.original.config_seats_bus) ? (<span>{row.original.config_seats_bus}</span>) : (<span>0</span>)}/
                    {(row.original.config_seats_first) ? (<span>{row.original.config_seats_first}</span>) : (<span>0</span>)}
                </span>)
            },
            {
                Header: 'AIRFRAME TIME',
                id: 'airframe-time',
                sortType: 'alphanumeric',
                isSorted: true,
                headerClassName: 'text-center',
                className: 'text-center',
                Cell: ({ row, }) => (<span>
                    {`${row.original.airframe_hours} hrs`}
                </span>)
            },
            {
                Header: '100H IN',
                id: 'hours_before_100h_inspection',
                sortType: 'alphanumeric',
                isSorted: true,
                headerClassName: 'text-center',
                className: 'text-center',
                Cell: ({ row, }) => (<span>
                    {`${row.original.hours_before_100h_inspection} hrs`}
                </span>)
            },
            {
                Header: '',
                id: 'actiosn',
                sortType: 'alphanumeric',
                isSorted: true,
                className: 'text-center',
                Cell: ({ row, }) => {
                    console.log(row);
                    return (<span>
                        <ButtonGroup>
                            <a href={route('flight.log', {
                                worldSlug: row.original.company.world.slug,
                                companyId: row.original.company.uuid,
                                identifier: row.original.identifier
                            })} className='btn btn-sm btn-info'>
                                <FontAwesomeIcon icon={faPlaneDeparture} />
                            </a>
                        </ButtonGroup>
                    </span>)
                }
            }
        ]
    );

    return (<>
        {(data && data.length > 0)
            ? <Table data={data} columns={columns} />
            : <p>No Content to display</p>
        }
    </>)
}
