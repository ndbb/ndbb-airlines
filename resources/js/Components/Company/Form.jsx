import React, { useState, } from 'react'
import { Formik, Form, Field } from 'formik';
import {
    Button,
    Row,
    Col,
    FormText,
    Label,
    FormGroup,
    Input,
    CustomInput,
} from 'reactstrap';
import axios from 'axios';

import FormComponents from '@Components/Form';
import { Company } from '..';

function validateCompanyUuid(value) {
    let error;
    if (!value) {
        error = 'Required';
    }

    return error;
}

function validateCompanyApiKey(value) {
    let error;
    if (!value) {
        error = 'Required';
    }

    return error;
}

const initialValues = {
    uuid: 'bdc8e9d3-2ba4-4a32-9f6c-0f63ebe6c5e4',
    api_key: 'a7ef356d-c0cc-461e-94cf-941650bd81cd',
    world: 'stratus',
};

const initialState = {
    details_fetched: false,
    uuid: 'bdc8e9d3-2ba4-4a32-9f6c-0f63ebe6c5e4',
    api_key: 'a7ef356d-c0cc-461e-94cf-941650bd81cd',
    world: 'stratus',
    company_name: '',
    airline: '',
    level: '',
    xp: '',
    sync_data: {
        company: false,
        fleet: false,
        flights: false,
        employees: false,
        fbos: false,
    },
};

export default function CompanyCreateForm({ handleCancel, handleSubmit, worlds, ...props }) {
    const [state, setState] = useState(initialState)

    const {
        details_fetched,
        uuid,
        api_key,
        company_name,
        world,
        airline,
        level,
        xp,
        sync_data,
    } = state;

    const lookupCompanyDetails = () => {
        console.log('lookupCompanyDetails', state);
        if (uuid.length > 0 && api_key.length > 0 && world.length > 0) {
            axios({
                url: '/company/lookup',
                method: 'POST',
                data: {
                    api_key,
                    uuid,
                    world,
                }
            })
            .then(({ data }) => {
                const {
                    airline,
                    name,
                    level,
                    xp,
                } = data;

                setState({
                    ...state,
                    details_fetched: true,
                    airline: airline,
                    company_name: name,
                    level: level,
                    xp: xp,
                })
            })
            .catch((err) => {
                console.error(err);
            })
        }
    }

    const storeCompanyUuid = (e) => {
        const value = e.target.value;

        setState({
            ...state,
            uuid: value,
        })
    }

    const storeCompanyApiKey = (e) => {
        const value = e.target.value;

        setState({
            ...state,
            api_key: value,
        })
    }

    const storeCompanyWorld = (e) => {
        const value = e.target.value;

        setState({
            ...state,
            world: value,
        })
    }


    const toggleSyncData = (e) => {
        const id = event.target.id.split('_').pop();
        let newSyncData = Object.assign({}, state.sync_data);

        newSyncData = {
            ...newSyncData,
            [id]: !state.sync_data[id]
        };

        setState({
            ...state,
            sync_data: newSyncData,
        })
    }

    const doSubmit = (d) => {

        handleSubmit({
            uuid,
            api_key,
            world,
            sync_company: sync_data.company,
            sync_fleet: sync_data.fleet,
            sync_flights: sync_data.flights,
            sync_employees: sync_data.employees,
            sync_fbos: sync_data.fbos,
        });
    }

    return (
        <Formik
            initialValues={initialValues}
            onSubmit={doSubmit}
        >
            {({ errors, touched, isValidating, ...formProps }) => {
                return (
                    <Form>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Field
                                        type='text'
                                        name='uuid'
                                        id='uuid'
                                        label='New Company UUID'
                                        validate={validateCompanyUuid}
                                        validmsg='Looks good!'
                                        helptext='Viewable within the OnAir Global Settings'
                                        component={FormComponents.Input}
                                        onBlur={storeCompanyUuid}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Field
                                        type='text'
                                        name='api_key'
                                        id='api_key'
                                        label='New Company API Key'
                                        validate={validateCompanyApiKey}
                                        validmsg='Looks good!'
                                        helptext='Viewable within the OnAir Global Settings'
                                        component={FormComponents.Input}
                                        onBlur={storeCompanyApiKey}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Field
                                        name='world'
                                        id='world'
                                        label='Company World'
                                        options={worlds}
                                        component={FormComponents.Select}
                                        onChange={storeCompanyWorld}
                                        value={world}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <hr/>
                            </Col>
                        </Row>
                        {(!details_fetched) &&
                        <Row>
                            <Col>
                                <Button color='info' block onClick={lookupCompanyDetails} disabled={(world.length <= 0 || api_key.length <= 0 || uuid.length <= 0)}>Load Company Details</Button>
                            </Col>
                        </Row>
                        }
                        {(details_fetched) &&
                        <>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label>Company Name</Label>
                                    <Input
                                        name='company_name'
                                        id='company_name'
                                        value={company_name}
                                        disabled
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={2}>
                                <FormGroup>
                                    <Label>Identifier</Label>
                                    <Input
                                        name='airline'
                                        id='airline'
                                        value={airline}
                                        disabled
                                    />
                                </FormGroup>
                            </Col>
                            <Col md={2}>
                                <FormGroup>
                                    <Label> XP</Label>
                                    <Input
                                        name='xp'
                                        id='xp'
                                        value={xp}
                                        disabled
                                    />
                                </FormGroup>
                            </Col>
                            <Col md={2}>
                                <FormGroup>
                                    <Label>Level</Label>
                                    <Input
                                        name='level'
                                        id='level'
                                        value={level}
                                        disabled
                                    />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <Label>OnAir Synchronization</Label>
                                <CustomInput type="switch" id="sync_company" name="sync_company" label="Enable Company Synchronization" value={sync_data.company} onChange={toggleSyncData} />
                                <CustomInput type="switch" id="sync_fleet" name="sync_fleet" label="Enable Fleet Synchronization" value={sync_data.fleet} onChange={toggleSyncData} />
                                <CustomInput type="switch" id="sync_flights" name="sync_flights" label="Enable Flight Synchronization" value={sync_data.flights} onChange={toggleSyncData} />
                                <CustomInput type="switch" id="sync_employees" name="sync_employees" label="Enable Employee Synchronization" value={sync_data.employees} onChange={toggleSyncData} />
                                <CustomInput type="switch" id="sync_fbos" name="sync_fbos" label="Enable FBO Synchronization" value={sync_data.fbos} onChange={toggleSyncData} />
                            </Col>
                        </Row>
                        </>
                        }
                        <Row>
                            <Col>
                                <hr/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button color='secondary' onClick={handleCancel}>Cancel</Button>
                                <Button color='success' onClick={doSubmit} disabled={details_fetched === false}>Save</Button>
                            </Col>
                        </Row>
                    </Form>
                );
            }}
        </Formik>
    )
}
