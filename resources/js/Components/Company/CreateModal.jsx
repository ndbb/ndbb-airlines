import React, { useState, } from 'react'
import {
    Row,
    Col,
    Modal,
    ModalHeader,
    ModalBody,
    Button,
} from 'reactstrap'
import { Company, } from '@Components';

export default function CreateModal({ handleSubmit, worlds, ...props }) {
    const initialState = {
        isOpen: false,
        uuid: '',
        api_key: '',
    };

    const [state, setState] = useState(initialState);

    const {
        isOpen,
        uuid,
        api_key
    } = state;

    const toggle = () => {
        console.log('toggleCreateModal');
        setState({
            ...state,
            isOpen: !isOpen,
        })
    }

    const doSubmit = (data) => {
        handleSubmit(data);
        toggle();
    }

    return (<>
        <Button color='primary' onClick={toggle}>Add Company</Button>
        <Modal size='lg' toggle={toggle} isOpen={isOpen}>
            <ModalHeader toggle={toggle}>
                Add New Company
            </ModalHeader>
            <ModalBody>
            <Company.Form
                worlds={worlds}
                handleCancel={toggle}
                handleSubmit={doSubmit} />
            </ModalBody>
        </Modal>
    </>)
}
