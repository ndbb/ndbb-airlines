import TableComponent from './Table';
import CreateModalComponent from './CreateModal';
import FormComponent from './Form';

export const Table = TableComponent;
export const CreateModal =  CreateModalComponent;
export const Form = FormComponent;

export default {
    Table,
    CreateModal,
    Form,
}
