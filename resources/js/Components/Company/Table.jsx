import React from 'react';
import { useTable, } from 'react-table';
import {
    Badge, ButtonGroup,
}from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faCheckSquare, faPlane, faPlaneDeparture, faSpellCheck } from '@fortawesome/free-solid-svg-icons';
import { Table, } from '@Components';

const CompanyStatus = ({ status, pill = false }) => {
    let variant = 'light';

    switch (status.id) {
        default:
        case 0:
            variant = 'success';
        break;
        case 1:
            variant = 'danger';
        break;
        case 2:
            variant = 'warning';
        break;
        case 3:
            variant = 'info';
        break;
    }

    return (<Badge pill={pill} variant={variant}>
        {status.name}
    </Badge>)
}


export default function CompanyTable({ data, }) {

    const columns = React.useMemo(
        () => [
            {
                Header: 'ID',
                accessor: 'airline',
                sortType: 'alphanumeric',
                isSorted: true,
            },
            {
                Header: 'Name',
                accessor: 'name',
                sortType: 'alphanumeric',
                isSorted: true,
            },
            {
                Header: 'World',
                id: 'world',
                sortType: 'alphanumeric',
                isSorted: true,
                className: 'text-center',
                Cell: ({ row, }) => {
                    const {
                        name,
                        slug,
                    } = row.original.world;

                    let color = 'secondary';

                    switch (slug) {
                        case 'thunder':
                            color = 'danger'
                        break;
                        case 'clear-sky':
                            color = 'success'
                        break;
                        case 'cumulus':
                            color = 'info';
                        break;
                        case 'stratus':
                            color = 'warning'
                        break;
                    }

                    return (<Badge color={color}>{name}</Badge>)
                }
            },
            {
                Header: '',
                id: 'actions',
                sortType: 'alphanumeric',
                isSorted: true,
                className: 'text-center',
                Cell: ({ row, }) => {
                    return (<span>
                    <ButtonGroup>
                        <a href={route('aircraft.list.by-company', { companyId: row.original.uuid })} className='btn btn-sm btn-info'>
                            <FontAwesomeIcon icon={faPlaneDeparture} />
                        </a>
                    </ButtonGroup>
                </span>)
                }
            }
        ]
    );
    console.log('data', data);
    return (<>
        {(data && data.length > 0)
            ? <Table data={data} columns={columns} />
            : <p>No Content to display</p>
        }
    </>)
}
