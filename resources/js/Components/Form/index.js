import InputComponent from './Input';
import SelectComponent from './Select';

export const Input = InputComponent;
export const Select = SelectComponent;

export default {
    Input,
    Select,
}
