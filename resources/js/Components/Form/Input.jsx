import React from 'react'
import {
    Label,
    Input,
    FormText,
} from 'reactstrap';

export default function InputComponent(props) {
    const {
        field, // { name, value, onChange, onBlur }
        form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
        label,
        id,
        name,
        type,
        placeholder,
        validmsg,
        helptext,
    } = props;

    return (
        <div>
            {label &&
            <Label>{label}</Label>
            }
            <Input
                type={type || 'text'}
                name={(field) ? field.name : name}
                id={(field) ? field.id : id}
                placeholder={placeholder}
                {...field}
                {...props}
            />
            {touched &&
            <>
                {(touched[(field) ? field.name : name] && errors[(field) ? field.name : name]) && <FormText color='danger'>{errors[(field) ? field.name : name]}</FormText>}
                {(touched[(field) ? field.name : name] && !errors[(field) ? field.name : name]) && <FormText color='success'>{validmsg || 'Looks Good!'}</FormText>}
            </>
            }
            {helptext && <FormText color='muted'>{helptext}</FormText>}
        </div>
    )
}
