import React from 'react';

export default function Label({ forInput, value, className, children }) {
    return (
        <label htmlFor={forInput} className='sr-only'>
            {value ? value : { children }}
        </label>
    );
}
