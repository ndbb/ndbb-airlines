import HeaderComponent from './Header';
import NavigationComponent from './Navigation';
import LogoComponent from './Logo';
import ColorSwitcherComponent from './ColorSwitcher';
import LabelComponent from './Label';
import InputComponent from './Input';
import ValidationErrorsComponent from './ValidationErrors';
import AuthHeadingComponent from './AuthHeading';
import CheckboxComponent from './Checkbox';
import FlightsComponents from './Flights';
import AircraftComponents from './Aircraft';
import CompanyComponents from './Company';
import TableComponent from './Table';

export const Header = HeaderComponent;
export const Navigation = NavigationComponent;
export const Logo = LogoComponent;
export const ColorSwitcher = ColorSwitcherComponent;
export const Label = LabelComponent;
export const Input = InputComponent;
export const ValidationErrors = ValidationErrorsComponent;
export const AuthHeading = AuthHeadingComponent;
export const Checkbox = CheckboxComponent;
export const Aircraft = AircraftComponents;
export const Company = CompanyComponents;
export const Flights = FlightsComponents;
export const Table = TableComponent;

export default {
    Header,
    Navigation,
    Logo,
    ColorSwitcher,
    Label,
    Input,
    ValidationErrors,
    AuthHeading,
    Checkbox,
    Aircraft,
    Company,
    Flights,
    Table,
}
