import React from 'react';
import Table from 'react-table';

export default function FlightTable({ data, }) {
    return (
        <table className='table table-striped'>
            <thead>
                <tr>
                    <th>AIRCRAFT CODE</th>
                    <th>DEPARTURE AIRPORT</th>
                    <th>PLANNED DEST AIRPORT</th>
                    <th>ACT ARRIVAL AIRPORT</th>
                    <th>FLIGHT STARTED AT</th>
                    <th>FLIGHT UPDATED AT</th>
                </tr>
            </thead>
            <tbody>
                {data.map((v, k) => {
                    console.log({ k, v });
                    return (<tr key={k}>
                        <td>
                            {(v.aircraft) ? v.aircraft.identifier : 'N/A'}
                        </td>
                        <td>{(v.departure_airport) ? v.departure_airport.icao : 'N/A'}</td>
                        <td>{(v.arrival_intended_airport) ? v.arrival_intended_airport.icao : 'N/A'}</td>
                        <td>{(v.arrival_intended_airport) ? v.arrival_intended_airport.icao : 'N/A'}</td>
                        <td>{(v.arrival_actual_airport) ? v.arrival_actual_airport.icao : 'N/A'}</td>
                        <td><p>foo</p></td>
                    </tr>)
                })}
            </tbody>
        </table>
    )
}
