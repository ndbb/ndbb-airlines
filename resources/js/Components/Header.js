import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCogs, } from '@fortawesome/free-solid-svg-icons';
import { Logo, ColorSwitcher, Navigation, } from '@Components';
import { Menu, AdminMenu } from '@Menu';

export default function Header({ id, auth, logoText, isAdmin, currentRoute }) {
    const initialState = {
        isLight: false,
        navIsOpen: false,
    };
    const savedState = JSON.parse(localStorage.getItem('state'));
    const [state, setState] = useState(savedState || initialState);

    const changeColor = () => {
        const {
            isLight,
            navIsOpen,
        } = state;

        const newState = Object.assign({}, state, {
            isLight: !isLight,
        });

        setState(newState);
    }

    const {
        isLight,
        navIsOpen,
    } = state;

    const toggleNav = () => setState({
        ...state,
        navIsOpen: !navIsOpen
    });

    useEffect(() => {
        localStorage.setItem('state',  JSON.stringify(state));
    }, [state]);

    const mode = (isLight) ? 'Light' : 'Dark';

    return (<header id={id}>
        <Navbar color="light" light expand="md">
            <NavbarBrand>
                <Logo light={isLight} height={40} />
                {(logoText) &&
                    <span className='logoText'>{` ${logoText}`}</span>
                }
            </NavbarBrand>
            <NavbarToggler onClick={toggleNav} />
            <Collapse isOpen={navIsOpen} navbar>
                <Navigation menu={Menu} auth={auth} />
                <Nav className="justify-content-end">
                    {(auth && auth.user && isAdmin) &&
                        <UncontrolledDropdown id="admin-nav-dropdown">
                            <DropdownToggle nav caret>
                                <FontAwesomeIcon icon={faCogs} />
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem href={route('users.list')} active={route().current('users.list')}>User Management</DropdownItem>
                                <DropdownItem href={route('config.show')} active={route().current('config.show')}>Config</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    }
                    {(auth && auth.user) &&
                        <UncontrolledDropdown id="basic-nav-dropdown">
                            <DropdownToggle nav caret>
                                {`Hi ${auth.user.username}`}
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem href={route('profile')}>My Profile</DropdownItem>
                                <DropdownItem divider />
                                <DropdownItem href={route('logout')}>Logout</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    }
                    { (!auth.user) &&
                    <>
                        <NavItem>
                            <NavLink href={route('login')}>Login</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href={route('register')}>Register</NavLink>
                        </NavItem>
                    </>
                    }
                        <NavItem>
                            <NavLink onClick={changeColor} alt={`Switch to ${mode} mode`}>
                                <ColorSwitcher isLight={!isLight} />
                            </NavLink>
                        </NavItem>
                </Nav>
            </Collapse>
        </Navbar>
    </header>);
}

Header.propTypes = {};
