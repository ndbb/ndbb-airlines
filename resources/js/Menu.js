export const Menu = [
    {
        label: 'Dashboard',
        name: 'dashboard',
        href: '/dashboard'
    },
    {
        label: 'Flights',
        name: 'flights',
        href: '/flight'
    },
    {
        label: 'Fleet',
        name: 'aircrafts',
        href: '/aircraft'
    },
    {
        label: 'Company',
        name: 'company.show',
        href: '/company'
    },
    {
        label: 'Employees',
        name: 'employees',
        href: '/employees'
    },

];

export const AdminMenu = [
    {
        label: 'Config',
        name: 'config.show',
        href: '/config',
    },
    {
        label: 'Users',
        name: 'users',
        href: '/users'
    },
];

export default {
    Menu,
    AdminMenu,
}
