import React from 'react';
import { Row, Col, }from 'reactstrap';
import Layouts from '@Layouts';

export default function UserProfile({ auth, isAdmin, errors, pageTitle, appTitle, user, ...props }) {
    return (
        <Layouts.Authenticated
            auth={auth}
            errors={errors}
            pageTitle={pageTitle}
            appTitle={appTitle}
            isAdmin={isAdmin}
        >
            <Row>
                <Col>Profile</Col>
            </Row>
        </Layouts.Authenticated>
    );
}
