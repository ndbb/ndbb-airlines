import React, { useState, useEffect } from 'react';
import { Inertia } from '@inertiajs/inertia';
import classNames from 'classnames';
import { Row, Col, Button, ButtonGroup, Modal, Form, ModalHeader, FormGroup, Label, Input, FormText, ModalBody, ModalFooter, } from 'reactstrap';
import Layouts from '@Layouts';
import { Company, } from '@Components';


export default function List({ auth, isAdmin, pageTitle, config, statistics, heading, companies, appTitle, worlds, ...props }) {

    const handleCreateCompany = (data) => {
        console.log('handleCreateCompany()', data);
        Inertia.post('/company/create', data);
    }

    return (
        <Layouts.Authenticated
            auth={auth}
            pageTitle={pageTitle}
            appTitle={appTitle}
            isAdmin={isAdmin}
            heading={heading}
        >

        {(companies && companies.length > 0) &&
            <Company.Table data={companies} />
        }

        <Company.CreateModal
            worlds={worlds}
            handleSubmit={handleCreateCompany}
        />
        </Layouts.Authenticated>
    );
}
