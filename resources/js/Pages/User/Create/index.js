import React, { useState, } from 'react'
import { Container, Row, Col, Alert, Button, Modal, }from 'reactstrap';
import classNames from 'classnames';
import axios from 'axios';
import { Formik, Form, Field, ErrorMessage, } from 'formik';
import Layouts from '@Layouts';
import { Label, Logo, ValidationErrors, Checkbox, AuthHeading } from '@Components';
import { faLessThanEqual } from '@fortawesome/free-solid-svg-icons';
const emailRegEx = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

export default function CreateUser({ config, auth, isAdmin, pageTitle, roles, appTitle, ...props }) {
    const initialState = {
        username: '',
        isValidatingUsername: false,
        isAvailable: undefined,
        wasChecked: false,
    };

    const [state, setState] = useState(initialState);

    const {
        isVisible,
        username,
        isAvailable,
        wasChecked,
    } = state;

    const handleSubmit =  () => {
        console.log('handleSubmit()');
    }

    const handleCancel = () => {
        console.log('handleCancel()');
    }

    const usernameIsUnique = async (username) => {
        await axios.post('/users/checkusername', {
            headers: {
                'X-CSRF-TOKEN': document.getElementsByName('csrf-token')[0].content
            },
            body: {
                username: username
            }
        })
        .then(({ data: { isAvailable }}) => {
            console.log(isAvailable);
            return isAvailable;
        })
        .catch((err) => {
            console.error(err);
        })
    }

    const validateUsername = (value) => {
        let error;
        if (!value || value.length < 4) {
            error = 'Username is required to be at least 3 characters long'
        } else {
            console.log(value);
            setState({
                ...state,
                isValidatingUsername: true,
            });
            axios.post('/users/checkusername', {
                headers: {
                    'X-CSRF-TOKEN': document.getElementsByName('csrf-token')[0].content
                },
                body: {
                    username: username
                }
            })
            .then(({ data: { isAvailable }}) => {
                if (isAvailable === true) {
                    setState({
                            isValidatingUsername: false,
                            wasChecked: true,
                            isAvailable: isAvailable,
                    });
                } else {
                    error = 'That username is already taken. Please pick a new one.';
                }
            })
            .catch((err) => {
                console.error(err);
            })

        }

        return error;
    }

    return (
        <Layouts.Authenticated
            auth={auth}
            pageTitle={pageTitle}
            appTitle={appTitle}
            isAdmin={isAdmin}
        >
            <Formik
                validateOnBlur
                onSubmit={handleSubmit}
                initialValues={{
                    name: '',
                    email: '',
                    username: '',
                }}
            >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    ...formikProps
                }) => {
                    const errorsExist = (Object.keys(errors).length > 0);
                    const isTouched = (Object.keys(touched).length > 0);
                    // console.log('formikProps', { values, errors, touched, isSubmitting, ...formikProps}, { errorsExist, isTouched, })

                    return (
                        <Form>
                            <div className='form-group'>
                                <label htmlFor='name'>Name</label>
                                <Field
                                    name='name'
                                    type='text'
                                    id='name'
                                    className={classNames(
                                        'form-control',
                                        {
                                            'is-invalid': (errors.name && touched.name),
                                            'is-valid': (!errors.name && touched.name),
                                        }
                                    )}
                                    validate={(value) => {
                                        let error;
                                        if (!value || value.length < 3) {
                                            error = 'Name is required.'
                                        }

                                        return error;
                                    }}
                                />
                                {(errors.name && touched.name) && (<div className='invalid-feedback'>{errors.name}</div>)}
                                {(!errors.name && touched.name) && (<div className='valid-feedback'>Looks good!</div>)}
                            </div>
                            <div className='form-group'>
                                <label htmlFor='email'>Email Address</label>
                                <Field
                                    name='email'
                                    type='text'
                                    id='email'
                                    className={classNames(
                                        'form-control',
                                        {
                                            'is-invalid': (errors.email && touched.email),
                                            'is-valid': (!errors.email && touched.email),
                                        }
                                    )}
                                    validate={(value) => {
                                        let error;
                                        if (!value || value.length < 3) {
                                            error = 'Email is required.'
                                        } else if (!emailRegEx.test(value)) {
                                            error = 'A Valid Email is required.'
                                        }
                                        return error;
                                    }}
                                />
                                {(errors.email && touched.email) && (<div className='invalid-feedback'>{errors.email}</div>)}
                                {(!errors.email && touched.email) && (<div className='valid-feedback'>Looks good!</div>)}
                            </div>
                            <div className='form-group'>
                                <label htmlFor='username'>Username</label>
                                <Field
                                    name='username'
                                    type='text'
                                    id='username'
                                    className={classNames(
                                        'form-control',
                                        {
                                            'is-invalid': (errors.username && touched.username),
                                            'is-valid': (!errors.username && touched.username),
                                        }
                                    )}
                                    validate={validateUsername}
                                />
                                {(errors.username && touched.username) && (<div className='invalid-feedback'>{errors.username}</div>)}
                                {(!errors.username && touched.username) && (<div className='valid-feedback'>Looks good!</div>)}
                            </div>
                            <Col>
                                <Row>
                                    <hr />
                                </Row>
                            </Col>
                            <div className='btn-group'>
                                <a href={route('users.list')} className='btn btn-secondary'>Cancel</a>
                                <Button variant='success' onClick={handleSubmit}>Create</Button>
                            </div>
                        </Form>
                    );
                }}
            </Formik>
        </Layouts.Authenticated>
    );
}
