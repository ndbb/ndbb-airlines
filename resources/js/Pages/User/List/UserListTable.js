import classNames from 'classnames'
import React, { useState, useEffect, } from 'react'
import { Form, Table, Badge, ButtonGroup, Button, Modal, ModalHeader, ModalBody, ModalFooter, } from 'reactstrap'
import { Inertia, } from '@inertiajs/inertia';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfo, faUserEdit, faTrash } from '@fortawesome/free-solid-svg-icons';

const RoleBadge = ({ name }) => {
    let variant = 'secondary';

    switch (name) {
        case 'admin':
            variant = 'danger'
        break;
        case 'owner':
            variant = 'primary'
        break;
        case 'employee':
            variant = 'success'
        break;
        case 'user':
            variant = 'info'
    }

    return (<Badge pill variant={variant}>{name}</Badge>);
}

const initialState = {
    deleteModalVisible: false,
    deletingUser: null,
}

function UserListTable({ users }) {
    const [state, setState] = useState(initialState);

    const {
        deleteModalVisible,
        deletingUser,
    } = state;

    const toggleDeleteModal = (e, id) => {
        e.preventDefault();
        setState({
            ...state,
            deleteModalVisible: !state.deleteModalVisible,
            deletingUser: id
        });
    }

    const closeDeleteModal = () => {
        setState({
            ...state,
            deleteModalVisible: false,
        })
    }

    const deleteUser = (e) => {
        e.preventDefault();
        Inertia.delete(route('users.delete', { id: deletingUser }));
        closeDeleteModal();
    }

    return (<>
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email Address</th>
                    <th>Roles</th>
                    <th/>
                </tr>
            </thead>
            {(users.length > 0) &&
            <tbody>
                {users.map((u, k) => (<tr key={k}>
                    <td>{u.id}</td>
                    <td>{u.name}</td>
                    <td>{u.username}</td>
                    <td>{u.email}</td>
                    <td>{u.roles.map((r, i) => (<RoleBadge key={i} name={r.name} />))}</td>
                    <td>
                        <ButtonGroup>
                            <a href={route('users.show', { id: u.id })} className='btn btn-sm btn-info'><FontAwesomeIcon icon={faInfo} /></a>
                            <a href={route('users.edit', { id: u.id })} className='btn btn-sm btn-warning'><FontAwesomeIcon icon={faUserEdit} /></a>
                            <Button size='sm' variant='danger' onClick={(e) => toggleDeleteModal(e, u.id)} disabled={u.id === 1}><FontAwesomeIcon icon={faTrash} /></Button>
                        </ButtonGroup>
                    </td>
                </tr>))}
            </tbody>
            }
        </Table>
        <Modal isOpen={deleteModalVisible} toggle={closeDeleteModal}>
            <ModalHeader closeButton>Are you sure?</ModalHeader>
            {(deletingUser) &&
            <ModalBody>
                Are you sure that you want to delete the below user?
                <pre>
                    User ID: {deletingUser.id}<br/>
                    Username: {deletingUser.username}
                </pre>
                This action will be irreversible.
            </ModalBody>
            }
            <ModalFooter>
                <Button variant='secondary' onClick={closeDeleteModal}>Close</Button>
                <Button variant='danger' onClick={deleteUser}>Delete</Button>
            </ModalFooter>
        </Modal>
    </>)
}

export default UserListTable
