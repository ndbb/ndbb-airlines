import React, { useState, useEffect } from 'react';
import { useForm } from '@inertiajs/inertia-react';
import classNames from 'classnames';
import { Row, Col, Button, ButtonGroup }from 'reactstrap';
import Layouts from '@Layouts';
import UserListTable from './UserListTable';
import CreateUserForm from './CreateUserForm';

const initialState = {
    disabled: true,
    isEditing: false,
};

export default function UserList({ config, auth, isAdmin, pageTitle, users, statistics, heading, appTitle, ...props }) {
    const [state, setState] = useState(initialState);

    const {
        disabled,
        isEditing,
    } = state;

    const handleCreateUser = (e) => {
        e.preventDefault();
        console.log('handleCreateUser()');
    }

    return (
        <Layouts.Authenticated
            auth={auth}
            pageTitle={pageTitle}
            appTitle={appTitle}
            isAdmin={isAdmin}
        >
            <UserListTable users={users} />
            <a href={route('users.create')} className='btn btn-success'>Create User</a>
        </Layouts.Authenticated>
    );
}
