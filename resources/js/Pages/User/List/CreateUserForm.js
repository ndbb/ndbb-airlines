import React, { useState } from 'react'
import { Inertia, } from '@inertiajs/inertia';
import { Button, ButtonGroup, Modal, }from 'reactstrap';
import { useForm } from '@inertiajs/inertia-react'
import { Label, Logo, Input, ValidationErrors, Checkbox, AuthHeading } from '@Components';
import classNames from 'classnames';

const initialState = {
    isVisible: false,
    errors: {},
    touched: {},
};

const emailRegEx = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

function CreateUserForm({ onSubmit }) {
    const [state, setState] = useState(initialState);
    const {
        isVisible,
        errors,
        touched,
    } = state;

    const { data, setData, post, processing, } = useForm({
        name: '',
        username: '',
        email: '',
        password: '',
        password_confirmation: '',
    });


    const toggleModal = () => {
        setState({
            ...state,
            isVisible: !isVisible
        })
    }

    const validateName = (e) => {
        let error;
        const {
            value,
            name,
        } = e.target;

        if (value.length < 0) {
            error = 'Name is required.'
            setState({
                ...state,
                errors: {
                    ...state.errors,
                    [name]: error
                },
                touched: {
                    ...state.touched,
                    [name]: true,
                }
            })
        } else {
            setData('name', value);
            setState({
                ...state,
                errors: {
                    ...state.errors
                },
                touched: {
                    ...state.touched,
                    [name]: true
                }
            })
        }

    }

    const validateEmail = (e) => {
        let error;
        const {
            value,
            name,
        } = e.target;

        if (value.length < 0) {
            error = 'Email is required.'
            setState({
                ...state,
                errors: {
                    ...state.errors,
                    [name]: error
                },
                touched: {
                    ...state.touched,
                    [name]: true,
                }
            })
        } else {
            setData('name', value);
            setState({
                ...state,
                errors: {
                    ...state.errors
                },
                touched: {
                    ...state.touched,
                    [name]: true
                }
            })
        }

    }


    const submit = (e) => {
        e.preventDefault();
        console.log(e);
    }

    return (<div id='CreateUserForm'>
        <Button variant='success' onClick={toggleModal}>Create User</Button>
            <Modal show={isVisible} onClick={toggleModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Create New User</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form onSubmit={submit}>
                        <div className='form-group'>
                            <label htmlFor='name'>Name</label>
                            <input
                                id='name'
                                type='text'
                                name='name'
                                className='form-control'
                                onChange={validateName}
                            />
                            {errors.name && touched.name && <div className='invalid-feedback'>{errors.name}</div>}
                            {touched.name && !errors.name && <div className='valid-feedback'>Looks good.</div>}
                        </div>
                        <div className='form-group'>
                            <label htmlFor='email'>Email</label>
                            <input
                                id='email'
                                type='email'
                                name='email'
                                className='form-control'
                                onChange={validateEmail}
                            />
                            {errors.email && touched.email && <div className='invalid-feedback'>{errors.email}</div>}
                            {touched.email && !errors.email && <div className='valid-feedback'>Looks good.</div>}
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <ButtonGroup>
                        <Button variant='default' onClick={toggleModal}>Cancel</Button>
                        <Button variant='success' onClick={submit} disabled={processing}>Create</Button>
                    </ButtonGroup>
                </Modal.Footer>
            </Modal>
    </div>);
}

export default CreateUserForm
