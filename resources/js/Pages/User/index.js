import UserListComponent from './List';
import UserCreateComponent from './Create';

export const UserList = UserListComponent;
export const UserCreate = UserCreateComponent;

export default {
    UserList,
    UserCreate,
}
