import React, { useState, useEffect } from 'react';
import { useForm } from '@inertiajs/inertia-react';
import classNames from 'classnames';
import { Row, Col, Button, ButtonGroup }from 'reactstrap';
import Form from './Form';
import Layouts from '@Layouts';

const initialState = {
    disabled: true,
    isEditing: false,
};

export default function ConfigShow({ auth, isAdmin, pageTitle, config, statistics, heading, worlds, appTitle, ...props }) {
    const [state, setState] = useState(initialState);
    const { data, setData, patch, cancel, reset, processing, hasErrors, errors, ...formProps } = useForm({
        apiKey: config.apiKey,
        companyId: config.companyId,
        companyName: config.companyName,
        baseURL: config.baseURL,
        world: config.world,
        logSchedulesToFile: config.logSchedulesToFile,
    });

    const toggleEditMode = () => {
        setState({
            disabled: !state.disabled,
            isEditing: !state.isEditing,
        });
    }

    const submit = (e) => {
        e.preventDefault();
        patch('/config/edit');
        toggleEditMode();
    }

    const handleCancel = (e) => {
        e.preventDefault();
        toggleEditMode();
        cancel();
    }

    const {
        disabled,
        isEditing,
    } = state;

    return (
        <Layouts.Authenticated
            auth={auth}
            errors={errors}
            pageTitle={pageTitle}
            appTitle={appTitle}
            isAdmin={isAdmin}
        >
            <Form data={data} worlds={worlds} disabled={disabled || hasErrors} setData={setData} processing={processing}>
                <Row>
                    <Col>
                        {(isEditing)
                            ?   (<ButtonGroup>
                                    <Button variant='default' onClick={(e) => handleCancel(e)}>Cancel</Button>
                                    <Button variant='success' onClick={submit}>Save</Button>
                                </ButtonGroup>)
                            :   (<ButtonGroup>
                                <a href="/" className='btn btn-default'>Back</a>
                                <Button variant='warning' onClick={toggleEditMode}>Edit</Button>
                                </ButtonGroup>)
                        }
                    </Col>
                </Row>
            </Form>
        </Layouts.Authenticated>
    );
}
