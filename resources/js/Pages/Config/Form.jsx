import React, { useState } from 'react'
import { Container, Row, Col, Form, Button, ButtonGroup }from 'reactstrap';

function ConfigForm({ worlds, data, setData, disabled, children, submit, errors, processing, ...props }) {
    const [logSchedulesToFile, toggleLogSchedules] = useState(data.logSchedulesToFile || false);

    const handleToggleLogFiles = (e) => {
        const value = e.target.value;
        let _logSchedulesToFile = false;

        if (value == 'on') {
            _logSchedulesToFile = true;
        }

        toggleLogSchedules(_logSchedulesToFile);
        setData('logSchedulesToFile', _logSchedulesToFile)
    }

    return (<>
        <Form onSubmit={submit}>
            <Row>
                <Col>
                    <Form.Group>
                        <Form.Label>OnAir API Key</Form.Label>
                        <Form.Control type="text" name='apiKey' id='apiKey' onChange={(e) => setData('apiKey', e.target.value)} value={data.apiKey} disabled={disabled} />
                    </Form.Group>
                </Col>
                <Col>
                    <Form.Group>
                        <Form.Label>OnAir Company ID</Form.Label>
                        <Form.Control type="text" name='companyId' id='companyId' onChange={(e) => setData('companyId', e.target.value)} value={data.companyId} disabled={disabled} />
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form.Group>
                        <Form.Label>OnAir Company Name</Form.Label>
                        <Form.Control type="text" name='companyName' id='companyName' onChange={(e) => setData('companyName', e.target.value)} value={data.companyName} disabled={disabled} />
                    </Form.Group>
                </Col>
                <Col>
                    <Form.Group>
                        <Form.Label>OnAir Base URL</Form.Label>
                        <Form.Control type="text" name='baseURL' id='baseURL' onChange={(e) => setData('baseURL', e.target.value)} value={data.baseURL} disabled={disabled} />
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col md={6}>
                    <Form.Group>
                        <Form.Label>OnAir World</Form.Label>
                        <Form.Control as='select' name='world' id='world' onChange={(e) => setData('world', e.target.value)} value={data.world} disabled={disabled}>
                            <option>  -- SELECT A WORLD -- </option>
                            {worlds.map((w, i) => (<option key={i} value={w.slug}>{w.name}</option>))}
                        </Form.Control>
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col>
                    <hr />
                </Col>
            </Row>
            {children}
        </Form>
   </>);
}

export default ConfigForm
