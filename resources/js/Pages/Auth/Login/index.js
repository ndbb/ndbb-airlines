import React, { useEffect } from 'react';
import { InertiaLink } from '@inertiajs/inertia-react';
import { useForm } from '@inertiajs/inertia-react';
import { Container, Row, Col, Alert, }from 'reactstrap';
import { Label, Logo, Input, ValidationErrors, Checkbox, AuthHeading } from '@Components';

import Layouts from '@Layouts';

import styles from './styles.scss';

export default function Login({ status, canResetPassword }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        email: '',
        password: '',
        remember: '',
    });

    useEffect(() => {
        return () => {
            reset('password');
        };
    }, []);

    const onHandleChange = (event) => {
        setData(event.target.name, event.target.type === 'checkbox' ? event.target.checked : event.target.value);
    };

    const submit = (e) => {
        e.preventDefault();

        post(route('login'));
    };

    return (<Layouts.Authenticating>
        <Container>
            {status && <Alert variant='danger'>{status}</Alert>}

            <form className='form-login' onSubmit={submit}>
                <ValidationErrors errors={errors} />
                <div className='logoContainer'>
                    <div className='logo'>
                        <Logo height={50} light />
                    </div>
                    <div className='logoText'>
                        <h1 className='h2'>NDBB Airlines</h1>
                    </div>
                </div>
                <AuthHeading>Please Sign In</AuthHeading>
                <Input
                    type="email"
                    name="email"
                    value={data.email}
                    className="form-control"
                    autoComplete="email"
                    placeholder="Email address"
                    isFocused={true}
                    handleChange={onHandleChange}
                />
                <Input
                    type="password"
                    name="password"
                    value={data.password}
                    className="form-control"
                    placeholder="Password"
                    autoComplete="current-password"
                    handleChange={onHandleChange}
                />

                <div className="checkbox mb-1">
                    <label>
                        <Checkbox name="remember" value={data.remember} handleChange={onHandleChange} />
                        <span> Remember me </span>
                    </label>
                </div>

                {canResetPassword && (<div className='mb-2'>
                    <a href={route('password.request')} style={{ marginBottom: 10 }}>Forgot your password?</a>
                </div>)}

                <button type='submit' className='btn btn-lg btn-block btn-primary' disabled={processing} >Sign In</button>


            </form>
        </Container>
    </Layouts.Authenticating>);
}
