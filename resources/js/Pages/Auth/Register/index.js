import React, { useEffect, useState } from 'react';
import { InertiaLink } from '@inertiajs/inertia-react';
import { useForm } from '@inertiajs/inertia-react';

import Layouts from '@Layouts';
import { ValidationErrors, Input, Logo, AuthHeading, } from '@Components';
import styles from './styles.scss';

export default function Register() {
    const { data, setData, post, processing, errors, reset, ...rest } = useForm({
        name: '',
        username: '',
        email: '',
        password: '',
        password_confirmation: '',
    });

    console.log(rest);

    const initialState = {
        isCheckingUsername: false,
        isValid: false,
        usernameValue: '',
    };

    const { state, setState } = useState('');

    console.log(state);

    useEffect(() => {
        return () => {
            reset('password', 'password_confirmation');
        };
    }, []);

    const onHandleChange = (event) => {
        const {
            name,
            type,
            checked,
            value,
        } = event.target;

        setData(name, (type === 'checkbox') ? checked : value);
    };

    const submit = (e) => {
        e.preventDefault();

        post(route('register'));
    };

    console.log(state);
    return (
        <Layouts.Guest>
            <ValidationErrors errors={errors} />

            <form className='form-register' onSubmit={submit}>
                <div className='logoContainer'>
                    <div className='logo'>
                        <Logo height={50} light />
                    </div>
                    <div className='logoText'>
                        <h1 className='h2'>NDBB Airlines</h1>
                    </div>
                </div>
                <AuthHeading>Create an account</AuthHeading>
                <label htmlFor="name">Name</label>
                <Input
                    type="text"
                    name="name"
                    value={data.name}
                    className="form-control"
                    autoComplete="name"
                    isFocused={true}
                    placeholder="Tell us your Name"
                    required
                />
                <label htmlFor="username">Username</label>
                <Input
                    type="text"
                    name="username"
                    value={data.username}
                    className="form-control"
                    autoComplete="username"
                    placeholder="Pick a username"
                    required
                />
                <label htmlFor="email">Email Address</label>
                <Input
                    type="email"
                    name="email"
                    value={data.email}
                    className="form-control"
                    autoComplete="email"
                    placeholder="Tell us your email Address"
                    required
                />
                <label htmlFor="password">Password</label>
                <Input
                    type="password"
                    name="password"
                    value={data.password}
                    className="form-control"
                    autoComplete="new-password"
                    placeholder="Type a secure password"
                    required
                />
                <label htmlFor="password_confirmation">Confirm Password</label>
                <Input
                    type="password"
                    name="password_confirmation"
                    value={data.password_confirmation}
                    className="form-control"
                    placeholder="Type the same password again"
                    required
                />

                <button type='submit' className="btn btn-block btn-success" disabled={processing}>Register</button>
            </form>
        </Layouts.Guest>
    );
}
