import React from 'react';
import Layouts from '@Layouts';
import { Flights } from '@Components';
export default function FlightsPage({ auth, isAdmin, pageTitle, config, statistics, heading, company, flights, appTitle, ...props }) {
    console.log(Flights);
    return (
        <Layouts.Authenticated
            auth={auth}
            pageTitle={pageTitle}
            appTitle={appTitle}
            isAdmin={isAdmin}
            heading={heading}
        >
            <Flights.Table data={flights} />
        </Layouts.Authenticated>
    )
}
