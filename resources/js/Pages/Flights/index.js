import FlightsComponent from './Show';
import LogByAircraftComponent from './LogByAircraft';

export const Show = FlightsComponent;
export const LogByAircraft = LogByAircraftComponent;

export default {
    Show,
    LogByAircraft,
}
