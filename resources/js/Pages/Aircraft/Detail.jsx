import React from 'react';
import {
    Row,
    Col,
    CardColumns,
    Card,
} from 'reactstrap'
import Layouts from '@Layouts';
import { Aircraft } from '@Components';

const PrettyPrintJson = ({data}) => {
    // (destructured) data could be a prop for example
    return (<div><pre>{ JSON.stringify(data, null, 2) }</pre></div>);
}

export default function AircraftDetail({ auth, isAdmin, pageTitle, config, statistics, heading, company, aircraft, appTitle, ...props }) {
    const placeholderImg = 'https://via.placeholder.com/300x200';

    return (
        <Layouts.Authenticated
            auth={auth}
            pageTitle={pageTitle}
            appTitle={appTitle}
            isAdmin={isAdmin}
            heading={heading}
        >
            {aircraft &&
                <Card>
                    <Card.Header>afoo</Card.Header>
                    <Card.Body>
                        <PrettyPrintJson data={aircraft} />
                    </Card.Body>
                </Card>
            }
        </Layouts.Authenticated>
    )
}
