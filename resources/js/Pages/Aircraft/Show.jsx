import React from 'react';
import {
    Row,
    Col,
    CardColumns,
    Card,
} from 'reactstrap'
import Layouts from '@Layouts';
import { Aircraft } from '@Components';

export default function AircraftPage({ auth, isAdmin, pageTitle, config, statistics, heading, company, aircraft, appTitle, ...props }) {
    const placeholderImg = 'https://via.placeholder.com/300x200';

    return (
        <Layouts.Authenticated
            auth={auth}
            pageTitle={pageTitle}
            appTitle={appTitle}
            isAdmin={isAdmin}
            heading={heading}
        >
            {aircraft &&
                <Aircraft.Table data={aircraft} />
            }
        </Layouts.Authenticated>
    )
}
