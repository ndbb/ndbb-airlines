import AircraftComponent from './Show';
import DetailComponent from './Detail';

export const Show = AircraftComponent;
export const Detail = DetailComponent;

export default {
    Show,
    Detail,
}
