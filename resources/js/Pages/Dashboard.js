import React from 'react';
import { Row, Col, }from 'reactstrap';
import Layouts from '@Layouts';

export default function Dashboard({ auth, isAdmin, errors, pageTitle, appTitle, statistics, company, ...props }) {
    return (
        <Layouts.Authenticated
            auth={auth}
            errors={errors}
            pageTitle={pageTitle}
            appTitle={appTitle}
            isAdmin={isAdmin}
        >
            <p>Dashboard content will go here</p>
        </Layouts.Authenticated>
    );
}
