const axios = require('axios')
const buildUrl = ({ baseURL, world }) => {
  return `https://${world}.${baseURL}`;
}

/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function() {
  const onAirConfig = sails.config.onAir
  const Api = axios.create({
    baseURL: buildUrl({ baseURL: onAirConfig.baseURL, world: onAirConfig.world }),
    headers: {
        'oa-apikey': onAirConfig.apiKey,
        'Accept': 'application/json',
    }
  })

  const get = (endPoint) => {
    if (!endPoint) {
        throw new Error('no endPoint provided')
    }
    endPoint = (endPoint.startsWith('/')) ? endPoint : '/' + endPoint
    return Api.get(endPoint)
  }

  const getCompanyDetails = (companyId = onAirConfig.companyId) => {
    const endPoint = '/company/' + companyId
    return Api.get(endPoint)
    .then(({ data }) => {
        return data.Content;
    })
    .catch((err) => {
        console.error(err)
    })
  }

  getCompanyDetails()
  .then((data) => {
    Company.findOrCreate({ Id: data.Id }, data)
    .exec(function(err, company, wasCreated) {
      if (err) throw new Error(err)
      sails.log.debug(company)
    })
  })

};
