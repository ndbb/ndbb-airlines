<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\OnAirController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\FlightController;
use App\Http\Controllers\AircraftController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [HomeController::class, 'index'])
    ->name('home');

// Route::get('/', function () {
//     return Inertia::render('dashboard', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION
//     ]);
// });

Route::get('dashboard', [DashboardController::class, 'index'])
    ->middleware(['auth', 'verified'])
    ->name('dashboard');

Route::prefix('company')
->middleware(['auth', 'verified'])
->group(function () {
    Route::get('/', [CompanyController::class, 'index'])
        ->name('company.list');

    Route::get('edit', [CompanyController::class, 'edit'])
        ->name('company.edit');
    Route::patch('edit', [CompanyController::class, 'update'])
        ->name('company.edit');

    Route::get('create', [CompanyController::class, 'create'])
        ->name('company.create');
    Route::post('create', [CompanyController::class, 'store'])
        ->name('company.create');

    Route::post('lookup', [CompanyController::class, 'lookup'])
        ->name('company.lookup');

    Route::get('detail/{id}', [CompanyController::class, 'show'])
    ->name('company.show');

});

Route::prefix('flight')
->middleware(['auth', 'verified'])
->group(function () {
    Route::get('/', [FlightController::class, 'index'])
        ->name('flight.list');

    Route::get('detail/{id}', [FlightController::class, 'show'])
        ->name('flight.detail');

    Route::get('log/{worldSlug}/{companyUuId}/{identifier}', [FlightController::class, 'log'])
        ->name('flight.log');

    Route::get('edit', [FlightController::class, 'edit'])
        ->name('flight.edit');
    Route::patch('edit', [FlightController::class, 'update'])
        ->name('flight.edit');

    Route::get('create', [FlightController::class, 'create'])
        ->name('flight.create');
    Route::post('create', [FlightController::class, 'store'])
        ->name('flight.create');
});

Route::prefix('aircraft')
->middleware(['auth', 'verified'])
->group(function () {
    Route::get('/', [AircraftController::class, 'index'])
        ->name('aircraft.list');
    Route::get('/by-company/{companyId}', [AircraftController::class, 'indexBy_company'])
    ->name('aircraft.list.by-company');

    Route::get('detail/{id}', [AircraftController::class, 'show'])
        ->name('aircraft.detail');



    Route::get('edit', [AircraftController::class, 'edit'])
        ->name('aircraft.edit');
    Route::patch('edit', [AircraftController::class, 'update'])
        ->name('aircraft.edit');

    Route::get('create', [AircraftController::class, 'create'])
        ->name('aircraft.create');
    Route::post('create', [AircraftController::class, 'store'])
        ->name('aircraft.create');
});

Route::prefix('config')
->middleware(['auth', 'verified', 'role:admin'])
->group(function () {
    Route::get('/', [ConfigController::class, 'show'])
        ->name('config.show');

    Route::get('edit', [ConfigController::class, 'edit'])
        ->name('config.edit');
    Route::patch('edit', [ConfigController::class, 'update'])
        ->name('config.edit');

    Route::get('create', [ConfigController::class, 'create'])
        ->name('config.create');
    Route::post('create', [ConfigController::class, 'store'])
        ->name('config.create');
});

Route::prefix('profile')
->middleware(['auth', 'verified'])
->group(function () {
    Route::get('/', [UserController::class, 'profile'])
        ->name('profile');

    Route::patch('profile/edit', [UserController::class, 'profileUpdate'])
        ->name('profile.edit');
});

Route::prefix('users')
->middleware(['auth', 'verified'])
->group(function () {
    Route::get('list', [UserController::class, 'index'])
        ->middleware(['permission:user.list'])
        ->name('users.list');

    Route::get('show/{id}', [UserController::class, 'show'])
        ->middleware(['permission:user.detail'])
        ->name('users.show');

    Route::get('edit/{id}', [UserController::class, 'edit'])
        ->middleware(['permission:user.edit'])
        ->name('users.edit');

    Route::delete('delete/{id}', [UserController::class, 'destroy'])
        ->middleware(['permission:user.delete'])
        ->name('users.delete');

    Route::get('create', [UserController::class, 'create'])
        ->middleware(['permission:user.create'])
        ->name('users.create');

    Route::post('create', [UserController::class, 'store'])
        ->middleware(['permission:user.create'])
        ->name('users.create');

    Route::post('checkusername', [UserController::class, 'check_username'])
        ->middleware(['permission:user.create'])
        ->name('checkusername');

});

require __DIR__.'/auth.php';
