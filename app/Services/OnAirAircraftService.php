<?php
namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Session\SessionManager;
use App\Services\OnAirCompany;
use App\Models\Company;
use App\Models\Airport;
use App\Models\Aircraft;
use App\Models\AircraftType;
use App\Models\AircraftClass;
use App\Models\AircraftStatus;
use App\Models\FuelType;

class OnAirAircraftService extends OnAirApiService {
    protected $RentCompany = null;
    protected $Company = null;
    protected $AircraftType = null;
    protected $FuelType = null;
    protected $AircraftStatus = null;
    protected $AircraftClass = null;
    protected $RentAirport = null;
    protected $CurrentAirport = null;
    protected $CurrentCompanyUuId = null;
    protected $IsRented = false;
    protected $IsOwned = false;

    public function translate($r)
    {
        $newAircraft = [
            'uuid' => $r['Id'],
            'current_airport_id' => (isset($this->CurrentAirport)) ? $CurrentAirport->id : null,
            'aircraft_status_id' => (isset($this->AircraftStatus)) ? $AircraftStatus->id : null,
            'last_status_change' => $r['LastStatusChange'],
            'current_status_duration_in_minutes' => $r['CurrentStatusDurationInMinutes'],
            'allow_sell' => $r['AllowSell'],
            'allow_rent' => $r['AllowRent'],
            'sell_price' => $r['SellPrice'],
            'rent_hour_price' => $r['RentHourPrice'],
            'rent_airport_id' => (isset($this->RentAirport)) ? $RentAirport->id : null,
            'rent_fuel_total_gallons' => (array_key_exists('RentFuelTotalGallons', $r)) ? $r['RentFuelTotalGallons'] : null,
            'rent_caution_amount' => (array_key_exists('RentCautionAmount', $r)) ? $r['RentCautionAmount'] : null,
            'rent_start_date' => (array_key_exists('RentStartDate', $r)) ? $r['RentStartDate'] : null,
            'rent_last_daily_charge_date' => (array_key_exists('RentLastDailyChargeDate', $r)) ? $r['RentLastDailyChargeDate'] : null,
            'identifier' => $r['Identifier'],
            'heading' => $r['Heading'],
            'longitude' => $r['Longitude'],
            'latitude' => $r['Latitude'],
            'fuel_total_gallons' => $r['fuelTotalGallons'],
            'fuel_weight' => $r['fuelWeight'],
            'loaded_weight' => $r['loadedWeight'],
            'zero_fuel_weight' => $r['zeroFuelWeight'],
            'airframe_hours' => $r['airframeHours'],
            'airframe_condition' => $r['airframeCondition'],
            'last_annual_checkup' => $r['LastAnnualCheckup'],
            'last_100h_inspection' => $r['Last100hInspection'],
            'last_weekly_ownership_payment' => $r['LastWeeklyOwnershipPayment'],
            'last_parking_fee_payment' => $r['LastParkingFeePayment'],
            'is_controlled_by_ai' => $r['IsControlledByAI'],
            'hours_before_100h_inspection' => $r['HoursBefore100HInspection'],
            'extra_weight_capacity' => $r['ExtraWeightCapacity'],
            'total_weight_capacity' => $r['TotalWeightCapacity'],
            'must_do_maintenance' => $r['MustDoMaintenance'],
            'aircraft_type_id' => (isset($this->AircraftType)) ? $AircraftType->id : null,
            'current_seats' => $r['CurrentSeats'],
            'config_seats_first' =>(array_key_exists('ConfigFirstSeats', $r)) ?  (int) $r['ConfigFirstSeats'] : null,
            'config_seats_bus' => (array_key_exists('ConfigBusSeats', $r)) ? (int) $r['ConfigBusSeats'] : null,
            'config_seats_eco' => (array_key_exists('ConfigEcoSeats', $r)) ? (int) $r['ConfigEcoSeats'] : null,
            'company_id' => $this->Company->id,
            'is_rented' => $this->IsRented,
            'is_owned' => $this->IsOwned,
        ];
    }


    public function refresh()
    {
        $companies = Company::with(['world'])->where('sync_company', true)->get();

        foreach ($companies as $key => $company) {
            $this->Company = $company;
            $companyId =  $this->Company->id;
            $api_key =  $this->Company->api_key;
            $world =  $this->Company->world;

            $endpoint = 'company/'.$this->Company->uuid.'/fleet';

            $aircraftResponse = $this->makeRequest($world->slug, $api_key, $endpoint);

            foreach($aircraftResponse as $key => $r) {
                $this->AircraftClass = $onAirAircraftClassService->updateOrCreate($r['AircraftType']['AircraftClass']);

                $fuelTypeId = (int) $r['AircraftType']['fuelType'];
                $this->FuelType = FuelType::where('id', $fuelTypeId)->first();

                $r['AircraftType']['aircraft_class_id'] = $AircraftClass->id;
                $r['AircraftType']['fuel_type_id'] = $FuelType->id;

                $this->AircraftType = $onAirAircraftTypeService->updateOrCreate($r['AircraftType'])
                    ->aircraft_class()->associate($AircraftClass)
                    ->fuel_type()->associate($FuelType);

                if (array_key_exists('RentAirport', $r) && array_key_exists('ID', $r['RentAirport'])) {
                    $this->RentAirport = $onAirAirportService->updateOrCreate($r['RentAirport']);
                }

                if (array_key_exists('CurrentAirport', $r) && array_key_exists('Id', $r['CurrentAirport'])) {
                    $this->CurrentAirport = $onAirAirportService->updateOrCreate($r['CurrentAirport']);
                }

                if (array_key_exists('RentCompany', $r)) {
                    $this->RentCompany = $onAirCompanyService->updateOrCreate($r['RentCompany']);
                    $this->IsRented = true;
                }

                if (array_key_exists('Company', $r)) {
                    $this->IsOwned = true;
                }

                $this->AircraftStatus = AircraftStatus::where('id', $r['AircraftStatus'])->first();

                $newAircraft = $this->translate($r);

                $this->Aircraft = Aircraft::updateOrCreate([
                    'uuid' => $r['Id'],
                ], $newAircraft);

                $this->Aircraft->company()->associate($this->Company);

                if (isset($this->AircraftType)) {
                    $this->Aircraft->aircraft_type()->associate($this->AircraftType);
                }

                if (isset($this->CurrentAirport)) {
                    $this->Aircraft
                    ->current_airport()->associate($this->CurrentAirport);
                }

                if (isset($this->RentAirport)) {
                    $this->Aircraft
                    ->rent_airport()->associate($this->RentAirport);
                }

                $this->Aircraft->save();

                if (is_object($Aircraft) && $Aircraft->wasRecentlyCreated) {
                    $this->created++;
                } else if ($Aircraft !== null) {
                    $this->updated++;
                }
            }
        }
    }
}
