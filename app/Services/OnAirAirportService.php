<?php
namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Session\SessionManager;
use App\Models\Company;
use App\Models\Airport;

class OnAirAirportService extends OnAirApiService {
    protected $updated = 0;
    protected $created = 0;

    public function translate($response)
    {
        $translated = [
            'uuid' => $response['Id'],
            'name' => $response['Name'],
            'airline' => $response['AirlineCode'],
            'last_connected' => $response['LastConnection'],
            'last_report_date' => $response['LastReportDate'],
            'reputation' => $response['Reputation'],
            'creation_date' => $response['CreationDate'],
            'difficulty_level' => $response['DifficultyLevel'],
            'level' => $response['Level'],
            'xp' => $response['LevelXP'],
            'transport_employee_instant' => $response['TransportEmployeeInstant'],
            'transport_player_instant' => $response['TransportPlayerInstant'],
            'force_time_in_simulator' => $response['ForceTimeInSimulator'],
            'use_small_airports' => $response['UseSmallAirports'],
            'use_only_vanilla_airports' => $response['UseOnlyVanillaAirports'],
            'enable_skill_tree' => $response['EnableSkillTree'],
            'checkride_level' => $response['CheckrideLevel'],
            'enable_landing_penalities' => $response['EnableLandingPenalities'],
            'enable_employees_flight_duty_and_sleep' => $response['EnableEmployeesFlightDutyAndSleep'],
            'aircraft_rent_level' => $response['AircraftRentLevel'],
            'enable_cargos_and_charters_loading_time' => $response['EnableCargosAndChartersLoadingTime'],
            'in_survival' => $response['InSurvival'],
            'pay_bonus_factor' => $response['PayBonusFactor'],
            'enable_sim_failures' => $response['EnableSimFailures'],
            'disable_seats_config_check' => $response['DisableSeatsConfigCheck'],
            'realistic_sim_procedures' => $response['RealisticSimProcedures'],
        ];

        return $translated;
    }

    public function refresh()
    {
        $companies = Company::with(['world'])->where('sync_company', true)->get();

        foreach ($companies as $key => $company) {
            $companyId = $company->uuid;
            $api_key = $company->api_key;
            $world = $company->world;

            $response = $this->makeRequest($world->slug, $api_key, '/company/'.$companyId);
            $newCompany = $this->translate($response);

            $company->update($newCompany);

            if (is_object($company) && $company->wasRecentlyCreated) {
                $this->created++;
            } else if ($company === true) {
                $this->updated++;
            }
        }

        return [
            'updated' => $this->updated,
            'created' => $this->created,
        ];
    }
}
