<?php
namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Session\SessionManager;
use App\Models\Company;
use App\Models\AircraftClass;
use App\Models\AircraftType;

class OnAirAircraftTypeService extends OnAirApiService {
    protected $updated = 0;
    protected $created = 0;

    public function translate($response)
    {
        $translated = [
            'uuid' => $response['AircraftTypeId'],
            'creation_date' => $response['AircraftType']['CreationDate'],
            'last_moderation_date' => $response['AircraftType']['LastModerationDate'],
            'display_name' => $response['AircraftType']['DisplayName'],
            'type_name' => $response['AircraftType']['TypeName'],
            'flights_count' => $response['AircraftType']['FlightsCount'],
            'time_between_overhaul' => $response['AircraftType']['TimeBetweenOverhaul'],
            'hightime_airframe' => $response['AircraftType']['HightimeAirframe'],
            'airport_min_size' => $response['AircraftType']['AirportMinSize'],
            'empty_weight' => $response['AircraftType']['emptyWeight'],
            'maximum_gross_weight' => $response['AircraftType']['maximumGrossWeight'],
            'estimated_cruise_ff' => $response['AircraftType']['estimatedCruiseFF'],
            'baseprice' => $response['AircraftType']['Baseprice'],
            'fuel_total_capacity_in_gallons' => $response['AircraftType']['FuelTotalCapacityInGallons'],
            'engine_type' => $response['AircraftType']['engineType'],
            'number_of_engines' => $response['AircraftType']['numberOfEngines'],
            'seats' => $response['AircraftType']['seats'],
            'needs_copilot' => $response['AircraftType']['needsCopilot'],
            'maximum_cargo_weight' => $response['AircraftType']['maximumCargoWeight'],
            'maximum_range_in_hour' => $response['AircraftType']['maximumRangeInHour'],
            'maximum_range_in_nm' => $response['AircraftType']['maximumRangeInNM'],
            'design_speed_vs0' => $response['AircraftType']['designSpeedVS0'],
            'design_speed_vs1' => $response['AircraftType']['designSpeedVS1'],
            'design_speed_vc' => $response['AircraftType']['designSpeedVC'],
            'is_disabled' => $response['AircraftType']['IsDisabled'],
            'luxef_actor' => $response['AircraftType']['LuxeFactor'],
            'standard_seat_weight' => $response['AircraftType']['StandardSeatWeight'],
            'is_fighter' => $response['AircraftType']['IsFighter'],
            'air_file_name' => (array_key_exists('AirFileName', $response['AircraftType'])) ? $response['AircraftType']['AirFileName'] : null,
            'simulator_version' => $response['AircraftType']['simulatorVersion'],
            'consolidated_design_speed_vc' => $response['AircraftType']['ConsolidatedDesignSpeedVC'],
            'consolidated_estimated_cruise_ff' => $response['AircraftType']['ConsolidatedEstimatedCruiseFF'],
            'addon_estimated_fuel_flow' => $response['AircraftType']['AddonEstimatedFuelFlow'],
            'addon_design_speed_vc' => $response['AircraftType']['AddonDesignSpeedVC'],
            'computed_max_payload' => $response['AircraftType']['ComputedMaxPayload'],
            'computed_seats' => $response['AircraftType']['ComputedSeats'],
            'aircraft_class_id' => $AircraftClass->id,
            'fuel_type_id' => $fuelTypeId
        ];

        return $translated;
    }

    public function updateOrCreate($response)
    {
        $translated = $this->translate($response);

        return AircraftClass::updateOrCreate([
            'uuid' => $translated['uuid']
        ], $translated);
    }

}
