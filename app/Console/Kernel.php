<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use \Spatie\ShortSchedule\ShortSchedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\OnAirRefreshCompanyCashFlow',
        'App\Console\Commands\OnAirRefreshCompanyDetails',
        'App\Console\Commands\OnAirRefreshCompanyEmployees',
        'App\Console\Commands\OnAirRefreshCompanyFBOs',
        'App\Console\Commands\OnAirRefreshCompanyFleet',
        'App\Console\Commands\OnAirRefreshCompanyFlights',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // hourly
        $schedule
            ->command('onair:refreshcompanydetails')
            ->hourly()
            ->withoutOverlapping()
            ->appendOutputTo('storage/logs/onaircompany.log')
            ->emailOutputTo('admin@ndboost.com');

        $schedule->command('onair:refreshcompanyfleet')
            ->hourly()
            ->withoutOverlapping()
            ->appendOutputTo('storage/logs/onaircompany.log')
            ->emailOutputTo('admin@ndboost.com');

        // every minute
        $schedule
            ->command('onair:refreshcompanyemployees')
            ->everyMinute()
            ->withoutOverlapping()
            ->appendOutputTo('storage/logs/onaircompany.log');

        $schedule
            ->command('onair:refreshcompanyflights')
            ->everyMinute()
            ->withoutOverlapping()
            ->appendOutputTo('storage/logs/onaircompany.log');

    }

    protected function shortSchedule(ShortSchedule $shortSchedule)
    {
        // $shortSchedule
        //     ->command('onair:refreshcompanyflights')
        //     ->everySeconds(30);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}

