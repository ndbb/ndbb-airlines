<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\OnAirAircraftService;
use App\Services\OnAirAircraftClassService;
use App\Services\OnAirAircraftTypeService;
use App\Services\OnAirAirportService;
use App\Services\OnAirCompanyService;

class OnAirRefreshCompanyFleet extends OnAirCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'onair:refreshfleet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refreshes the OnAir Company fleet (Aircraft) information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

        /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(
        OnAirAircraftService $onAirAircraftService
    ) {
        $this->logStart();
        $results = $onAirAircraftService->refresh();

        $this->logStats($results['updated'], $results['created']);
        return 0;

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function old_handle()
    {
        $this->logStart();

        $companies = Company::with(['world'])->where('sync_fleet', true)->get();

        foreach ($companies as $key => $company) {
            $c = Company::with(['world'])
              ->where('sync_fleet', true)
              ->where('uuid', $company->uuid)
              ->first();

            $companyId = $c['id'];
            $CompanyUuId = $c['uuid'];
            $api_key = $c['api_key'];
            $world = $c['world'];
            echo "companyId: $companyId \n";
            echo "companyUuId: $CompanyUuId\n";

            $response = $this->makeRequest($world->slug, $api_key, 'company/'.$CompanyUuId.'/fleet');

            $updatedFleet = [];
            $createdFleet = [];

            foreach($response as $key => $r) {
                $IsRented = false;
                $IsOwned = false;
                $RentCompany = null;
                $Company = null;
                $AircraftType = null;
                $FuelType = null;
                $AircraftStatus = null;
                $AircraftClass = null;
                $RentAirport = null;
                $CurrentAirport = null;
                $CurrentCompanyUuId = null;

                $newAircraftClass = [
                    'uuid' => $r['AircraftType']['AircraftClass']['Id'],
                    'short_name' => $r['AircraftType']['AircraftClass']['ShortName'],
                    'name' => $r['AircraftType']['AircraftClass']['Name'],
                    'order' => $r['AircraftType']['AircraftClass']['Order'],
                ];

                $AircraftClass = AircraftClass::updateOrCreate([
                    'uuid' => $newAircraftClass['uuid']
                ], $newAircraftClass);

                $fuelTypeId = (int) $r['AircraftType']['fuelType'];
                $FuelType = FuelType::where('id', $fuelTypeId)->first();

                $newAircraftType = [
                    'uuid' => $r['AircraftTypeId'],
                    'creation_date' => $r['AircraftType']['CreationDate'],
                    'last_moderation_date' => $r['AircraftType']['LastModerationDate'],
                    'display_name' => $r['AircraftType']['DisplayName'],
                    'type_name' => $r['AircraftType']['TypeName'],
                    'flights_count' => $r['AircraftType']['FlightsCount'],
                    'time_between_overhaul' => $r['AircraftType']['TimeBetweenOverhaul'],
                    'hightime_airframe' => $r['AircraftType']['HightimeAirframe'],
                    'airport_min_size' => $r['AircraftType']['AirportMinSize'],
                    'empty_weight' => $r['AircraftType']['emptyWeight'],
                    'maximum_gross_weight' => $r['AircraftType']['maximumGrossWeight'],
                    'estimated_cruise_ff' => $r['AircraftType']['estimatedCruiseFF'],
                    'baseprice' => $r['AircraftType']['Baseprice'],
                    'fuel_total_capacity_in_gallons' => $r['AircraftType']['FuelTotalCapacityInGallons'],
                    'engine_type' => $r['AircraftType']['engineType'],
                    'number_of_engines' => $r['AircraftType']['numberOfEngines'],
                    'seats' => $r['AircraftType']['seats'],
                    'needs_copilot' => $r['AircraftType']['needsCopilot'],
                    'maximum_cargo_weight' => $r['AircraftType']['maximumCargoWeight'],
                    'maximum_range_in_hour' => $r['AircraftType']['maximumRangeInHour'],
                    'maximum_range_in_nm' => $r['AircraftType']['maximumRangeInNM'],
                    'design_speed_vs0' => $r['AircraftType']['designSpeedVS0'],
                    'design_speed_vs1' => $r['AircraftType']['designSpeedVS1'],
                    'design_speed_vc' => $r['AircraftType']['designSpeedVC'],
                    'is_disabled' => $r['AircraftType']['IsDisabled'],
                    'luxef_actor' => $r['AircraftType']['LuxeFactor'],
                    'standard_seat_weight' => $r['AircraftType']['StandardSeatWeight'],
                    'is_fighter' => $r['AircraftType']['IsFighter'],
                    'air_file_name' => (array_key_exists('AirFileName', $r['AircraftType'])) ? $r['AircraftType']['AirFileName'] : null,
                    'simulator_version' => $r['AircraftType']['simulatorVersion'],
                    'consolidated_design_speed_vc' => $r['AircraftType']['ConsolidatedDesignSpeedVC'],
                    'consolidated_estimated_cruise_ff' => $r['AircraftType']['ConsolidatedEstimatedCruiseFF'],
                    'addon_estimated_fuel_flow' => $r['AircraftType']['AddonEstimatedFuelFlow'],
                    'addon_design_speed_vc' => $r['AircraftType']['AddonDesignSpeedVC'],
                    'computed_max_payload' => $r['AircraftType']['ComputedMaxPayload'],
                    'computed_seats' => $r['AircraftType']['ComputedSeats'],
                    'aircraft_class_id' => $AircraftClass->id,
                    'fuel_type_id' => $fuelTypeId
                ];

                $AircraftType = AircraftType::updateOrCreate([
                    'uuid' => $newAircraftType['uuid']
                ], $newAircraftType)
                    ->aircraft_class()->associate($AircraftClass)
                    ->fuel_type()->associate($FuelType);

                // dd($AircraftType);

                if (array_key_exists('RentAirport', $r) && array_key_exists('ID', $r['RentAirport'])) {
                        $newRentAirport = [
                            'uuid' => $r['RentAirportId'],
                            'icao' => $r['RentAirport']['ICAO'],
                            'has_no_runways' => $r['RentAirport']['HasNoRunways'],
                            'time_offset_in_sec' => $r['RentAirport']['TimeOffsetInSec'],
                            'local_time_open_in_hours_since_midnight' => $r['RentAirport']['LocalTimeOpenInHoursSinceMidnight'],
                            'local_time_close_in_hours_since_midnight' => $r['RentAirport']['LocalTimeCloseInHoursSinceMidnight'],
                            'iata' => (array_key_exists('IATA', $r['RentAirport'])) ? $r['RentAirport']['IATA'] : null,
                            'name' => $r['RentAirport']['Name'],
                            'state' => (array_key_exists('State', $r['RentAirport'])) ? $r['RentAirport']['State'] : null,
                            'country_code' => (array_key_exists('CountryCode', $r['RentAirport'])) ? $r['RentAirport']['CountryCode'] : null,
                            'country_name' => (array_key_exists('CountryName', $r['RentAirport'])) ? $r['RentAirport']['CountryName'] : null,
                            'city' => (array_key_exists('City', $r['RentAirport'])) ? $r['RentAirport']['City'] : null,
                            'latitude' => $r['RentAirport']['Latitude'],
                            'longitude' => $r['RentAirport']['Longitude'],
                            'elevation' => $r['RentAirport']['Elevation'],
                            'has_land_runway' => $r['RentAirport']['HasLandRunway'],
                            'has_water_runway' => $r['RentAirport']['HasWaterRunway'],
                            'has_helipad' => $r['RentAirport']['HasHelipad'],
                            'size' => $r['RentAirport']['Size'],
                            'transition_altitude' => $r['RentAirport']['TransitionAltitude'],
                            'is_not_in_vanilla_fsx' => $r['RentAirport']['IsNotInVanillaFSX'],
                            'is_not_in_vanilla_p3d' => $r['RentAirport']['IsNotInVanillaP3D'],
                            'is_not_in_vanilla_xplane' => $r['RentAirport']['IsNotInVanillaXPLANE'],
                            'is_not_in_vanilla_fs2020' => $r['RentAirport']['IsNotInVanillaFS2020'],
                            'is_closed' => $r['RentAirport']['IsClosed'],
                            'is_valid' => $r['RentAirport']['IsValid'],
                            'mag_var' => $r['RentAirport']['MagVar'],
                            'is_addon' => $r['RentAirport']['IsAddon'],
                            'random_seed' => $r['RentAirport']['RandomSeed'],
                            'last_random_seed_generation' =>(array_key_exists('LastRandomSeedGeneration', $r['RentAirport'])) ? \Carbon\Carbon::parse($r['RentAirport']['LastRandomSeedGeneration']) : null,
                            'is_military' => $r['RentAirport']['IsMilitary'],
                            'has_lights' => $r['RentAirport']['HasLights'],
                            'airport_source' => $r['RentAirport']['AirportSource'],
                            'last_very_short_request_date' => (array_key_exists('LastVeryShortRequestDate', $r['RentAirport'])) ? \Carbon\Carbon::parse($r['RentAirport']['LastVeryShortRequestDate']) : null,
                            'last_small_trip_request_date' => (array_key_exists('LastSmallTripRequestDate', $r['RentAirport'])) ? \Carbon\Carbon::parse($r['RentAirport']['LastSmallTripRequestDate']) : null,
                            'last_medium_trip_request_date' => (array_key_exists('LastMediumTripRequestDate', $r['RentAirport']) )? \Carbon\Carbon::parse($r['RentAirport']['LastMediumTripRequestDate']) : null,
                            'last_short_haul_request_date' => (array_key_exists('LastShortHaulRequestDate', $r['RentAirport'])) ? \Carbon\Carbon::parse($r['RentAirport']['LastShortHaulRequestDate']) : null,
                            'last_medium_haul_request_date' => (array_key_exists('LastMediumHaulRequestDate', $r['RentAirport']) )? \Carbon\Carbon::parse($r['RentAirport']['LastMediumHaulRequestDate']) : null,
                            'last_long_haul_request_date' => (array_key_exists('LastLongHaulRequestDate', $r['RentAirport'])) ? \Carbon\Carbon::parse($r['RentAirport']['LastLongHaulRequestDate']) : null,
                            'display_name' => $r['RentAirport']['DisplayName'],
                            'utc_time_open_in_hours_since_midnight' => $r['RentAirport']['UTCTimeOpenInHoursSinceMidnight'],
                        'utc_time_close_in_hours_since_midnight' => $r['RentAirport']['UTCTimeCloseInHoursSinceMidnight'],
                    ];

                    $RentAirport = Airport::updateOrCreate([
                        'uuid' => $r['RentAirportId']
                    ], $newRentAirport);

                }

                if (array_key_exists('CurrentAirport', $r) && array_key_exists('Id', $r['CurrentAirport'])) {
                    $newCurrentAirport = [
                        'uuid' => $r['CurrentAirport']['Id'],
                        'icao' => $r['CurrentAirport']['ICAO'],
                        'has_no_runways' => $r['CurrentAirport']['HasNoRunways'],
                        'time_offset_in_sec' => $r['CurrentAirport']['TimeOffsetInSec'],
                        'local_time_open_in_hours_since_midnight' => $r['CurrentAirport']['LocalTimeOpenInHoursSinceMidnight'],
                        'local_time_close_in_hours_since_midnight' => $r['CurrentAirport']['LocalTimeCloseInHoursSinceMidnight'],
                        'iata' => (array_key_exists('IATA', $r['CurrentAirport'])) ? $r['CurrentAirport']['IATA'] : null,
                        'name' => $r['CurrentAirport']['Name'],
                        'state' => (array_key_exists('State', $r['CurrentAirport'])) ? $r['CurrentAirport']['State'] : null,
                        'country_code' => (array_key_exists('CountryCode', $r['CurrentAirport'])) ? $r['CurrentAirport']['CountryCode'] : null,
                        'country_name' => (array_key_exists('CountryName', $r['CurrentAirport'])) ? $r['CurrentAirport']['CountryName'] : null,
                        'city' => (array_key_exists('City', $r['CurrentAirport'])) ? $r['CurrentAirport']['City'] : null,
                        'latitude' => $r['CurrentAirport']['Latitude'],
                        'longitude' => $r['CurrentAirport']['Longitude'],
                        'elevation' => $r['CurrentAirport']['Elevation'],
                        'has_land_runway' => $r['CurrentAirport']['HasLandRunway'],
                        'has_water_runway' => $r['CurrentAirport']['HasWaterRunway'],
                        'has_helipad' => $r['CurrentAirport']['HasHelipad'],
                        'size' => $r['CurrentAirport']['Size'],
                        'transition_altitude' => $r['CurrentAirport']['TransitionAltitude'],
                        'is_not_in_vanilla_fsx' => $r['CurrentAirport']['IsNotInVanillaFSX'],
                        'is_not_in_vanilla_p3d' => $r['CurrentAirport']['IsNotInVanillaP3D'],
                        'is_not_in_vanilla_xplane' => $r['CurrentAirport']['IsNotInVanillaXPLANE'],
                        'is_not_in_vanilla_fs2020' => $r['CurrentAirport']['IsNotInVanillaFS2020'],
                        'is_closed' => $r['CurrentAirport']['IsClosed'],
                        'is_valid' => $r['CurrentAirport']['IsValid'],
                        'mag_var' => $r['CurrentAirport']['MagVar'],
                        'is_addon' => $r['CurrentAirport']['IsAddon'],
                        'random_seed' => $r['CurrentAirport']['RandomSeed'],
                        'last_random_seed_generation' => $r['CurrentAirport']['LastRandomSeedGeneration'],
                        'is_military' => $r['CurrentAirport']['IsMilitary'],
                        'has_lights' => $r['CurrentAirport']['HasLights'],
                        'airport_source' => $r['CurrentAirport']['AirportSource'],
                        'last_very_short_request_date' => (array_key_exists('LastVeryShortRequestDate', $r['CurrentAirport'])) ? $r['CurrentAirport']['LastVeryShortRequestDate'] : null,
                        'last_small_trip_request_date' => (array_key_exists('LastSmallTripRequestDate', $r['CurrentAirport'])) ? $r['CurrentAirport']['LastSmallTripRequestDate'] : null,
                        'last_medium_trip_request_date' => (array_key_exists('LastMediumTripRequestDate', $r['CurrentAirport'])) ? $r['CurrentAirport']['LastMediumTripRequestDate'] : null,
                        'last_short_haul_request_date' => (array_key_exists('LastShortHaulRequestDate', $r['CurrentAirport'])) ? $r['CurrentAirport']['LastShortHaulRequestDate'] : null,
                        'last_medium_haul_request_date' => (array_key_exists('LastMediumHaulRequestDate', $r['CurrentAirport'])) ? $r['CurrentAirport']['LastMediumHaulRequestDate'] : null,
                        'last_long_haul_request_date' => (array_key_exists('LastLongHaulRequestDate', $r['CurrentAirport'])) ? $r['CurrentAirport']['LastLongHaulRequestDate'] : null,
                        'display_name' => $r['CurrentAirport']['DisplayName'],
                        'utc_time_open_in_hours_since_midnight' => $r['CurrentAirport']['UTCTimeOpenInHoursSinceMidnight'],
                        'utc_time_close_in_hours_since_midnight' => $r['CurrentAirport']['UTCTimeCloseInHoursSinceMidnight'],
                    ];

                    $CurrentAirport = Airport::updateOrCreate([
                        'uuid' => $newCurrentAirport['uuid']
                    ], $newCurrentAirport);
                }

                if (array_key_exists('RentCompany', $r)) {
                    $newRentCompany = [
                        'uuid' => $r['RentCompany']['Id'],
                        'name' => $r['RentCompany']['Name'],
                        'airline' => $r['RentCompany']['AirlineCode'],
                        'last_connected' => $r['RentCompany']['LastConnection'],
                        'last_report_date' => $r['RentCompany']['LastReportDate'],
                        'reputation' => $r['RentCompany']['Reputation'],
                        'creation_date' => $r['RentCompany']['CreationDate'],
                        'difficulty_level' => $r['RentCompany']['DifficultyLevel'],
                        'level' => $r['RentCompany']['Level'],
                        'xp' => $r['RentCompany']['LevelXP'],
                        'transport_employee_instant' => $r['RentCompany']['TransportEmployeeInstant'],
                        'transport_player_instant' => $r['RentCompany']['TransportPlayerInstant'],
                        'force_time_in_simulator' => $r['RentCompany']['ForceTimeInSimulator'],
                        'use_small_airports' => $r['RentCompany']['UseSmallAirports'],
                        'use_only_vanilla_airports' => $r['RentCompany']['UseOnlyVanillaAirports'],
                        'enable_skill_tree' => $r['RentCompany']['EnableSkillTree'],
                        'checkride_level' => $r['RentCompany']['CheckrideLevel'],
                        'enable_landing_penalities' => $r['RentCompany']['EnableLandingPenalities'],
                        'enable_employees_flight_duty_and_sleep' => $r['RentCompany']['EnableEmployeesFlightDutyAndSleep'],
                        'aircraft_rent_level' => $r['RentCompany']['AircraftRentLevel'],
                        'enable_cargos_and_charters_loading_time' => $r['RentCompany']['EnableCargosAndChartersLoadingTime'],
                        'in_survival' => $r['RentCompany']['InSurvival'],
                        'pay_bonus_factor' => $r['RentCompany']['PayBonusFactor'],
                        'enable_sim_failures' => $r['RentCompany']['EnableSimFailures'],
                        'disable_seats_config_check' => $r['RentCompany']['DisableSeatsConfigCheck'],
                        'realistic_sim_procedures' => $r['RentCompany']['RealisticSimProcedures'],
                    ];

                    $RentCompany = Company::updateOrCreate([
                        'uuid' => $newRentCompany['uuid']
                    ], $newRentCompany);

                    $IsRented = true;
                }


                if (array_key_exists('Company', $r)) {
                    $IsOwned = true;
                }

                $AircraftStatus = AircraftStatus::where('id', $r['AircraftStatus'])->first();

                $newAircraft = [
                    'uuid' => $r['Id'],
                    'current_airport_id' => (isset($CurrentAirport)) ? $CurrentAirport->id : null,
                    'aircraft_status_id' => (isset($AircraftStatus)) ? $AircraftStatus->id : null,
                    'last_status_change' => $r['LastStatusChange'],
                    'current_status_duration_in_minutes' => $r['CurrentStatusDurationInMinutes'],
                    'allow_sell' => $r['AllowSell'],
                    'allow_rent' => $r['AllowRent'],
                    'sell_price' => $r['SellPrice'],
                    'rent_hour_price' => $r['RentHourPrice'],
                    'rent_airport_id' => (isset($RentAirport)) ? $RentAirport->id : null,
                    'rent_fuel_total_gallons' => (array_key_exists('RentFuelTotalGallons', $r)) ? $r['RentFuelTotalGallons'] : null,
                    'rent_caution_amount' => (array_key_exists('RentCautionAmount', $r)) ? $r['RentCautionAmount'] : null,
                    'rent_start_date' => (array_key_exists('RentStartDate', $r)) ? $r['RentStartDate'] : null,
                    'rent_last_daily_charge_date' => (array_key_exists('RentLastDailyChargeDate', $r)) ? $r['RentLastDailyChargeDate'] : null,
                    'identifier' => $r['Identifier'],
                    'heading' => $r['Heading'],
                    'longitude' => $r['Longitude'],
                    'latitude' => $r['Latitude'],
                    'fuel_total_gallons' => $r['fuelTotalGallons'],
                    'fuel_weight' => $r['fuelWeight'],
                    'loaded_weight' => $r['loadedWeight'],
                    'zero_fuel_weight' => $r['zeroFuelWeight'],
                    'airframe_hours' => $r['airframeHours'],
                    'airframe_condition' => $r['airframeCondition'],
                    'last_annual_checkup' => $r['LastAnnualCheckup'],
                    'last_100h_inspection' => $r['Last100hInspection'],
                    'last_weekly_ownership_payment' => $r['LastWeeklyOwnershipPayment'],
                    'last_parking_fee_payment' => $r['LastParkingFeePayment'],
                    'is_controlled_by_ai' => $r['IsControlledByAI'],
                    'hours_before_100h_inspection' => $r['HoursBefore100HInspection'],
                    'extra_weight_capacity' => $r['ExtraWeightCapacity'],
                    'total_weight_capacity' => $r['TotalWeightCapacity'],
                    'must_do_maintenance' => $r['MustDoMaintenance'],
                    'aircraft_type_id' => (isset($AircraftType)) ? $AircraftType->id : null,
                    'current_seats' => $r['CurrentSeats'],
                    'config_seats_first' =>(array_key_exists('ConfigFirstSeats', $r)) ?  (int) $r['ConfigFirstSeats'] : null,
                    'config_seats_bus' => (array_key_exists('ConfigBusSeats', $r)) ? (int) $r['ConfigBusSeats'] : null,
                    'config_seats_eco' => (array_key_exists('ConfigEcoSeats', $r)) ? (int) $r['ConfigEcoSeats'] : null,
                    'company_id' => $companyId,
                    'is_rented' => $IsRented,
                    'is_owned' => $IsOwned,
                ];

                var_dump($newAircraft);

                $Aircraft = Aircraft::updateOrCreate([
                    'uuid' => $r['Id'],
                ], $newAircraft);

                $Aircraft
                  ->company()->associate($company);

                if (isset($AircraftType)) {
                    $Aircraft
                      ->aircraft_type()->associate($AircraftType);
                }

                if (isset($CurrentAirport)) {
                    $Aircraft
                      ->current_airport()->associate($CurrentAirport);
                }

                if (isset($RentAirport)) {
                    $Aircraft
                      ->rent_airport()->associate($RentAirport);
                }

                $Aircraft->save();

                if (is_object($Aircraft) && $Aircraft->wasRecentlyCreated) {
                    $this->created++;
                } else if ($Aircraft !== null) {
                    $this->updated++;
                }
            }
        }

        $this->logStats();
        return 0;

    }
}
