<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Company;
use App\Models\Airport;
use App\Models\Flight;
use App\Models\Aircraft;
use App\Services\OnAirApiService;

class OnAirRefreshCompanyFlights extends OnAirCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'onair:refreshflights';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OnAirApiService $onAirService)
    {
        parent::__construct($onAirService);

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->logStart();

        $companies = Company::with(['world'])->where('sync_fleet', true)->get();

        foreach ($companies as $key => $company) {
            $companyUuId = $company->uuid;
            $companyId = $company->id;
            $api_key = $company->api_key;
            $world = $company->world;
            $response = $this->makeRequest($world->slug, $api_key, '/company/'.$companyUuId.'/flights');

            $flights = [];

            foreach ($response as $key => $r) {
                $aircraftId = null;

                // ensure DepartureAirport exists
                if (array_key_exists('DepartureAirport', $r)) {
                    $newDepartureAirport = [
                        'uuid' => $r['DepartureAirport']['Id'],
                        'icao' => $r['DepartureAirport']['ICAO'],
                        'has_no_runways' => $r['DepartureAirport']['HasNoRunways'],
                        'time_offset_in_sec' => (array_key_exists('TimeOffsetInSec', $r['DepartureAirport'])) ? $r['DepartureAirport']['TimeOffsetInSec'] : null,
                        'local_time_open_in_hours_since_midnight' => (array_key_exists('LocalTimeOpenInHoursSinceMidnight', $r['DepartureAirport'])) ? $r['DepartureAirport']['LocalTimeOpenInHoursSinceMidnight'] : null,
                        'local_time_close_in_hours_since_midnight' => (array_key_exists('LocalTimeCloseInHoursSinceMidnight', $r['DepartureAirport'])) ? $r['DepartureAirport']['LocalTimeCloseInHoursSinceMidnight'] : null,
                        'iata' => (array_key_exists('IATA', $r['DepartureAirport'])) ? $r['DepartureAirport']['IATA'] : null,
                        'name' => $r['DepartureAirport']['Name'],
                        'state' => (array_key_exists('State', $r['DepartureAirport'])) ? $r['DepartureAirport']['State'] : null,
                        'country_code' => (array_key_exists('CountryCode', $r['DepartureAirport'])) ? $r['DepartureAirport']['CountryCode'] : null,
                        'country_name' => (array_key_exists('CountryName', $r['DepartureAirport'])) ? $r['DepartureAirport']['CountryName'] : null,
                        'city' => $r['DepartureAirport']['City'],
                        'latitude' => $r['DepartureAirport']['Latitude'],
                        'longitude' => $r['DepartureAirport']['Longitude'],
                        'elevation' => $r['DepartureAirport']['Elevation'],
                        'has_land_runway' => $r['DepartureAirport']['HasLandRunway'],
                        'has_water_runway' => $r['DepartureAirport']['HasWaterRunway'],
                        'has_helipad' => $r['DepartureAirport']['HasHelipad'],
                        'size' => $r['DepartureAirport']['Size'],
                        'transition_altitude' => $r['DepartureAirport']['TransitionAltitude'],
                        'is_not_in_vanilla_fsx' => $r['DepartureAirport']['IsNotInVanillaFSX'],
                        'is_not_in_vanilla_p3d' => $r['DepartureAirport']['IsNotInVanillaP3D'],
                        'is_not_in_vanilla_xplane' => $r['DepartureAirport']['IsNotInVanillaXPLANE'],
                        'is_not_in_vanilla_fs2020' => $r['DepartureAirport']['IsNotInVanillaFS2020'],
                        'is_closed' => $r['DepartureAirport']['IsClosed'],
                        'is_valid' => $r['DepartureAirport']['IsValid'],
                        'mag_var' => $r['DepartureAirport']['MagVar'],
                        'is_addon' => $r['DepartureAirport']['IsAddon'],
                        'random_seed' => $r['DepartureAirport']['RandomSeed'],
                        'last_random_seed_generation' => (array_key_exists('LastRandomSeedGeneration',  $r['DepartureAirport'])) ? \Carbon\Carbon::parse($r['DepartureAirport']['LastRandomSeedGeneration']) : null,
                        'is_military' => $r['DepartureAirport']['IsMilitary'],
                        'has_lights' => $r['DepartureAirport']['HasLights'],
                        'airport_source' => $r['DepartureAirport']['AirportSource'],
                        'last_very_short_request_date' => (array_key_exists('LastVeryShortRequestDate', $r['DepartureAirport'])) ? \Carbon\Carbon::parse($r['DepartureAirport']['LastVeryShortRequestDate']) : null,
                        'last_small_trip_request_date' => (array_key_exists('LastSmallTripRequestDate', $r['DepartureAirport'])) ? \Carbon\Carbon::parse($r['DepartureAirport']['LastSmallTripRequestDate']) : null,
                        'last_medium_trip_request_date' => (array_key_exists('LastMediumTripRequestDate', $r['DepartureAirport']) )? \Carbon\Carbon::parse($r['DepartureAirport']['LastMediumTripRequestDate']) : null,
                        'last_short_haul_request_date' => (array_key_exists('LastShortHaulRequestDate', $r['DepartureAirport'])) ? \Carbon\Carbon::parse($r['DepartureAirport']['LastShortHaulRequestDate']) : null,
                        'last_medium_haul_request_date' => (array_key_exists('LastMediumHaulRequestDate', $r['DepartureAirport']) )? \Carbon\Carbon::parse($r['DepartureAirport']['LastMediumHaulRequestDate']) : null,
                        'last_long_haul_request_date' => (array_key_exists('LastLongHaulRequestDate', $r['DepartureAirport'])) ? \Carbon\Carbon::parse($r['DepartureAirport']['LastLongHaulRequestDate']) : null,
                        'display_name' => $r['DepartureAirport']['DisplayName'],
                        'utc_time_open_in_hours_since_midnight' => $r['DepartureAirport']['UTCTimeOpenInHoursSinceMidnight'],
                        'utc_time_close_in_hours_since_midnight' => $r['DepartureAirport']['UTCTimeCloseInHoursSinceMidnight'],
                    ];

                    $departureAirport = Airport::updateOrCreate([
                        'uuid' => $r['DepartureAirport']['Id']
                    ], $newDepartureAirport);
                }

                // ensure ArrivalIntendedAirport exists
                if (array_key_exists('ArrivalIntendedAirport', $r)) {
                    $newArrivalIntendedAirport = [
                        'uuid' => $r['ArrivalIntendedAirport']['Id'],
                        'icao' => $r['ArrivalIntendedAirport']['ICAO'],
                        'has_no_runways' => $r['ArrivalIntendedAirport']['HasNoRunways'],
                        'time_offset_in_sec' => (array_key_exists('TimeOffsetInSec', $r['ArrivalIntendedAirport'])) ? $r['ArrivalIntendedAirport']['TimeOffsetInSec'] : null,
                        'local_time_open_in_hours_since_midnight' => (array_key_exists('LocalTimeOpenInHoursSinceMidnight', $r['ArrivalIntendedAirport'])) ? $r['ArrivalIntendedAirport']['LocalTimeOpenInHoursSinceMidnight'] : null,
                        'local_time_close_in_hours_since_midnight' => (array_key_exists('LocalTimeCloseInHoursSinceMidnight', $r['ArrivalIntendedAirport'])) ? $r['ArrivalIntendedAirport']['LocalTimeCloseInHoursSinceMidnight'] : null,
                        'iata' => (array_key_exists('IATA', $r['ArrivalIntendedAirport'])) ? $r['ArrivalIntendedAirport']['IATA'] : null,
                        'name' => $r['ArrivalIntendedAirport']['Name'],
                        'state' => (array_key_exists('State', $r['ArrivalIntendedAirport'])) ? $r['ArrivalIntendedAirport']['State']: null,
                        'country_code' => (array_key_exists('CountryCode', $r['ArrivalIntendedAirport'])) ? $r['ArrivalIntendedAirport']['CountryCode']: null,
                        'country_name' => (array_key_exists('CountryName', $r['ArrivalIntendedAirport'])) ? $r['ArrivalIntendedAirport']['CountryName']: null,
                        'city' => $r['ArrivalIntendedAirport']['City'],
                        'latitude' => $r['ArrivalIntendedAirport']['Latitude'],
                        'longitude' => $r['ArrivalIntendedAirport']['Longitude'],
                        'elevation' => $r['ArrivalIntendedAirport']['Elevation'],
                        'has_land_runway' => $r['ArrivalIntendedAirport']['HasLandRunway'],
                        'has_water_runway' => $r['ArrivalIntendedAirport']['HasWaterRunway'],
                        'has_helipad' => $r['ArrivalIntendedAirport']['HasHelipad'],
                        'size' => $r['ArrivalIntendedAirport']['Size'],
                        'transition_altitude' => $r['ArrivalIntendedAirport']['TransitionAltitude'],
                        'is_not_in_vanilla_fsx' => $r['ArrivalIntendedAirport']['IsNotInVanillaFSX'],
                        'is_not_in_vanilla_p3d' => $r['ArrivalIntendedAirport']['IsNotInVanillaP3D'],
                        'is_not_in_vanilla_xplane' => $r['ArrivalIntendedAirport']['IsNotInVanillaXPLANE'],
                        'is_not_in_vanilla_fs2020' => $r['ArrivalIntendedAirport']['IsNotInVanillaFS2020'],
                        'is_closed' => $r['ArrivalIntendedAirport']['IsClosed'],
                        'is_valid' => $r['ArrivalIntendedAirport']['IsValid'],
                        'mag_var' => $r['ArrivalIntendedAirport']['MagVar'],
                        'is_addon' => $r['ArrivalIntendedAirport']['IsAddon'],
                        'random_seed' => $r['ArrivalIntendedAirport']['RandomSeed'],
                        'last_random_seed_generation' => (array_key_exists('ArrivalIntendedAirport', $r['ArrivalIntendedAirport'])) ? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastRandomSeedGeneration']) : null,
                        'is_military' => $r['ArrivalIntendedAirport']['IsMilitary'],
                        'has_lights' => $r['ArrivalIntendedAirport']['HasLights'],
                        'airport_source' => $r['ArrivalIntendedAirport']['AirportSource'],
                        'last_very_short_request_date' => (array_key_exists('LastVeryShortRequestDate', $r['ArrivalIntendedAirport'])) ? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastVeryShortRequestDate']) : null,
                        'last_small_trip_request_date' => (array_key_exists('LastSmallTripRequestDate', $r['ArrivalIntendedAirport'])) ? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastSmallTripRequestDate']) : null,
                        'last_medium_trip_request_date' => (array_key_exists('LastMediumTripRequestDate', $r['ArrivalIntendedAirport']) )? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastMediumTripRequestDate']) : null,
                        'last_short_haul_request_date' => (array_key_exists('LastShortHaulRequestDate', $r['ArrivalIntendedAirport'])) ? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastShortHaulRequestDate']) : null,
                        'last_medium_haul_request_date' => (array_key_exists('LastMediumHaulRequestDate', $r['ArrivalIntendedAirport']) )? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastMediumHaulRequestDate']) : null,
                        'last_long_haul_request_date' => (array_key_exists('LastLongHaulRequestDate', $r['ArrivalIntendedAirport'])) ? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastLongHaulRequestDate']) : null,
                        'display_name' => $r['ArrivalIntendedAirport']['DisplayName'],
                        'utc_time_open_in_hours_since_midnight' => $r['ArrivalIntendedAirport']['UTCTimeOpenInHoursSinceMidnight'],
                        'utc_time_close_in_hours_since_midnight' => $r['ArrivalIntendedAirport']['UTCTimeCloseInHoursSinceMidnight'],
                    ];

                    $arrivalIntendedAirport = Airport::updateOrCreate([
                        'uuid' => $r['ArrivalIntendedAirportId']
                    ], $newArrivalIntendedAirport);
                }

                // ensure ArrivalActualAirport exists
                if (array_key_exists('ArrivalActualAirport', $r)) {
                    $newArrivalActualAirport = [
                        'uuid' => $r['ArrivalActualAirport']['Id'],
                        'icao' => $r['ArrivalActualAirport']['ICAO'],
                        'has_no_runways' => $r['ArrivalActualAirport']['HasNoRunways'],
                        'time_offset_in_sec' => (array_key_exists('TimeOffsetInSec', $r['ArrivalActualAirport'])) ? $r['ArrivalActualAirport']['TimeOffsetInSec'] : null,
                        'local_time_open_in_hours_since_midnight' => (array_key_exists('LocalTimeOpenInHoursSinceMidnight', $r['ArrivalActualAirport'])) ? $r['ArrivalActualAirport']['LocalTimeOpenInHoursSinceMidnight'] : null,
                        'local_time_close_in_hours_since_midnight' => (array_key_exists('LocalTimeCloseInHoursSinceMidnight', $r['ArrivalActualAirport'])) ? $r['ArrivalActualAirport']['LocalTimeCloseInHoursSinceMidnight'] : null,
                        'iata' => (array_key_exists('IATA', $r['ArrivalActualAirport'])) ? $r['ArrivalActualAirport']['IATA'] : null,
                        'name' => $r['ArrivalActualAirport']['Name'],
                        'state' => (array_key_exists('State', $r['ArrivalActualAirport'])) ? $r['ArrivalActualAirport']['State']: null,
                        'country_code' => (array_key_exists('CountryCode', $r['ArrivalActualAirport'])) ? $r['ArrivalActualAirport']['CountryCode']: null,
                        'country_name' => (array_key_exists('CountryName', $r['ArrivalActualAirport'])) ? $r['ArrivalActualAirport']['CountryName']: null,
                        'city' => $r['ArrivalActualAirport']['City'],
                        'latitude' => $r['ArrivalActualAirport']['Latitude'],
                        'longitude' => $r['ArrivalActualAirport']['Longitude'],
                        'elevation' => $r['ArrivalActualAirport']['Elevation'],
                        'has_land_runway' => $r['ArrivalActualAirport']['HasLandRunway'],
                        'has_water_runway' => $r['ArrivalActualAirport']['HasWaterRunway'],
                        'has_helipad' => $r['ArrivalActualAirport']['HasHelipad'],
                        'size' => $r['ArrivalActualAirport']['Size'],
                        'transition_altitude' => $r['ArrivalActualAirport']['TransitionAltitude'],
                        'is_not_in_vanilla_fsx' => $r['ArrivalActualAirport']['IsNotInVanillaFSX'],
                        'is_not_in_vanilla_p3d' => $r['ArrivalActualAirport']['IsNotInVanillaP3D'],
                        'is_not_in_vanilla_xplane' => $r['ArrivalActualAirport']['IsNotInVanillaXPLANE'],
                        'is_not_in_vanilla_fs2020' => $r['ArrivalActualAirport']['IsNotInVanillaFS2020'],
                        'is_closed' => $r['ArrivalActualAirport']['IsClosed'],
                        'is_valid' => $r['ArrivalActualAirport']['IsValid'],
                        'mag_var' => $r['ArrivalActualAirport']['MagVar'],
                        'is_addon' => $r['ArrivalActualAirport']['IsAddon'],
                        'random_seed' => $r['ArrivalActualAirport']['RandomSeed'],
                        'last_random_seed_generation' => (array_key_exists('ArrivalActualAirport', $r['ArrivalActualAirport'])) ? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastRandomSeedGeneration']) : null,
                        'is_military' => $r['ArrivalActualAirport']['IsMilitary'],
                        'has_lights' => $r['ArrivalActualAirport']['HasLights'],
                        'airport_source' => $r['ArrivalActualAirport']['AirportSource'],
                        'last_very_short_request_date' => (array_key_exists('LastVeryShortRequestDate', $r['ArrivalActualAirport'])) ? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastVeryShortRequestDate']) : null,
                        'last_small_trip_request_date' => (array_key_exists('LastSmallTripRequestDate', $r['ArrivalActualAirport'])) ? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastSmallTripRequestDate']) : null,
                        'last_medium_trip_request_date' => (array_key_exists('LastMediumTripRequestDate', $r['ArrivalActualAirport']) )? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastMediumTripRequestDate']) : null,
                        'last_short_haul_request_date' => (array_key_exists('LastShortHaulRequestDate', $r['ArrivalActualAirport'])) ? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastShortHaulRequestDate']) : null,
                        'last_medium_haul_request_date' => (array_key_exists('LastMediumHaulRequestDate', $r['ArrivalActualAirport']) )? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastMediumHaulRequestDate']) : null,
                        'last_long_haul_request_date' => (array_key_exists('LastLongHaulRequestDate', $r['ArrivalActualAirport'])) ? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastLongHaulRequestDate']) : null,
                        'display_name' => $r['ArrivalActualAirport']['DisplayName'],
                        'utc_time_open_in_hours_since_midnight' => $r['ArrivalActualAirport']['UTCTimeOpenInHoursSinceMidnight'],
                        'utc_time_close_in_hours_since_midnight' => $r['ArrivalActualAirport']['UTCTimeCloseInHoursSinceMidnight'],
                    ];

                    $arrivalActualAirport = Airport::updateOrCreate([
                        'uuid' => $r['ArrivalActualAirportId']
                    ], $newArrivalActualAirport);
                }

                $aircraftUuId = $r['AircraftId'];
                $newAircraft = [
                    'uuid' => $r['Id'],
                    'current_airport_id' => (isset($CurrentAirport)) ? $CurrentAirport->id : null,
                    'aircraft_status_id' => (isset($AircraftStatus)) ? $AircraftStatus->id : null,
                    'last_status_change' => $r['LastStatusChange'],
                    'current_status_duration_in_minutes' => $r['CurrentStatusDurationInMinutes'],
                    'allow_sell' => $r['AllowSell'],
                    'allow_rent' => $r['AllowRent'],
                    'sell_price' => $r['SellPrice'],
                    'rent_hour_price' => $r['RentHourPrice'],
                    'rent_airport_id' => (isset($RentAirport)) ? $RentAirport->id : null,
                    'rent_fuel_total_gallons' => (array_key_exists('RentFuelTotalGallons', $r)) ? $r['RentFuelTotalGallons'] : null,
                    'rent_caution_amount' => (array_key_exists('RentCautionAmount', $r)) ? $r['RentCautionAmount'] : null,
                    'rent_start_date' => (array_key_exists('RentStartDate', $r)) ? $r['RentStartDate'] : null,
                    'rent_last_daily_charge_date' => (array_key_exists('RentLastDailyChargeDate', $r)) ? $r['RentLastDailyChargeDate'] : null,
                    'identifier' => $r['Identifier'],
                    'heading' => $r['Heading'],
                    'longitude' => $r['Longitude'],
                    'latitude' => $r['Latitude'],
                    'fuel_total_gallons' => $r['fuelTotalGallons'],
                    'fuel_weight' => $r['fuelWeight'],
                    'loaded_weight' => $r['loadedWeight'],
                    'zero_fuel_weight' => $r['zeroFuelWeight'],
                    'airframe_hours' => $r['airframeHours'],
                    'airframe_condition' => $r['airframeCondition'],
                    'last_annual_checkup' => $r['LastAnnualCheckup'],
                    'last_100h_inspection' => $r['Last100hInspection'],
                    'last_weekly_ownership_payment' => $r['LastWeeklyOwnershipPayment'],
                    'last_parking_fee_payment' => $r['LastParkingFeePayment'],
                    'is_controlled_by_ai' => $r['IsControlledByAI'],
                    'hours_before_100h_inspection' => $r['HoursBefore100HInspection'],
                    'extra_weight_capacity' => $r['ExtraWeightCapacity'],
                    'total_weight_capacity' => $r['TotalWeightCapacity'],
                    'must_do_maintenance' => $r['MustDoMaintenance'],
                    'aircraft_type_id' => (isset($AircraftType)) ? $AircraftType->id : null,
                    'current_seats' => $r['CurrentSeats'],
                    'config_seats_first' =>(array_key_exists('ConfigFirstSeats', $r)) ?  (int) $r['ConfigFirstSeats'] : null,
                    'config_seats_bus' => (array_key_exists('ConfigBusSeats', $r)) ? (int) $r['ConfigBusSeats'] : null,
                    'config_seats_eco' => (array_key_exists('ConfigEcoSeats', $r)) ? (int) $r['ConfigEcoSeats'] : null,
                    'company_id' => $companyId,
                    'is_rented' => $IsRented,
                    'is_owned' => $IsOwned,
                ];

                var_dump($newAircraft);

                $Aircraft = Aircraft::updateOrCreate([
                    'uuid' => $r['Id'],
                ], $newAircraft);

                $Aircraft
                  ->company()->associate($company);

                if (isset($AircraftType)) {
                    $Aircraft
                      ->aircraft_type()->associate($AircraftType);
                }

                if (isset($CurrentAirport)) {
                    $Aircraft
                      ->current_airport()->associate($CurrentAirport);
                }

                if (isset($RentAirport)) {
                    $Aircraft
                      ->rent_airport()->associate($RentAirport);
                }

                $Aircraft->save();

                if ($aircraft['id' == null]) {
                    dd($aircraft);
                }

                $aircraftId = $aircraft->id;
                /// reformat newFlight into compatible syntax
                $newFlight = [
                    'uuid' => $r['Id'],
                    'aircraft_id' => $aircraftId,
                    'company_id' => $companyId,
                    'departure_airport_id' => (isset($departureAirport)) ? $departureAirport->id : null,
                    'arrival_intended_airport_id' => (isset($arrivalIntendedAirport)) ? $arrivalIntendedAirport->id : null,
                    'arrival_actual_airport_id' => (isset($arrivalActualAirport)) ? $arrivalActualAirport->id : null,
                    'registered' => $r['Registered'],
                    'category' => $r['Category'],
                    'result_comments' => (array_key_exists('ResultComments', $r)) ? $r['ResultComments'] : null,
                    'start_time' => $r['StartTime'],
                    'engine_on_time' => (array_key_exists('EngineOnTime', $r)) ? \Carbon\Carbon::parse($r['EngineOnTime']) : null,
                    'engine_off_time' => (array_key_exists('EngineOffTime', $r)) ? \Carbon\Carbon::parse($r['EngineOffTime']) : null,
                    'airborne_time' => (array_key_exists('AirborneTime', $r)) ? \Carbon\Carbon::parse($r['AirborneTime']) : null,
                    'landed_time' => (array_key_exists('LandedTime', $r)) ? \Carbon\Carbon::parse($r['LandedTime']) : null,
                    'intended_flight_level' => $r['IntendedFlightLevel'],
                    'passengers' => $r['Passengers'],
                    'cargo' => $r['Cargo'],
                    'added_fuel_qty' => $r['AddedFuelQty'],
                    'is_ai' => $r['IsAI'],
                    'vertical_speed_at_touchdown_mps' => $r['VerticalSpeedAtTouchdownMpS'],
                    'maxg_force' => $r['MaxGForce'],
                    'ming_force' => $r['MinGForce'],
                    'max_bank' => $r['MaxBank'],
                    'max_pitch' => $r['MaxPitch'],
                    'has_stalled' => $r['HasStalled'],
                    'has_overspeeded' => $r['HasOverspeeded'],
                    'engine1_status' => $r['Engine1Status'],
                    'engine2_status' => $r['Engine2Status'],
                    'engine3_status' => $r['Engine3Status'],
                    'engine4_status' => $r['Engine4Status'],
                    'engine5_status' => $r['Engine5Status'],
                    'engine6_status' => $r['Engine6Status'],
                    'xp_flight' => $r['XPFlight'],
                    'xp_flight_bonus' => $r['XPFlightBonus'],
                    'xp_missions' => $r['XPMissions'],
                    'cargos_total_weight' => $r['CargosTotalWeight'],
                    'pax_count' => $r['PAXCount'],
                    'aircraft_current_fob' => $r['AircraftCurrentFOB'],
                    'aircraft_current_altitude' => $r['AircraftCurrentAltitude'],
                    'actual_cruise_altitude' => $r['ActualCruiseAltitude'],
                    'actual_consumption_at_cruise_level_in_lbsperhour' => $r['ActualConsumptionAtCruiseLevelInLbsPerHour'],
                    'actual_total_fuel_consumption_inlbs' => $r['ActualTotalFuelConsumptionInLbs'],
                    'actual_consumption_at_cruise_level_in_galperhour' => $r['ActualConsumptionAtCruiseLevelInGalPerHour'],
                    'actual_total_fuel_consumptioningal' => (array_key_exists('ActualTotalFuelConsumptionInGal', $r)) ? $r['ActualTotalFuelConsumptionInGal'] : null,
                    'actual_tas_at_cruise_level' => $r['ActualTASAtCruiseLevel'],
                    'actual__cruise_time_in_minutes' => $r['ActualCruiseTimeInMinutes'],
                    'actual_pressure_altitude' => $r['ActualPressureAltitude'],
                    'register_state' => $r['RegisterState'],
                    'wrong_fuel_detected' => $r['WrongFuelDetected'],
                    'wrong_weight_detected' => $r['WrongWeightDetected'],
                    'time_offset' => (array_key_exists('TimeOffset', $r)) ? $r['TimeOffset'] : null,
                    'can_resume_or_abort' => $r['CanResumeOrAbort'],
                    'engine_on_real_time' => (array_key_exists('EngineOnRealTime', $r)) ? \Carbon\Carbon::parse($r['EngineOnRealTime']) : null,
                    'engine_off_real_time' => (array_key_exists('EngineOffRealTime', $r)) ? \Carbon\Carbon::parse($r['EngineOffRealTime']) : null,
                ];

                // find the flight if it exists, or create if not
                $flight = Flight::updateOrCreate([
                    'uuid' => $newFlight['uuid']
                ], $newFlight)
                    ->save();


                if (is_object($flight) && $flight->wasRecentlyCreated) {
                    $this->created++;
                } else if ($flight === true) {
                    $this->updated++;
                }

            }
        }

        $this->logStats();
        return 0;
    }
}
