<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;
use App\Models\Config;
use App\Services\OnAirApiService;

abstract class OnAirCommand extends Command
{
    protected $OnAirService;

    public $apiKey;
    public $companyId;
    public $world;
    public $baseURL;
    public $updated = 0;
    public $created = 0;
    public $logSchedulesToFile = true;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function logStart()
    {
        if ($this->logSchedulesToFile === true) {
            $this->info("$this->signature started");
        }
    }

    protected function makeRequest($world, $api_key, $endPoint)
    {
        echo "world: ${world}, endPoint: ${endPoint}, api_key: ${api_key}\n";
        $url = $this->makeUrl($world, $endPoint);
        echo "url: ${url}\n";

        $response = Http::withHeaders([
            'oa-apikey' => $api_key
        ])->get($url)->json()['Content'];

        return $response;
    }

    protected function makeUrl($world, $endPoint)
    {
        $endPoint = (strpos($endPoint, '/') === 0) ? $endPoint : '/'.$endPoint;

        if ($world === 'clear-sky') {
            $world = 'stratus';
        }

        $url = 'https://'.$world.'.onair.company/api/v1'.$endPoint;

        return $url;
    }

    public function logStats($updated, $created)
    {
        if ($this->logSchedulesToFile === true) {
            $this->info("$this->signature completed");
            $this->info("  Run statistics");
            $this->info("    Records Updated: $updated");
            $this->info("    Records Created: $created");
        }
    }

    public function logStop()
    {
        if ($this->logSchedulesToFile === true) {
            $this->info("$this->signature finished");
        }
    }
}
