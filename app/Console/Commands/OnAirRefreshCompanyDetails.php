<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Company;
use App\Services\OnAirCompanyService;

class OnAirRefreshCompanyDetails extends OnAirCommand
{
/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'onair:refreshcompanydetails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refreshes/Synchronizes the OnAir Company Details';


    public $OnAirService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(OnAirCompanyService $onAirCompanyService)
    {
        $this->logStart();

        $response = $onAirCompanyService->refresh();

        $this->logStats($response['updated'], $response['created']);

        return 0;
    }
}
