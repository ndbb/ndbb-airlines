<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Company;
use App\Models\Aircraft;
use App\Models\AircraftClass;
use App\Models\Employee;
use App\Models\ClassCertification;
use App\Models\Airport;
use App\Services\OnAirApiService;

class OnAirRefreshCompanyEmployees extends OnAirCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'onair:refreshemployees';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refreshes/synchronizes the OnAir Employees';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OnAirApiService $onAirService)
    {
        parent::__construct($onAirService);

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->logStart();

        $companies = Company::with(['world'])->where('sync_fleet', true)->get();

        foreach ($companies as $key => $company) {
            $companyId = $company->id;
            $companyUuId = $company->uuid;
            $api_key = $company->api_key;
            $world = $company->world;
            $response = $this->makeRequest($world->slug, $api_key, '/company/'.$companyUuId.'/employees');

            $employees = [];

            foreach ($response as $key => $r) {
                $certifications = $r['ClassCertifications'] ;
                $newCertifications = [];
                $currentAirportId = null;
                $homeAirportId = null;

                if (array_key_exists('CurrentAirport', $r)) {
                    $newCurrentAirport = [
                        'uuid' => $r['CurrentAirport']['Id'],
                        'icao' => $r['CurrentAirport']['ICAO'],
                        'has_no_runways' => $r['CurrentAirport']['HasNoRunways'],
                        'time_offset_in_sec' => $r['CurrentAirport']['TimeOffsetInSec'],
                        'local_time_open_in_hours_since_midnight' => $r['CurrentAirport']['LocalTimeOpenInHoursSinceMidnight'],
                        'local_time_close_in_hours_since_midnight' => $r['CurrentAirport']['LocalTimeCloseInHoursSinceMidnight'],
                        'iata' => (array_key_exists('IATA', $r['CurrentAirport'])) ? $r['CurrentAirport']['IATA'] : null,
                        'name' => $r['CurrentAirport']['Name'],
                        'state' => (array_key_exists('State', $r['CurrentAirport'])) ? $r['CurrentAirport']['State'] : null,
                        'country_code' => (array_key_exists('CountryCode', $r['CurrentAirport'])) ? $r['CurrentAirport']['CountryCode'] : null,
                        'country_name' => (array_key_exists('CountryName', $r['CurrentAirport'])) ? $r['CurrentAirport']['CountryName'] : null,
                        'city' => (array_key_exists('City', $r['CurrentAirport'])) ? $r['CurrentAirport']['City'] : null,
                        'latitude' => $r['CurrentAirport']['Latitude'],
                        'longitude' => $r['CurrentAirport']['Longitude'],
                        'elevation' => $r['CurrentAirport']['Elevation'],
                        'has_land_runway' => $r['CurrentAirport']['HasLandRunway'],
                        'has_water_runway' => $r['CurrentAirport']['HasWaterRunway'],
                        'has_helipad' => $r['CurrentAirport']['HasHelipad'],
                        'size' => $r['CurrentAirport']['Size'],
                        'transition_altitude' => $r['CurrentAirport']['TransitionAltitude'],
                        'is_not_in_vanilla_fsx' => $r['CurrentAirport']['IsNotInVanillaFSX'],
                        'is_not_in_vanilla_p3d' => $r['CurrentAirport']['IsNotInVanillaP3D'],
                        'is_not_in_vanilla_xplane' => $r['CurrentAirport']['IsNotInVanillaXPLANE'],
                        'is_not_in_vanilla_fs2020' => $r['CurrentAirport']['IsNotInVanillaFS2020'],
                        'is_closed' => $r['CurrentAirport']['IsClosed'],
                        'is_valid' => $r['CurrentAirport']['IsValid'],
                        'mag_var' => $r['CurrentAirport']['MagVar'],
                        'is_addon' => $r['CurrentAirport']['IsAddon'],
                        'random_seed' => $r['CurrentAirport']['RandomSeed'],
                        'last_random_seed_generation' => $r['CurrentAirport']['LastRandomSeedGeneration'],
                        'is_military' => $r['CurrentAirport']['IsMilitary'],
                        'has_lights' => $r['CurrentAirport']['HasLights'],
                        'airport_source' => $r['CurrentAirport']['AirportSource'],
                        'last_very_short_request_date' => (array_key_exists('LastVeryShortRequestDate', $r['CurrentAirport'])) ? $r['CurrentAirport']['LastVeryShortRequestDate'] : null,
                        'last_small_trip_request_date' => (array_key_exists('LastSmallTripRequestDate', $r['CurrentAirport'])) ? $r['CurrentAirport']['LastSmallTripRequestDate'] : null,
                        'last_medium_trip_request_date' => (array_key_exists('LastMediumTripRequestDate', $r['CurrentAirport'])) ? $r['CurrentAirport']['LastMediumTripRequestDate'] : null,
                        'last_short_haul_request_date' => (array_key_exists('LastShortHaulRequestDate', $r['CurrentAirport'])) ? $r['CurrentAirport']['LastShortHaulRequestDate'] : null,
                        'last_medium_haul_request_date' => (array_key_exists('LastMediumHaulRequestDate', $r['CurrentAirport'])) ? $r['CurrentAirport']['LastMediumHaulRequestDate'] : null,
                        'last_long_haul_request_date' => (array_key_exists('LastLongHaulRequestDate', $r['CurrentAirport'])) ? $r['CurrentAirport']['LastLongHaulRequestDate'] : null,
                        'display_name' => $r['CurrentAirport']['DisplayName'],
                        'utc_time_open_in_hours_since_midnight' => $r['CurrentAirport']['UTCTimeOpenInHoursSinceMidnight'],
                        'utc_time_close_in_hours_since_midnight' => $r['CurrentAirport']['UTCTimeCloseInHoursSinceMidnight'],
                    ];

                    $currentAirport = Airport::updateOrCreate([
                        'uuid' => $newCurrentAirport['uuid']
                    ], $newCurrentAirport);
                    $currentAirportId = $currentAirport->id;
                }

                if (array_key_exists('Id', $r['HomeAirport'])) {
                    $newHomeAirport = [
                        'uuid' => $r['HomeAirport']['Id'],
                        'icao' => $r['HomeAirport']['ICAO'],
                        'has_no_runways' => $r['HomeAirport']['HasNoRunways'],
                        'time_offset_in_sec' => $r['HomeAirport']['TimeOffsetInSec'],
                        'local_time_open_in_hours_since_midnight' => $r['HomeAirport']['LocalTimeOpenInHoursSinceMidnight'],
                        'local_time_close_in_hours_since_midnight' => $r['HomeAirport']['LocalTimeCloseInHoursSinceMidnight'],
                        'iata' => (array_key_exists('IATA', $r['HomeAirport'])) ? $r['HomeAirport']['IATA'] : null,
                        'name' => $r['HomeAirport']['Name'],
                        'state' => (array_key_exists('State', $r['HomeAirport'])) ? $r['HomeAirport']['State'] : null,
                        'country_code' => (array_key_exists('CountryCode', $r['HomeAirport'])) ? $r['HomeAirport']['CountryCode'] : null,
                        'country_name' => (array_key_exists('CountryName', $r['HomeAirport'])) ? $r['HomeAirport']['CountryName'] : null,
                        'city' => (array_key_exists('City', $r['HomeAirport'])) ? $r['HomeAirport']['City'] : null,
                        'latitude' => $r['HomeAirport']['Latitude'],
                        'longitude' => $r['HomeAirport']['Longitude'],
                        'elevation' => $r['HomeAirport']['Elevation'],
                        'has_land_runway' => $r['HomeAirport']['HasLandRunway'],
                        'has_water_runway' => $r['HomeAirport']['HasWaterRunway'],
                        'has_helipad' => $r['HomeAirport']['HasHelipad'],
                        'size' => $r['HomeAirport']['Size'],
                        'transition_altitude' => $r['HomeAirport']['TransitionAltitude'],
                        'is_not_in_vanilla_fsx' => $r['HomeAirport']['IsNotInVanillaFSX'],
                        'is_not_in_vanilla_p3d' => $r['HomeAirport']['IsNotInVanillaP3D'],
                        'is_not_in_vanilla_xplane' => $r['HomeAirport']['IsNotInVanillaXPLANE'],
                        'is_not_in_vanilla_fs2020' => $r['HomeAirport']['IsNotInVanillaFS2020'],
                        'is_closed' => $r['HomeAirport']['IsClosed'],
                        'is_valid' => $r['HomeAirport']['IsValid'],
                        'mag_var' => $r['HomeAirport']['MagVar'],
                        'is_addon' => $r['HomeAirport']['IsAddon'],
                        'random_seed' => $r['HomeAirport']['RandomSeed'],
                        'last_random_seed_generation' => $r['HomeAirport']['LastRandomSeedGeneration'],
                        'is_military' => $r['HomeAirport']['IsMilitary'],
                        'has_lights' => $r['HomeAirport']['HasLights'],
                        'airport_source' => $r['HomeAirport']['AirportSource'],
                        (array_key_exists('EngineOnRealTime', $r)) ? \Carbon\Carbon::parse($r['EngineOnRealTime']) : null,
                        'last_very_short_request_date' => (array_key_exists('LastVeryShortRequestDate', $r['HomeAirport'])) ? $r['HomeAirport']['LastVeryShortRequestDate'] : null,
                        'last_small_trip_request_date' => (array_key_exists('LastSmallTripRequestDate', $r['HomeAirport'])) ? $r['HomeAirport']['LastSmallTripRequestDate'] : null,
                        'last_medium_trip_request_date' => (array_key_exists('LastMediumTripRequestDate', $r['HomeAirport'])) ? $r['HomeAirport']['LastMediumTripRequestDate'] : null,
                        'last_short_haul_request_date' => (array_key_exists('LastShortHaulRequestDate', $r['HomeAirport'])) ? $r['HomeAirport']['LastShortHaulRequestDate'] : null,
                        'last_medium_haul_request_date' => (array_key_exists('LastMediumHaulRequestDate', $r['HomeAirport'])) ? $r['HomeAirport']['LastMediumHaulRequestDate'] : null,
                        'last_long_haul_request_date' => (array_key_exists('LastLongHaulRequestDate', $r['HomeAirport'])) ? $r['HomeAirport']['LastLongHaulRequestDate'] : null,
                        'display_name' => $r['HomeAirport']['DisplayName'],
                        'utc_time_open_in_hours_since_midnight' => $r['HomeAirport']['UTCTimeOpenInHoursSinceMidnight'],
                        'utc_time_close_in_hours_since_midnight' => $r['HomeAirport']['UTCTimeCloseInHoursSinceMidnight'],
                    ];

                    $homeAirport = Airport::updateOrCreate([
                        'uuid' => $newHomeAirport['uuid']
                    ], $newHomeAirport);
                    $homeAirportId = $homeAirport->id;
                }

                $newEmployee = [
                    'uuid' => $r['Id'],
                    'psuedo_name' => $r['Pseudo'],
                    'company_id' => $companyId,
                    'flight_hours_total_before_hiring' => $r['FlightHoursTotalBeforeHiring'],
                    'flight_hours_in_company' => $r['FlightHoursInCompany'],
                    'weight' => $r['Weight'],
                    'birth_date' => $r['BirthDate'],
                    'fatigue' => $r['Fatigue'],
                    'punctuality' => $r['Punctuality'],
                    'comfort' => $r['Comfort'],
                    'happiness' => $r['Happiness'],
                    'home_airport_uuid' => $r['HomeAirportId'],
                    'home_airport_id' => $homeAirportId,
                    'current_airport_uuid' => (array_key_exists('CurrentAirportId', $r)) ? $r['CurrentAirportId'] : null,
                    'current_airport_id' => $currentAirportId,
                    'per_flight_hour_wages' => $r['PerFlightHourWages'],
                    'weekly_garanted_salary' => $r['WeeklyGarantedSalary'],
                    'per_fligh_thour_salary' => $r['PerFlightHourSalary'],
                    'category' => $r['Category'],
                    'status' => $r['Status'],
                    'last_status_change' => $r['LastStatusChange'],
                    'current_total_flight_hours_in_duty' => $r['CurrentTotalFlightHoursInDuty'],
                    'freelance_since' => (array_key_exists('FreelanceSince', $r)) ? $r['FreelanceSince'] : null,
                    'freelance_until' => (array_key_exists('FreelanceUntil', $r)) ? $r['FreelanceUntil'] : null,
                    'last_payment_date' => (array_key_exists('LastPaymentDate', $r)) ? $r['LastPaymentDate'] : null,
                    'is_online' => $r['IsOnline'],
                    'flight_hours_grand_total' => $r['FlightHoursGrandTotal']
                ];

                $employee = Employee::updateOrCreate([
                    'uuid' => $newEmployee['uuid']
                ], $newEmployee);

                $employeeId = $employee->id;

                foreach ($certifications as $key => $c) {
                    // create the aircraftClassification if it doesn't exist
                    $newAircraftClass = [
                        'uuid' => $c['AircraftClass']['Id'],
                        'short_name' => $c['AircraftClass']['ShortName'],
                        'name' => $c['AircraftClass']['Name'],
                        'order' => $c['AircraftClass']['Order'],
                    ];

                    $aircraftClass = AircraftClass::updateOrCreate([
                        'uuid' => $newAircraftClass['uuid']
                    ], $newAircraftClass);

                    // create the EmployeeClassCertification if it doesn't exist
                    $newCertification = [
                        'uuid' => $c['Id'],
                        'employee_uuid' => $c['PeopleId'],
                        'employee_id' => $employeeId,
                        'aircraft_class_id' => $aircraftClass->id,
                        'last_validation' => $c['LastValidation'],
                        'comments' => (array_key_exists('Comments', $c)) ? $c['Comments'] : null,
                    ];

                    $newCertification = ClassCertification::updateOrCreate([
                        'uuid' => $newCertification['uuid']
                    ], $newCertification);

                    array_push($newCertifications, $newCertification);
                }

                if (is_object($employee) && $employee->wasRecentlyCreated) {
                    $this->created++;
                } else if ($employee === true) {
                    $this->updated++;
                }
            }

        }

        $this->logStats();
        return 0;
    }

}
