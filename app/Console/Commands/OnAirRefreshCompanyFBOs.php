<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\OnAirApiService;

class OnAirRefreshCompanyFBOs extends OnAirCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'onair:refreshfbos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OnAirApiService $onAirService)
    {
        parent::__construct($onAirService);

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->logStart();

        $this->logStats();
        return 0;
    }
}
