<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    use HasFactory;

    protected $fillable = [
        'apiKey',
        'companyId',
        'world',
        'baseURL',
        'logSchedulesToFile',
        'companyName',
    ];
}
