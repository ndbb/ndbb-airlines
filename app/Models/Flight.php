<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Company;
use App\Models\Airport;

class Flight extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'aircraft_id',
        'company_id',
        'departure_airport_id',
        'arrival_intended_airport_id',
        'arrival_actual_airport_id',
        'registered',
        'category',
        'result_comments',
        'start_time',
        'engine_on_time',
        'engine_off_time',
        'airborne_time',
        'landed_time',
        'intended_flight_level',
        'passengers',
        'cargo',
        'added_fuel_qty',
        'is_ai',
        'vertical_speed_at_touchdown_mps',
        'maxg_force',
        'ming_force',
        'max_bank',
        'max_pitch',
        'has_stalled',
        'has_overspeeded',
        'engine1_status',
        'engine2_status',
        'engine3_status',
        'engine4_status',
        'engine5_status',
        'engine6_status',
        'xp_flight',
        'xp_flight_bonus',
        'xp_missions',
        'cargos_total_weight',
        'pax_count',
        'aircraft_current_fob',
        'aircraft_current_altitude',
        'actual_cruise_altitude',
        'actual_consumption_at_cruise_level_in_lbsperhour',
        'actual_total_fuel_consumption_inlbs',
        'actual_consumption_at_cruise_level_in_galperhour',
        'actual_total_fuel_consumptioningal',
        'actual_tas_at_cruise_level',
        'actual__cruise_time_in_minutes',
        'actual_pressure_altitude',
        'register_state',
        'wrong_fuel_detected',
        'wrong_weight_detected',
        'time_offset',
        'can_resume_or_abort',
        'engine_on_real_time',
        'engine_off_real_time',
    ];

    public function aircraft()
    {
        return $this->belongsTo(Aircraft::class, 'aircraft_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function departure_airport()
    {
        return $this->hasOne(Airport::class, 'id', 'departure_airport_id');
    }

    public function arrival_intended_airport()
    {
        return $this->hasOne(Airport::class, 'id', 'arrival_intended_airport_id');
    }

    public function arrival_actual_airport()
    {
        return $this->hasOne(Airport::class, 'id', 'arrival_actual_airport_id');
    }

}
