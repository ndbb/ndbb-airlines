<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Airport;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'psuedo_name',
        'company_id',
        'flight_hours_total_before_hiring',
        'flight_hours_in_company',
        'weight',
        'birth_date',
        'fatigue',
        'punctuality',
        'comfort',
        'happiness',
        'per_flight_hour_wages',
        'weekly_garanted_salary',
        'per_fligh_thour_salary',
        'category',
        'status',
        'last_status_change',
        'current_total_flight_hours_in_duty',
        'freelance_since',
        'freelance_until',
        'last_payment_date',
        'is_online',
        'flight_hours_grand_total',
        'current_airport_id',
        'home_airport_id',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function certifications()
    {
        return $this->hasMany(ClassCertification::class, 'employee_id', 'id');
    }

    public function current_airport()
    {
        return $this->belongsTo(Airport::class, 'current_airport_id', 'id');
    }

    public function home_airport()
    {
        return $this->belongsTo(Airport::class, 'home_airport_id', 'id');
    }

}
