<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Company;
use App\Models\Airport;
use App\Models\Aircraft;
use App\Models\AircraftType;
use App\Models\AircraftClass;
use App\Models\Employee;
use App\Models\Flight;
use App\Models\ClassCertification;

class OnAirController extends Controller
{

    private function makeRequest($url)
    {
        $apiKey = env('ONAIR_APIKEY');
        $response = Http::withHeaders([
            'oa-apikey' => $apiKey
        ])->get($url)->json()['Content'];

        return $response;
    }

    private function makeUrl($endPoint)
    {
        $endPoint = (strpos($endPoint, '/') === 0) ? $endPoint : '/'.$endPoint;
        $baseUrl = env('ONAIR_BASEURL');
        $world = env('ONAIR_WORLD');

        $url = 'https://'.$world.'.'.$baseUrl.$endPoint;

        return $url;
    }

    public function refreshCompanyDetails()
    {
        \Artisan::call('onair:refreshcompanydetails');
    }

    public function refreshCompanyFleet()
    {
        $companyId = env('ONAIR_COMPANYID');
        $url = $this->makeUrl('company/'.$companyId.'/fleet');
        // dd($url);
        $response = $this->makeRequest($url);
        $fleet = [];
        $company = Company::first();

        foreach($response as $key => $r) {
            $newAircraft = [
                'uuid' => $r['Id'],
                'nickname' => $r['Nickname'],
                'current_airport_uuid' => $r['CurrentAirportId'],
                'aircraft_status' => $r['AircraftStatus'],
                'last_status_change' => $r['LastStatusChange'],
                'current_status_duration_in_minutes' => $r['CurrentStatusDurationInMinutes'],
                'allow_sell' => $r['AllowSell'],
                'allow_rent' => $r['AllowRent'],
                'sell_price' => $r['SellPrice'],
                'rent_hour_price' => $r['RentHourPrice'],
                'rent_airport_uuid' => $r['RentAirportId'],
                'rent_fuel_total_gallons' => $r['RentFuelTotalGallons'],
                'rent_caution_amount' => $r['RentCautionAmount'],
                'rent_company_uuid' => $r['RentCompany']['Id'],
                'rent_start_date' => $r['RentStartDate'],
                'rent_last_daily_charge_date' => $r['RentLastDailyChargeDate'],
                'identifier' => $r['Identifier'],
                'heading' => $r['Heading'],
                'longitude' => $r['Longitude'],
                'latitude' => $r['Latitude'],
                'fuel_total_gallons' => $r['fuelTotalGallons'],
                'fuel_weight' => $r['fuelWeight'],
                'loaded_weight' => $r['loadedWeight'],
                'zero_fuel_weight' => $r['zeroFuelWeight'],
                'airframe_hours' => $r['airframeHours'],
                'airframe_condition' => $r['airframeCondition'],
                'last_annual_checkup' => $r['LastAnnualCheckup'],
                'last_100h_inspection' => $r['Last100hInspection'],
                'last_weekly_ownership_payment' => $r['LastWeeklyOwnershipPayment'],
                'last_parking_fee_payment' => $r['LastParkingFeePayment'],
                'is_controlled_by_ai' => $r['IsControlledByAI'],
                'hours_before_100h_inspection' => $r['HoursBefore100HInspection'],
                'extra_weight_capacity' => $r['ExtraWeightCapacity'],
                'total_weight_capacity' => $r['TotalWeightCapacity'],
                'must_do_maintenance' => $r['MustDoMaintenance'],
                'rental_company_uuid' => $companyId,
                'aircraft_type_uuid' => $r['AircraftType']['Id'],
                'current_seats' => $r['CurrentSeats']
            ];

            $newAircraftType = [
                'uuid' => $r['AircraftType']['Id'],
                'creation_date' => $r['AircraftType']['CreationDate'],
                'last_moderation_date' => $r['AircraftType']['LastModerationDate'],
                'display_name' => $r['AircraftType']['DisplayName'],
                'type_name' => $r['AircraftType']['TypeName'],
                'flights_count' => $r['AircraftType']['FlightsCount'],
                'time_between_overhaul' => $r['AircraftType']['TimeBetweenOverhaul'],
                'hightime_airframe' => $r['AircraftType']['HightimeAirframe'],
                'airport_min_size' => $r['AircraftType']['AirportMinSize'],
                'empty_weight' => $r['AircraftType']['emptyWeight'],
                'maximum_gross_weight' => $r['AircraftType']['maximumGrossWeight'],
                'estimated_cruise_ff' => $r['AircraftType']['estimatedCruiseFF'],
                'baseprice' => $r['AircraftType']['Baseprice'],
                'fuel_total_capacity_in_gallons' => $r['AircraftType']['FuelTotalCapacityInGallons'],
                'engine_type' => $r['AircraftType']['engineType'],
                'number_of_engines' => $r['AircraftType']['numberOfEngines'],
                'seats' => $r['AircraftType']['seats'],
                'needs_copilot' => $r['AircraftType']['needsCopilot'],
                'fuel_type' => $r['AircraftType']['fuelType'],
                'maximum_cargo_weight' => $r['AircraftType']['maximumCargoWeight'],
                'maximum_range_in_hour' => $r['AircraftType']['maximumRangeInHour'],
                'maximum_range_in_nm' => $r['AircraftType']['maximumRangeInNM'],
                'design_speed_vs0' => $r['AircraftType']['designSpeedVS0'],
                'design_speed_vs1' => $r['AircraftType']['designSpeedVS1'],
                'design_speed_vc' => $r['AircraftType']['designSpeedVC'],
                'is_disabled' => $r['AircraftType']['IsDisabled'],
                'luxef_actor' => $r['AircraftType']['LuxeFactor'],
                'standard_seat_weight' => $r['AircraftType']['StandardSeatWeight'],
                'is_fighter' => $r['AircraftType']['IsFighter'],
                'air_file_name' => $r['AircraftType']['AirFileName'],
                'simulator_version' => $r['AircraftType']['simulatorVersion'],
                'consolidated_design_speed_vc' => $r['AircraftType']['ConsolidatedDesignSpeedVC'],
                'consolidated_estimated_cruise_ff' => $r['AircraftType']['ConsolidatedEstimatedCruiseFF'],
                'addon_estimated_fuel_flow' => $r['AircraftType']['AddonEstimatedFuelFlow'],
                'addon_design_speed_vc' => $r['AircraftType']['AddonDesignSpeedVC'],
                'computed_max_payload' => $r['AircraftType']['ComputedMaxPayload'],
                'computed_seats' => $r['AircraftType']['ComputedSeats'],
                'aircraft_class_uuid' => $r['AircraftType']['AircraftClass']['Id']
            ];

            $newAircraftClass = [
                'uuid' => $r['AircraftType']['AircraftClass']['Id'],
                'short_name' => $r['AircraftType']['AircraftClass']['ShortName'],
                'name' => $r['AircraftType']['AircraftClass']['Name'],
                'order' => $r['AircraftType']['AircraftClass']['Order'],
            ];

            $newRentAirport = [
                'uuid' => $r['RentAirportId'],
                'icao' => $r['RentAirport']['ICAO'],
                'has_no_runways' => $r['RentAirport']['HasNoRunways'],
                'time_offset_in_sec' => $r['RentAirport']['TimeOffsetInSec'],
                'local_time_open_in_hours_since_midnight' => $r['RentAirport']['LocalTimeOpenInHoursSinceMidnight'],
                'local_time_close_in_hours_since_midnight' => $r['RentAirport']['LocalTimeCloseInHoursSinceMidnight'],
                'iata' => $r['RentAirport']['IATA'],
                'name' => $r['RentAirport']['Name'],
                'state' => $r['RentAirport']['State'],
                'country_code' => $r['RentAirport']['CountryCode'],
                'country_name' => $r['RentAirport']['CountryName'],
                'city' => $r['RentAirport']['City'],
                'latitude' => $r['RentAirport']['Latitude'],
                'longitude' => $r['RentAirport']['Longitude'],
                'elevation' => $r['RentAirport']['Elevation'],
                'has_land_runway' => $r['RentAirport']['HasLandRunway'],
                'has_water_runway' => $r['RentAirport']['HasWaterRunway'],
                'has_helipad' => $r['RentAirport']['HasHelipad'],
                'size' => $r['RentAirport']['Size'],
                'transition_altitude' => $r['RentAirport']['TransitionAltitude'],
                'is_not_in_vanilla_fsx' => $r['RentAirport']['IsNotInVanillaFSX'],
                'is_not_in_vanilla_p3d' => $r['RentAirport']['IsNotInVanillaP3D'],
                'is_not_in_vanilla_xplane' => $r['RentAirport']['IsNotInVanillaXPLANE'],
                'is_not_in_vanilla_fs2020' => $r['RentAirport']['IsNotInVanillaFS2020'],
                'is_closed' => $r['RentAirport']['IsClosed'],
                'is_valid' => $r['RentAirport']['IsValid'],
                'mag_var' => $r['RentAirport']['MagVar'],
                'is_addon' => $r['RentAirport']['IsAddon'],
                'random_seed' => $r['RentAirport']['RandomSeed'],
                'last_random_seed_generation' =>(array_key_exists('LastRandomSeedGeneration', $r['RentAirport'])) ? \Carbon\Carbon::parse($r['RentAirport']['LastRandomSeedGeneration']) : null,
                'is_military' => $r['RentAirport']['IsMilitary'],
                'has_lights' => $r['RentAirport']['HasLights'],
                'airport_source' => $r['RentAirport']['AirportSource'],
                'last_very_short_request_date' => (array_key_exists('LastVeryShortRequestDate', $r['RentAirport'])) ? \Carbon\Carbon::parse($r['RentAirport']['LastVeryShortRequestDate']) : null,
                'last_small_trip_request_date' => (array_key_exists('LastSmallTripRequestDate', $r['RentAirport'])) ? \Carbon\Carbon::parse($r['RentAirport']['LastSmallTripRequestDate']) : null,
                'last_medium_trip_request_date' => (array_key_exists('LastMediumTripRequestDate', $r['RentAirport']) )? \Carbon\Carbon::parse($r['RentAirport']['LastMediumTripRequestDate']) : null,
                'last_short_haul_request_date' => (array_key_exists('LastShortHaulRequestDate', $r['RentAirport'])) ? \Carbon\Carbon::parse($r['RentAirport']['LastShortHaulRequestDate']) : null,
                'last_medium_haul_request_date' => (array_key_exists('LastMediumHaulRequestDate', $r['RentAirport']) )? \Carbon\Carbon::parse($r['RentAirport']['LastMediumHaulRequestDate']) : null,
                'last_long_haul_request_date' => (array_key_exists('LastLongHaulRequestDate', $r['RentAirport'])) ? \Carbon\Carbon::parse($r['RentAirport']['LastLongHaulRequestDate']) : null,
                'display_name' => $r['RentAirport']['DisplayName'],
                'utc_time_open_in_hours_since_midnight' => $r['RentAirport']['UTCTimeOpenInHoursSinceMidnight'],
                'utc_time_close_in_hours_since_midnight' => $r['RentAirport']['UTCTimeCloseInHoursSinceMidnight'],
            ];

            $rentAirport = Airport::firstOrNew([
                'uuid' => $r['RentAirportId']
            ], $newRentAirport)->save();

            $newCurrentAirport = [
                'uuid' => $r['CurrentAirport']['Id'],
                'icao' => $r['CurrentAirport']['ICAO'],
                'has_no_runways' => $r['CurrentAirport']['HasNoRunways'],
                'time_offset_in_sec' => $r['CurrentAirport']['TimeOffsetInSec'],
                'local_time_open_in_hours_since_midnight' => $r['CurrentAirport']['LocalTimeOpenInHoursSinceMidnight'],
                'local_time_close_in_hours_since_midnight' => $r['CurrentAirport']['LocalTimeCloseInHoursSinceMidnight'],
                'iata' => $r['CurrentAirport']['IATA'],
                'name' => $r['CurrentAirport']['Name'],
                'state' => $r['CurrentAirport']['State'],
                'country_code' => $r['CurrentAirport']['CountryCode'],
                'country_name' => $r['CurrentAirport']['CountryName'],
                'city' => $r['CurrentAirport']['City'],
                'latitude' => $r['CurrentAirport']['Latitude'],
                'longitude' => $r['CurrentAirport']['Longitude'],
                'elevation' => $r['CurrentAirport']['Elevation'],
                'has_land_runway' => $r['CurrentAirport']['HasLandRunway'],
                'has_water_runway' => $r['CurrentAirport']['HasWaterRunway'],
                'has_helipad' => $r['CurrentAirport']['HasHelipad'],
                'size' => $r['CurrentAirport']['Size'],
                'transition_altitude' => $r['CurrentAirport']['TransitionAltitude'],
                'is_not_in_vanilla_fsx' => $r['CurrentAirport']['IsNotInVanillaFSX'],
                'is_not_in_vanilla_p3d' => $r['CurrentAirport']['IsNotInVanillaP3D'],
                'is_not_in_vanilla_xplane' => $r['CurrentAirport']['IsNotInVanillaXPLANE'],
                'is_not_in_vanilla_fs2020' => $r['CurrentAirport']['IsNotInVanillaFS2020'],
                'is_closed' => $r['CurrentAirport']['IsClosed'],
                'is_valid' => $r['CurrentAirport']['IsValid'],
                'mag_var' => $r['CurrentAirport']['MagVar'],
                'is_addon' => $r['CurrentAirport']['IsAddon'],
                'random_seed' => $r['CurrentAirport']['RandomSeed'],
                'last_random_seed_generation' => $r['CurrentAirport']['LastRandomSeedGeneration'],
                'is_military' => $r['CurrentAirport']['IsMilitary'],
                'has_lights' => $r['CurrentAirport']['HasLights'],
                'airport_source' => $r['CurrentAirport']['AirportSource'],
                'last_very_short_request_date' => $r['CurrentAirport']['LastVeryShortRequestDate'],
                'last_small_trip_request_date' => $r['CurrentAirport']['LastSmallTripRequestDate'],
                'last_medium_trip_request_date' => $r['CurrentAirport']['LastMediumTripRequestDate'],
                'last_short_haul_request_date' => $r['CurrentAirport']['LastShortHaulRequestDate'],
                'last_medium_haul_request_date' => $r['CurrentAirport']['LastMediumHaulRequestDate'],
                'last_long_haul_request_date' => $r['CurrentAirport']['LastLongHaulRequestDate'],
                'display_name' => $r['CurrentAirport']['DisplayName'],
                'utc_time_open_in_hours_since_midnight' => $r['CurrentAirport']['UTCTimeOpenInHoursSinceMidnight'],
                'utc_time_close_in_hours_since_midnight' => $r['CurrentAirport']['UTCTimeCloseInHoursSinceMidnight'],
            ];

            $currentAirport = Airport::firstOrNew([
                'uuid' => $newCurrentAirport['uuid']
            ], $newCurrentAirport)->save();

            // dd($newAircraft, $newAircraftType);
            $aircraftClass = AircraftClass::firstOrNew([
                'uuid' => $newAircraftClass['uuid']
            ], $newAircraftClass);
            $aircraftClass->save();

            $aircraftType = AircraftType::firstOrNew([
                'uuid' => $newAircraftType['uuid']
            ], $newAircraftType);
            $aircraftType->save();


            $aircraft = Aircraft::with(['aircraft_type', 'rented_by'])->firstOrCreate([
                'uuid' => $r['Id'],
                ], $newAircraft);

            $aircraft->aircraft_type()->associate($aircraftType);
            $aircraft->rented_by()->associate($company);
            $aircraft->save();

            array_push($fleet, $aircraft);
        }


        $fleet = Aircraft::with(['flights', 'aircraft_type', 'rent_airport', 'current_airport', 'aircraft_type.aircraft_class', 'aircraft_type.aircraft_class.required_certification', 'rented_by'])->get();
        // return response()->json($fleet);
    }

    public function refreshCompanyEmployees()
    {
        $companyId = env('ONAIR_COMPANYID');
        $url = $this->makeUrl('/company/'.$companyId.'/employees');

        $response = $this->makeRequest($url);
        $employees = [];

        foreach ($response as $key => $r) {
            $certifications = $r['ClassCertifications'] ;
            $newCertifications = [];
            foreach ($certifications as $key => $c) {
                $newCertification = [
                    'uuid' => $c['Id'],
                    'employee_uuid' => $c['PeopleId'],
                    'aircraft_class_uuid' => $c['AircraftClassId'],
                    'last_validation' => $c['LastValidation'],
                    'comments' => (array_key_exists('Comments', $c)) ? $c['Comments'] : null,
                ];

                $newCertification = ClassCertification::firstOrNew([
                    'uuid' => $newCertification['uuid']
                ], $newCertification)->save();

                array_push($newCertifications, $newCertification);
            }

            $newCurrentAirport = [
                'uuid' => $r['CurrentAirport']['Id'],
                'icao' => $r['CurrentAirport']['ICAO'],
                'has_no_runways' => $r['CurrentAirport']['HasNoRunways'],
                'time_offset_in_sec' => $r['CurrentAirport']['TimeOffsetInSec'],
                'local_time_open_in_hours_since_midnight' => $r['CurrentAirport']['LocalTimeOpenInHoursSinceMidnight'],
                'local_time_close_in_hours_since_midnight' => $r['CurrentAirport']['LocalTimeCloseInHoursSinceMidnight'],
                'iata' => $r['CurrentAirport']['IATA'],
                'name' => $r['CurrentAirport']['Name'],
                'state' => $r['CurrentAirport']['State'],
                'country_code' => $r['CurrentAirport']['CountryCode'],
                'country_name' => $r['CurrentAirport']['CountryName'],
                'city' => $r['CurrentAirport']['City'],
                'latitude' => $r['CurrentAirport']['Latitude'],
                'longitude' => $r['CurrentAirport']['Longitude'],
                'elevation' => $r['CurrentAirport']['Elevation'],
                'has_land_runway' => $r['CurrentAirport']['HasLandRunway'],
                'has_water_runway' => $r['CurrentAirport']['HasWaterRunway'],
                'has_helipad' => $r['CurrentAirport']['HasHelipad'],
                'size' => $r['CurrentAirport']['Size'],
                'transition_altitude' => $r['CurrentAirport']['TransitionAltitude'],
                'is_not_in_vanilla_fsx' => $r['CurrentAirport']['IsNotInVanillaFSX'],
                'is_not_in_vanilla_p3d' => $r['CurrentAirport']['IsNotInVanillaP3D'],
                'is_not_in_vanilla_xplane' => $r['CurrentAirport']['IsNotInVanillaXPLANE'],
                'is_not_in_vanilla_fs2020' => $r['CurrentAirport']['IsNotInVanillaFS2020'],
                'is_closed' => $r['CurrentAirport']['IsClosed'],
                'is_valid' => $r['CurrentAirport']['IsValid'],
                'mag_var' => $r['CurrentAirport']['MagVar'],
                'is_addon' => $r['CurrentAirport']['IsAddon'],
                'random_seed' => $r['CurrentAirport']['RandomSeed'],
                'last_random_seed_generation' => $r['CurrentAirport']['LastRandomSeedGeneration'],
                'is_military' => $r['CurrentAirport']['IsMilitary'],
                'has_lights' => $r['CurrentAirport']['HasLights'],
                'airport_source' => $r['CurrentAirport']['AirportSource'],
                'last_very_short_request_date' => $r['CurrentAirport']['LastVeryShortRequestDate'],
                'last_small_trip_request_date' => $r['CurrentAirport']['LastSmallTripRequestDate'],
                'last_medium_trip_request_date' => $r['CurrentAirport']['LastMediumTripRequestDate'],
                'last_short_haul_request_date' => $r['CurrentAirport']['LastShortHaulRequestDate'],
                'last_medium_haul_request_date' => $r['CurrentAirport']['LastMediumHaulRequestDate'],
                'last_long_haul_request_date' => $r['CurrentAirport']['LastLongHaulRequestDate'],
                'display_name' => $r['CurrentAirport']['DisplayName'],
                'utc_time_open_in_hours_since_midnight' => $r['CurrentAirport']['UTCTimeOpenInHoursSinceMidnight'],
                'utc_time_close_in_hours_since_midnight' => $r['CurrentAirport']['UTCTimeCloseInHoursSinceMidnight'],
            ];

            $currentAirport = Airport::firstOrNew([
                'uuid' => $newCurrentAirport['uuid']
            ], $newCurrentAirport)->save();

            $newHomeAirport = [
                'uuid' => $r['HomeAirport']['Id'],
                'icao' => $r['HomeAirport']['ICAO'],
                'has_no_runways' => $r['HomeAirport']['HasNoRunways'],
                'time_offset_in_sec' => $r['HomeAirport']['TimeOffsetInSec'],
                'local_time_open_in_hours_since_midnight' => $r['HomeAirport']['LocalTimeOpenInHoursSinceMidnight'],
                'local_time_close_in_hours_since_midnight' => $r['HomeAirport']['LocalTimeCloseInHoursSinceMidnight'],
                'iata' => $r['HomeAirport']['IATA'],
                'name' => $r['HomeAirport']['Name'],
                'state' => $r['HomeAirport']['State'],
                'country_code' => $r['HomeAirport']['CountryCode'],
                'country_name' => $r['HomeAirport']['CountryName'],
                'city' => $r['HomeAirport']['City'],
                'latitude' => $r['HomeAirport']['Latitude'],
                'longitude' => $r['HomeAirport']['Longitude'],
                'elevation' => $r['HomeAirport']['Elevation'],
                'has_land_runway' => $r['HomeAirport']['HasLandRunway'],
                'has_water_runway' => $r['HomeAirport']['HasWaterRunway'],
                'has_helipad' => $r['HomeAirport']['HasHelipad'],
                'size' => $r['HomeAirport']['Size'],
                'transition_altitude' => $r['HomeAirport']['TransitionAltitude'],
                'is_not_in_vanilla_fsx' => $r['HomeAirport']['IsNotInVanillaFSX'],
                'is_not_in_vanilla_p3d' => $r['HomeAirport']['IsNotInVanillaP3D'],
                'is_not_in_vanilla_xplane' => $r['HomeAirport']['IsNotInVanillaXPLANE'],
                'is_not_in_vanilla_fs2020' => $r['HomeAirport']['IsNotInVanillaFS2020'],
                'is_closed' => $r['HomeAirport']['IsClosed'],
                'is_valid' => $r['HomeAirport']['IsValid'],
                'mag_var' => $r['HomeAirport']['MagVar'],
                'is_addon' => $r['HomeAirport']['IsAddon'],
                'random_seed' => $r['HomeAirport']['RandomSeed'],
                'last_random_seed_generation' => $r['HomeAirport']['LastRandomSeedGeneration'],
                'is_military' => $r['HomeAirport']['IsMilitary'],
                'has_lights' => $r['HomeAirport']['HasLights'],
                'airport_source' => $r['HomeAirport']['AirportSource'],
                'last_very_short_request_date' => $r['HomeAirport']['LastVeryShortRequestDate'],
                'last_small_trip_request_date' => $r['HomeAirport']['LastSmallTripRequestDate'],
                'last_medium_trip_request_date' => $r['HomeAirport']['LastMediumTripRequestDate'],
                'last_short_haul_request_date' => $r['HomeAirport']['LastShortHaulRequestDate'],
                'last_medium_haul_request_date' => $r['HomeAirport']['LastMediumHaulRequestDate'],
                'last_long_haul_request_date' => $r['HomeAirport']['LastLongHaulRequestDate'],
                'display_name' => $r['HomeAirport']['DisplayName'],
                'utc_time_open_in_hours_since_midnight' => $r['HomeAirport']['UTCTimeOpenInHoursSinceMidnight'],
                'utc_time_close_in_hours_since_midnight' => $r['HomeAirport']['UTCTimeCloseInHoursSinceMidnight'],
            ];

            $homeAirport = Airport::firstOrNew([
                'uuid' => $newHomeAirport['uuid']
            ], $newHomeAirport)->save();

            $newEmployee = [
                'uuid' => $r['Id'],
                'psuedo_name' => $r['Pseudo'],
                'company_uuid' => $r['CompanyId'],
                'flight_hours_total_before_hiring' => $r['FlightHoursTotalBeforeHiring'],
                'flight_hours_in_company' => $r['FlightHoursInCompany'],
                'weight' => $r['Weight'],
                'birth_date' => $r['BirthDate'],
                'fatigue' => $r['Fatigue'],
                'punctuality' => $r['Punctuality'],
                'comfort' => $r['Comfort'],
                'happiness' => $r['Happiness'],
                'home_airport_uuid' => $r['HomeAirportId'],
                'current_airport_uuid' => $r['CurrentAirportId'],
                'per_flight_hour_wages' => $r['PerFlightHourWages'],
                'weekly_garanted_salary' => $r['WeeklyGarantedSalary'],
                'per_fligh_thour_salary' => $r['PerFlightHourSalary'],
                'category' => $r['Category'],
                'status' => $r['Status'],
                'last_status_change' => $r['LastStatusChange'],
                'current_total_flight_hours_in_duty' => $r['CurrentTotalFlightHoursInDuty'],
                'freelance_since' => (array_key_exists('FreelanceSince', $r)) ? $r['FreelanceSince'] : null,
                'freelance_until' => (array_key_exists('FreelanceUntil', $r)) ? $r['FreelanceUntil'] : null,
                'last_payment_date' => (array_key_exists('LastPaymentDate', $r)) ? $r['LastPaymentDate'] : null,
                'is_online' => $r['IsOnline'],
                'flight_hours_grand_total' => $r['FlightHoursGrandTotal']
            ];

            $employee = Employee::firstOrNew([
                'uuid' => $newEmployee['uuid']
            ], $newEmployee)->save();

            array_push($employees, $employee);
        }

        $employees = Employee::with(['company', 'certifications', 'current_airport', 'home_airport'])->get();
        // return response()->json($employees);
    }

    public function refreshCompanyFlights()
    {
        $companyId = env('ONAIR_COMPANYID');
        $url = $this->makeUrl('/company/'.$companyId.'/flights');

        $response = $this->makeRequest($url);
        $flights = [];

        foreach ($response as $key => $r) {
            // ensure DepartureAirport exists
            if (array_key_exists('DepartureAirport', $r)) {
                $newDepartureAirport = [
                    'uuid' => $r['DepartureAirport']['Id'],
                    'icao' => $r['DepartureAirport']['ICAO'],
                    'has_no_runways' => $r['DepartureAirport']['HasNoRunways'],
                    'time_offset_in_sec' => $r['DepartureAirport']['TimeOffsetInSec'],
                    'local_time_open_in_hours_since_midnight' => $r['DepartureAirport']['LocalTimeOpenInHoursSinceMidnight'],
                    'local_time_close_in_hours_since_midnight' => $r['DepartureAirport']['LocalTimeCloseInHoursSinceMidnight'],
                    'iata' => $r['DepartureAirport']['IATA'],
                    'name' => $r['DepartureAirport']['Name'],
                    'state' => $r['DepartureAirport']['State'],
                    'country_code' => $r['DepartureAirport']['CountryCode'],
                    'country_name' => $r['DepartureAirport']['CountryName'],
                    'city' => $r['DepartureAirport']['City'],
                    'latitude' => $r['DepartureAirport']['Latitude'],
                    'longitude' => $r['DepartureAirport']['Longitude'],
                    'elevation' => $r['DepartureAirport']['Elevation'],
                    'has_land_runway' => $r['DepartureAirport']['HasLandRunway'],
                    'has_water_runway' => $r['DepartureAirport']['HasWaterRunway'],
                    'has_helipad' => $r['DepartureAirport']['HasHelipad'],
                    'size' => $r['DepartureAirport']['Size'],
                    'transition_altitude' => $r['DepartureAirport']['TransitionAltitude'],
                    'is_not_in_vanilla_fsx' => $r['DepartureAirport']['IsNotInVanillaFSX'],
                    'is_not_in_vanilla_p3d' => $r['DepartureAirport']['IsNotInVanillaP3D'],
                    'is_not_in_vanilla_xplane' => $r['DepartureAirport']['IsNotInVanillaXPLANE'],
                    'is_not_in_vanilla_fs2020' => $r['DepartureAirport']['IsNotInVanillaFS2020'],
                    'is_closed' => $r['DepartureAirport']['IsClosed'],
                    'is_valid' => $r['DepartureAirport']['IsValid'],
                    'mag_var' => $r['DepartureAirport']['MagVar'],
                    'is_addon' => $r['DepartureAirport']['IsAddon'],
                    'random_seed' => $r['DepartureAirport']['RandomSeed'],
                    'last_random_seed_generation' => (array_key_exists('LastRandomSeedGeneration',  $r['DepartureAirport'])) ? \Carbon\Carbon::parse($r['DepartureAirport']['LastRandomSeedGeneration']) : null,
                    'is_military' => $r['DepartureAirport']['IsMilitary'],
                    'has_lights' => $r['DepartureAirport']['HasLights'],
                    'airport_source' => $r['DepartureAirport']['AirportSource'],
                    'last_very_short_request_date' => (array_key_exists('LastVeryShortRequestDate', $r['DepartureAirport'])) ? \Carbon\Carbon::parse($r['DepartureAirport']['LastVeryShortRequestDate']) : null,
                    'last_small_trip_request_date' => (array_key_exists('LastSmallTripRequestDate', $r['DepartureAirport'])) ? \Carbon\Carbon::parse($r['DepartureAirport']['LastSmallTripRequestDate']) : null,
                    'last_medium_trip_request_date' => (array_key_exists('LastMediumTripRequestDate', $r['DepartureAirport']) )? \Carbon\Carbon::parse($r['DepartureAirport']['LastMediumTripRequestDate']) : null,
                    'last_short_haul_request_date' => (array_key_exists('LastShortHaulRequestDate', $r['DepartureAirport'])) ? \Carbon\Carbon::parse($r['DepartureAirport']['LastShortHaulRequestDate']) : null,
                    'last_medium_haul_request_date' => (array_key_exists('LastMediumHaulRequestDate', $r['DepartureAirport']) )? \Carbon\Carbon::parse($r['DepartureAirport']['LastMediumHaulRequestDate']) : null,
                    'last_long_haul_request_date' => (array_key_exists('LastLongHaulRequestDate', $r['DepartureAirport'])) ? \Carbon\Carbon::parse($r['DepartureAirport']['LastLongHaulRequestDate']) : null,
                    'display_name' => $r['DepartureAirport']['DisplayName'],
                    'utc_time_open_in_hours_since_midnight' => $r['DepartureAirport']['UTCTimeOpenInHoursSinceMidnight'],
                    'utc_time_close_in_hours_since_midnight' => $r['DepartureAirport']['UTCTimeCloseInHoursSinceMidnight'],
                ];
                $departureAirport = Airport::firstOrNew([ 'uuid' => $r['DepartureAirport']['Id'] ], $newDepartureAirport)->save();
                // $flight->departure_airport()->associate($newDepartureAirport);
            }

            // ensure ArrivalIntendedAirport exists
            if (array_key_exists('ArrivalIntendedAirport', $r)) {
                $newArrivalIntendedAirport = [
                    'uuid' => $r['ArrivalIntendedAirport']['Id'],
                    'icao' => $r['ArrivalIntendedAirport']['ICAO'],
                    'has_no_runways' => $r['ArrivalIntendedAirport']['HasNoRunways'],
                    'time_offset_in_sec' => $r['ArrivalIntendedAirport']['TimeOffsetInSec'],
                    'local_time_open_in_hours_since_midnight' => $r['ArrivalIntendedAirport']['LocalTimeOpenInHoursSinceMidnight'],
                    'local_time_close_in_hours_since_midnight' => $r['ArrivalIntendedAirport']['LocalTimeCloseInHoursSinceMidnight'],
                    'iata' => $r['ArrivalIntendedAirport']['IATA'],
                    'name' => $r['ArrivalIntendedAirport']['Name'],
                    'state' => $r['ArrivalIntendedAirport']['State'],
                    'country_code' => $r['ArrivalIntendedAirport']['CountryCode'],
                    'country_name' => $r['ArrivalIntendedAirport']['CountryName'],
                    'city' => $r['ArrivalIntendedAirport']['City'],
                    'latitude' => $r['ArrivalIntendedAirport']['Latitude'],
                    'longitude' => $r['ArrivalIntendedAirport']['Longitude'],
                    'elevation' => $r['ArrivalIntendedAirport']['Elevation'],
                    'has_land_runway' => $r['ArrivalIntendedAirport']['HasLandRunway'],
                    'has_water_runway' => $r['ArrivalIntendedAirport']['HasWaterRunway'],
                    'has_helipad' => $r['ArrivalIntendedAirport']['HasHelipad'],
                    'size' => $r['ArrivalIntendedAirport']['Size'],
                    'transition_altitude' => $r['ArrivalIntendedAirport']['TransitionAltitude'],
                    'is_not_in_vanilla_fsx' => $r['ArrivalIntendedAirport']['IsNotInVanillaFSX'],
                    'is_not_in_vanilla_p3d' => $r['ArrivalIntendedAirport']['IsNotInVanillaP3D'],
                    'is_not_in_vanilla_xplane' => $r['ArrivalIntendedAirport']['IsNotInVanillaXPLANE'],
                    'is_not_in_vanilla_fs2020' => $r['ArrivalIntendedAirport']['IsNotInVanillaFS2020'],
                    'is_closed' => $r['ArrivalIntendedAirport']['IsClosed'],
                    'is_valid' => $r['ArrivalIntendedAirport']['IsValid'],
                    'mag_var' => $r['ArrivalIntendedAirport']['MagVar'],
                    'is_addon' => $r['ArrivalIntendedAirport']['IsAddon'],
                    'random_seed' => $r['ArrivalIntendedAirport']['RandomSeed'],
                    'last_random_seed_generation' => (array_key_exists('ArrivalIntendedAirport', $r['ArrivalIntendedAirport'])) ? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastRandomSeedGeneration']) : null,
                    'is_military' => $r['ArrivalIntendedAirport']['IsMilitary'],
                    'has_lights' => $r['ArrivalIntendedAirport']['HasLights'],
                    'airport_source' => $r['ArrivalIntendedAirport']['AirportSource'],
                    'last_very_short_request_date' => (array_key_exists('LastVeryShortRequestDate', $r['ArrivalIntendedAirport'])) ? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastVeryShortRequestDate']) : null,
                    'last_small_trip_request_date' => (array_key_exists('LastSmallTripRequestDate', $r['ArrivalIntendedAirport'])) ? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastSmallTripRequestDate']) : null,
                    'last_medium_trip_request_date' => (array_key_exists('LastMediumTripRequestDate', $r['ArrivalIntendedAirport']) )? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastMediumTripRequestDate']) : null,
                    'last_short_haul_request_date' => (array_key_exists('LastShortHaulRequestDate', $r['ArrivalIntendedAirport'])) ? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastShortHaulRequestDate']) : null,
                    'last_medium_haul_request_date' => (array_key_exists('LastMediumHaulRequestDate', $r['ArrivalIntendedAirport']) )? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastMediumHaulRequestDate']) : null,
                    'last_long_haul_request_date' => (array_key_exists('LastLongHaulRequestDate', $r['ArrivalIntendedAirport'])) ? \Carbon\Carbon::parse($r['ArrivalIntendedAirport']['LastLongHaulRequestDate']) : null,
                    'display_name' => $r['ArrivalIntendedAirport']['DisplayName'],
                    'utc_time_open_in_hours_since_midnight' => $r['ArrivalIntendedAirport']['UTCTimeOpenInHoursSinceMidnight'],
                    'utc_time_close_in_hours_since_midnight' => $r['ArrivalIntendedAirport']['UTCTimeCloseInHoursSinceMidnight'],
                ];
                $arrivalIntendedAirport = Airport::firstOrNew([ 'uuid' => $r['ArrivalIntendedAirportId'] ], $newArrivalIntendedAirport)->save();
                // $flight->arrival_intended_airport()->associate($newArrivalIntendedAirport);
            }

            // ensure ArrivalActualAirport exists
            if (array_key_exists('ArrivalActualAirport', $r)) {
                $newArrivalActualAirport = [
                    'uuid' => $r['ArrivalActualAirport']['Id'],
                    'icao' => $r['ArrivalActualAirport']['ICAO'],
                    'has_no_runways' => $r['ArrivalActualAirport']['HasNoRunways'],
                    'time_offset_in_sec' => $r['ArrivalActualAirport']['TimeOffsetInSec'],
                    'local_time_open_in_hours_since_midnight' => $r['ArrivalActualAirport']['LocalTimeOpenInHoursSinceMidnight'],
                    'local_time_close_in_hours_since_midnight' => $r['ArrivalActualAirport']['LocalTimeCloseInHoursSinceMidnight'],
                    'iata' => $r['ArrivalActualAirport']['IATA'],
                    'name' => $r['ArrivalActualAirport']['Name'],
                    'state' => $r['ArrivalActualAirport']['State'],
                    'country_code' => $r['ArrivalActualAirport']['CountryCode'],
                    'country_name' => $r['ArrivalActualAirport']['CountryName'],
                    'city' => $r['ArrivalActualAirport']['City'],
                    'latitude' => $r['ArrivalActualAirport']['Latitude'],
                    'longitude' => $r['ArrivalActualAirport']['Longitude'],
                    'elevation' => $r['ArrivalActualAirport']['Elevation'],
                    'has_land_runway' => $r['ArrivalActualAirport']['HasLandRunway'],
                    'has_water_runway' => $r['ArrivalActualAirport']['HasWaterRunway'],
                    'has_helipad' => $r['ArrivalActualAirport']['HasHelipad'],
                    'size' => $r['ArrivalActualAirport']['Size'],
                    'transition_altitude' => $r['ArrivalActualAirport']['TransitionAltitude'],
                    'is_not_in_vanilla_fsx' => $r['ArrivalActualAirport']['IsNotInVanillaFSX'],
                    'is_not_in_vanilla_p3d' => $r['ArrivalActualAirport']['IsNotInVanillaP3D'],
                    'is_not_in_vanilla_xplane' => $r['ArrivalActualAirport']['IsNotInVanillaXPLANE'],
                    'is_not_in_vanilla_fs2020' => $r['ArrivalActualAirport']['IsNotInVanillaFS2020'],
                    'is_closed' => $r['ArrivalActualAirport']['IsClosed'],
                    'is_valid' => $r['ArrivalActualAirport']['IsValid'],
                    'mag_var' => $r['ArrivalActualAirport']['MagVar'],
                    'is_addon' => $r['ArrivalActualAirport']['IsAddon'],
                    'random_seed' => $r['ArrivalActualAirport']['RandomSeed'],
                    'last_random_seed_generation' => (array_key_exists('ArrivalActualAirport', $r['ArrivalActualAirport'])) ? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastRandomSeedGeneration']) : null,
                    'is_military' => $r['ArrivalActualAirport']['IsMilitary'],
                    'has_lights' => $r['ArrivalActualAirport']['HasLights'],
                    'airport_source' => $r['ArrivalActualAirport']['AirportSource'],
                    'last_very_short_request_date' => (array_key_exists('LastVeryShortRequestDate', $r['ArrivalActualAirport'])) ? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastVeryShortRequestDate']) : null,
                    'last_small_trip_request_date' => (array_key_exists('LastSmallTripRequestDate', $r['ArrivalActualAirport'])) ? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastSmallTripRequestDate']) : null,
                    'last_medium_trip_request_date' => (array_key_exists('LastMediumTripRequestDate', $r['ArrivalActualAirport']) )? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastMediumTripRequestDate']) : null,
                    'last_short_haul_request_date' => (array_key_exists('LastShortHaulRequestDate', $r['ArrivalActualAirport'])) ? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastShortHaulRequestDate']) : null,
                    'last_medium_haul_request_date' => (array_key_exists('LastMediumHaulRequestDate', $r['ArrivalActualAirport']) )? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastMediumHaulRequestDate']) : null,
                    'last_long_haul_request_date' => (array_key_exists('LastLongHaulRequestDate', $r['ArrivalActualAirport'])) ? \Carbon\Carbon::parse($r['ArrivalActualAirport']['LastLongHaulRequestDate']) : null,
                    'display_name' => $r['ArrivalActualAirport']['DisplayName'],
                    'utc_time_open_in_hours_since_midnight' => $r['ArrivalActualAirport']['UTCTimeOpenInHoursSinceMidnight'],
                    'utc_time_close_in_hours_since_midnight' => $r['ArrivalActualAirport']['UTCTimeCloseInHoursSinceMidnight'],
                ];
                $arrivalActualAirport = Airport::firstOrNew([ 'uuid' => $r['ArrivalActualAirportId'] ], $newArrivalActualAirport)->save();
                // $flight->arrival_actual_airport()->associate($newArrivalActualAirport);
            }

            /// reformat newFlight into compatible syntax
            $newFlight = [
                'uuid' => $r['Id'],
                'aircraft_uuid' => $r['AircraftId'],
                'company_uuid' => $r['CompanyId'],
                'departure_airport_uuid' => (array_key_exists('DepartureAirportId', $r)) ? $r['DepartureAirportId'] : null,
                'arrival_intended_airport_uuid' => (array_key_exists('ArrivalIntendedAirportId', $r)) ? $r['ArrivalIntendedAirportId'] : null,
                'arrival_actual_airport_uuid' => (array_key_exists('ArrivalActualAirportId', $r)) ? $r['ArrivalActualAirportId'] : null,
                'registered' => $r['Registered'],
                'category' => $r['Category'],
                'result_comments' => (array_key_exists('ResultComments', $r)) ? $r['ResultComments'] : null,
                'start_time' => $r['StartTime'],
                'engine_on_time' => (array_key_exists('EngineOnTime', $r)) ? \Carbon\Carbon::parse($r['EngineOnTime']) : null,
                'engine_off_time' => (array_key_exists('EngineOffTime', $r)) ? \Carbon\Carbon::parse($r['EngineOffTime']) : null,
                'airborne_time' => (array_key_exists('AirborneTime', $r)) ? \Carbon\Carbon::parse($r['AirborneTime']) : null,
                'landed_time' => (array_key_exists('LandedTime', $r)) ? \Carbon\Carbon::parse($r['LandedTime']) : null,
                'intended_flight_level' => $r['IntendedFlightLevel'],
                'passengers' => $r['Passengers'],
                'cargo' => $r['Cargo'],
                'added_fuel_qty' => $r['AddedFuelQty'],
                'is_ai' => $r['IsAI'],
                'vertical_speed_at_touchdown_mps' => $r['VerticalSpeedAtTouchdownMpS'],
                'maxg_force' => $r['MaxGForce'],
                'ming_force' => $r['MinGForce'],
                'max_bank' => $r['MaxBank'],
                'max_pitch' => $r['MaxPitch'],
                'has_stalled' => $r['HasStalled'],
                'has_overspeeded' => $r['HasOverspeeded'],
                'engine1_status' => $r['Engine1Status'],
                'engine2_status' => $r['Engine2Status'],
                'engine3_status' => $r['Engine3Status'],
                'engine4_status' => $r['Engine4Status'],
                'engine5_status' => $r['Engine5Status'],
                'engine6_status' => $r['Engine6Status'],
                'xp_flight' => $r['XPFlight'],
                'xp_flight_bonus' => $r['XPFlightBonus'],
                'xp_missions' => $r['XPMissions'],
                'cargos_total_weight' => $r['CargosTotalWeight'],
                'pax_count' => $r['PAXCount'],
                'aircraft_current_fob' => $r['AircraftCurrentFOB'],
                'aircraft_current_altitude' => $r['AircraftCurrentAltitude'],
                'actual_cruise_altitude' => $r['ActualCruiseAltitude'],
                'actual_consumption_at_cruise_level_in_lbsperhour' => $r['ActualConsumptionAtCruiseLevelInLbsPerHour'],
                'actual_total_fuel_consumption_inlbs' => $r['ActualTotalFuelConsumptionInLbs'],
                'actual_consumption_at_cruise_level_in_galperhour' => $r['ActualConsumptionAtCruiseLevelInGalPerHour'],
                'actual_total_fuel_consumptioningal' => $r['ActualTotalFuelConsumptionInGal'],
                'actual_tas_at_cruise_level' => $r['ActualTASAtCruiseLevel'],
                'actual__cruise_time_in_minutes' => $r['ActualCruiseTimeInMinutes'],
                'actual_pressure_altitude' => $r['ActualPressureAltitude'],
                'register_state' => $r['RegisterState'],
                'wrong_fuel_detected' => $r['WrongFuelDetected'],
                'wrong_weight_detected' => $r['WrongWeightDetected'],
                'time_offset' => $r['TimeOffset'],
                'can_resume_or_abort' => $r['CanResumeOrAbort'],
                'engine_on_real_time' => (array_key_exists('EngineOnRealTime', $r)) ? \Carbon\Carbon::parse($r['EngineOnRealTime']) : null,
                'engine_off_real_time' => (array_key_exists('EngineOffRealTime', $r)) ? \Carbon\Carbon::parse($r['EngineOffRealTime']) : null,
            ];

            // find the flight if it exists, or create if not
            $flight = Flight::firstOrNew([
                'uuid' => $newFlight['uuid']
            ], $newFlight);

            $flight->save();

            array_push($flights, $flight);
        }

        // grab all flights, pull in relationShips
        $flights = Flight::with([
            'aircraft',
            'company',
            'departure_airport',
            'arrival_intended_airport',
            'arrival_actual_airport',
        ])->get();

        // return response()->json($flights);
    }

    public function refreshCompanyFBOs()
    {
        $companyId = env('ONAIR_COMPANYID');
        $url = $this->makeUrl('/company/'.$companyId.'/fobs');

        $response = $this->makeRequest($url);

        return response()->json($response);
    }

    public function refreshCompanyCashFlow()
    {
        $companyId = env('ONAIR_COMPANYID');
        $url = $this->makeUrl('/company/'.$companyId.'/cashflow');

        $response = $this->makeRequest($url);

        return response()->json($response);
    }
}
