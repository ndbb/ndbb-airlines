<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\Company;
use App\Models\Config;
use App\Models\Flight;
use App\Models\Aircraft;
use App\Models\AircraftType;
use App\Models\AircraftClass;

class AircraftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = Config::first();
        $company = Company::first();
        $aircraft = Aircraft::with([
            'status',
            'aircraft_type',
            'aircraft_type.fuel_type',
            'aircraft_type.aircraft_class',
            'current_airport',
            'rent_airport',
            'flights'
        ])->get();

        return Inertia::render('Aircraft/Show', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => 'Fleet',
            'appTitle' => $config->companyName,
            'company' => $company,
            'aircraft' => $aircraft
        ]);
    }

    public function indexBy_company(Request $request, $companyId)
    {
        $config = Config::first();

        $company = Company::with([
            'world',
            'aircraft',
            'aircraft.status',
            'aircraft.aircraft_type',
            'aircraft.aircraft_type.fuel_type',
            'aircraft.aircraft_type.aircraft_class',
            'aircraft.current_airport',
            'aircraft.rent_airport',
            'aircraft.flights',
            'aircraft.company',
            'aircraft.company.world',
        ])
        ->where('uuid', $companyId)
        ->first();

        $companyIdentifier = $company->airline;

        return Inertia::render('Aircraft/Show', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => $company->airline.' Fleet',
            'appTitle' => $config->companyName,
            'company' => $company,
            'aircraft' => $company->aircraft,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $config = Config::first();
        $company = Company::first();

        $aircraft = Aircraft::with([
            'status',
            'aircraft_type',
            'aircraft_type.fuel_type',
            'aircraft_type.aircraft_class',
            'rented_by',
            'current_airport',
            'rent_airport',
            'flights'
        ])->where('id', $id)->first();

        return Inertia::render('Aircraft/Detail', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => $aircraft['identifier'].' Details',
            'appTitle' => $config->companyName,
            'company' => $company,
            'aircraft' => $aircraft
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function edit(Flight $flight)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Flight $flight)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function destroy(Flight $flight)
    {
        //
    }
}
