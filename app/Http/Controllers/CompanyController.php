<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\Company;
use App\Models\Config;
use App\Models\World;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = Config::first();
        $companies = Company::with(['world'])->get();
        $worlds = World::where('is_enabled', true)->get();

        return Inertia::render('Company/List', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => 'OnAir Companies',
            'appTitle' => $config->companyName,
            'companies' => $companies,
            'worlds' => $worlds,
        ]);
    }

    public function show(Request $request, $id)
    {
        $config = Config::first();
        $company = Company::where('id', $id)->first();

        return Inertia::render('Company/Show', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => 'OnAir Company',
            'appTitle' => $config->companyName,
            'company' => $company,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $body = $request->all();

        $worldSlug = $body['world'];
        $uuid = $body['uuid'];
        $api_key = $body['api_key'];

        $sync_company = $body['sync_company'];
        $sync_employees = $body['sync_employees'];
        $sync_fbos = $body['sync_fbos'];
        $sync_fleet = $body['sync_fleet'];
        $sync_flights = $body['sync_flights'];

        $world = World::where('slug', $worldSlug)->first();
        $newCompany = $this->onAirLookup($body);

        $newCompany['api_key'] = $api_key;
        $newCompany['sync_company'] = $sync_company;
        $newCompany['sync_employees'] = $sync_employees;
        $newCompany['sync_fbos'] = $sync_fbos;
        $newCompany['sync_fleet'] = $sync_fleet;
        $newCompany['sync_flights'] = $sync_flights;

        $newC = Company::create($newCompany);
        $newC->world()->associate($world);
        $newC->save();

        return redirect('/company');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function edit(Flight $flight)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Flight $flight)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function destroy(Flight $flight)
    {
        //
    }

    public function lookup(Request $request)
    {
        $body = $request->all();
        $response = $this->onAirLookup($body);
        return response()->json($response);
    }

    protected function onAirLookup($body)
    {
        $world = $body['world'];
        $uuid = $body['uuid'];
        $api_key = $body['api_key'];

        if (isset($world) && isset($uuid) && isset($api_key)) {
            $url = '/company/'.$uuid;
            $data = $this->makeOnAirRequest($world, $api_key, $url);

            $newCompany = [
                'uuid' => $data['Id'],
                'world_id' => $data['WorldId'],
                'name' => $data['Name'],
                'airline' => $data['AirlineCode'],
                'last_connected' => $data['LastConnection'],
                'last_report_date' => $data['LastReportDate'],
                'reputation' => $data['Reputation'],
                'creation_date' => $data['CreationDate'],
                'difficulty_level' => $data['DifficultyLevel'],
                'level' => $data['Level'],
                'xp' => $data['LevelXP'],
                'transport_employee_instant' => $data['TransportEmployeeInstant'],
                'transport_player_instant' => $data['TransportPlayerInstant'],
                'force_time_in_simulator' => $data['ForceTimeInSimulator'],
                'use_small_airports' => $data['UseSmallAirports'],
                'use_only_vanilla_airports' => $data['UseOnlyVanillaAirports'],
                'enable_skill_tree' => $data['EnableSkillTree'],
                'checkride_level' => $data['CheckrideLevel'],
                'enable_landing_penalities' => $data['EnableLandingPenalities'],
                'enable_employees_flight_duty_and_sleep' => $data['EnableEmployeesFlightDutyAndSleep'],
                'aircraft_rent_level' => $data['AircraftRentLevel'],
                'enable_cargos_and_charters_loading_time' => $data['EnableCargosAndChartersLoadingTime'],
                'in_survival' => $data['InSurvival'],
                'pay_bonus_factor' => $data['PayBonusFactor'],
                'enable_sim_failures' => $data['EnableSimFailures'],
                'disable_seats_config_check' => $data['DisableSeatsConfigCheck'],
                'realistic_sim_procedures' => $data['RealisticSimProcedures'],
            ];

            return $newCompany;
        }
    }

    protected function makeOnAirRequest($world, $api_key, $endPoint)
    {

        $url = $this->makeOnAirUrl($world, $endPoint);

        $response = Http::withHeaders([
            'oa-apikey' => $api_key
        ])->get($url)->json()['Content'];

        return $response;
    }

    protected function makeOnAirUrl($world, $endPoint, $baseURL = 'onair.company/api/v1')
    {
        $endPoint = (strpos($endPoint, '/') === 0) ? $endPoint : '/'.$endPoint;

        $url = 'https://'.$world.'.'.$baseURL.$endPoint;

        return $url;
    }
}
