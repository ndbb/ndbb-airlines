<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Models\Config;

class HomeController extends Controller
{
    public function index()
    {
        $config = Config::first();
        return Inertia::render('Welcome', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => '',
            'heading' => '',
            'appTitle' => $config->companyName,
            'menu' => [

            ]
        ]);
    }
}
