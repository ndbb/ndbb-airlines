<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\Config;
use App\Models\World;

class ConfigController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Config/Create', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => '',
            'heading' => 'Create Config',
            'appTitle' => $config->companyName,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $config = Config::create([
            'apiKey' => $input->apiKey,
            'companyId' => $input->companyId,
            'world' => $input->world,
            'baseURL' => $input->baseURL,
            'logSchedules' => $input->logSchedules,
            'companyName' => $input->companyName,
        ]);

        return redirect('config.show');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $config = Config::first();
        $worlds = World::all();
        return Inertia::render('Config/Show', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => 'Config',
            'heading' => 'Config',
            'appTitle' => $config->companyName,
            'config' => $config,
            'worlds' => $worlds,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $config = Config::first();
        $worlds = World::all();
        return Inertia::render('Config/Edit', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => 'Edit Config',
            'heading' => 'Edit Config',
            'appTitle' => $config->companyName,
            'config' => $config,
            'worlds' => $worlds,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $inputs = $request->all();
        $config = Config::first();
        $config->update($inputs);
        $config->save();

        return redirect()->route('config.show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function destroy(Config $config)
    {
        //
    }
}
