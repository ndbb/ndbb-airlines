<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\Company;
use App\Models\Config;
use App\Models\Flight;
use App\Models\Aircraft;

class FlightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = Config::first();
        $company = Company::first();
        $flights = Flight::with([
            'aircraft',
            'company',
            'departure_airport',
            'arrival_intended_airport',
            'arrival_actual_airport',
        ])->get();

        return Inertia::render('Flights/Show', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => 'Flights',
            'appTitle' => $config->companyName,
            'company' => $company,
            'flights' => $flights
        ]);
    }

    public function show(Request $request, $id)
    {
        $config = Config::first();
        $company = Company::first();
        $flight = Flight::with([
            'aircraft',
            'company',
            'departure_airport',
            'arrival_intended_airport',
            'arrival_actual_airport',
        ])->where('id', $id)->first();

        return Inertia::render('Flights/Detail', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => 'Flights',
            'appTitle' => $config->companyName,
            'company' => $company,
            'flight' => $flight
        ]);
    }

    public function log(Request $request, $worldSlug, $companyUuId, $identifier)  {
        $company = Company::with([
            'world',
        ])
        ->where('uuid', $companyUuId)
        ->first();

        $aircraft = Aircraft::with([
            'status',
            'aircraft_type',
            'aircraft_type.fuel_type',
            'aircraft_type.aircraft_class',
            'current_airport',
            'rent_airport',
            'flights',
            'company' => function ($query) use($companyUuId) {
                $query->where('uuid', $companyUuId);
            }
        ])
        ->where('identifier', $identifier)
        ->first();

        $flights = Flight::with([
            'departure_airport',
            'arrival_intended_airport',
            'arrival_actual_airport',
        ])->where('aircraft_id', $aircraft->id)
        ->get();

        return Inertia::render('Flights/LogByAircraft', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => 'Flights',
            'pageTitle' => "[$company->airline] $aircraft->identifier Flight Logs",
            'company' => $company,
            'aircraft' => $aircraft,
            'flights' => $flights,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function edit(Flight $flight)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Flight $flight)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function destroy(Flight $flight)
    {
        //
    }
}
