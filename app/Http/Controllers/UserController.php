<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Inertia\Inertia;
use App\Models\User;
use App\Models\Config;

class UserController extends Controller
{
    public function profile()
    {
        $config = Config::first();
        $user = Auth::user();
        return Inertia::Render('Profile', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => 'My Profile',
            'heading' => 'My Profile',
            'appTitle' => $config->companyName,
            'user' => $user,
        ]);
    }

    public function profileUpdate(Request $request)
    {
        $config = Config::first();
        $user = Auth::user();
        $inputs = $request->all();

        $user->update($inputs);

        return redirect()->route('user.profile');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('roles')->get();
        $config = Config::first();
        foreach ($users as $key => $user) {
            $user->roles();
        }

        return Inertia::render('User/List', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => 'All Users',
            'heading' => 'All Users',
            'appTitle' => $config->companyName,
            'users' => $users,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $config = Config::first();
        $roles = Role::all();

        return Inertia::render('User/Create', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => 'Create User',
            'appTitle' => $config->companyName,
            'roles' => $roles
        ]);
    }

    public function check_username(Request $request)
    {
        $isAvailable = false;
        $input = $request->json()->all();
        $username = $input['body']['username'];

        if ($username) {
            $user = User::where('username', $username)->first();

            if (!$user) {
                $isAvailable = true;
            }
        }

        $response = [
            'isAvailable' => $isAvailable
        ];

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('/users/list');
    }
}
