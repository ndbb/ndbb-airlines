<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\Company;
use App\Models\Aircraft;
use App\Models\Employee;
use App\Models\Flight;
use App\Models\Config;

class DashboardController extends Controller
{
    public function index()
    {
        $company = Company::first();
        $numAircraft = Aircraft::all()->count();
        $numEmployees = Employee::all()->count();
        $numFlights = Flight::all()->count();
        $config = Config::first();

        return Inertia::render('Dashboard', [
            'isAdmin' => (Auth::user()) ? Auth::user()->hasRole('admin') : false,
            'pageTitle' => 'Dashboard',
            'heading' => 'Dashboard',
            'appTitle' => $config->companyName,
            'company' => $company,
            'statistics' => [
                'numAircraft' => $numAircraft,
                'numEmployees' => $numEmployees,
                'numFlights' => $numFlights,
            ]
        ]);
    }
}
