<?php

namespace Database\Factories;

use App\Models\Airport;
use Illuminate\Database\Eloquent\Factories\Factory;

class AirportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Airport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => "b8c8301a-3193-4fc3-ac54-c435aa4a38f4",
            'icao' => "KPHX",
            'has_no_runways' => false,
            'time_offset_in_sec' => -25200.0,
            'local_time_open_in_hours_since_midnight' => 3.5,
            'local_time_close_in_hours_since_midnight' => 24.0,
            'iata' => "PHX",
            'name' => "Phoenix Sky Harbor Intl",
            'state' => "Arizona",
            'country_code' => "K2",
            'country_name' => "USA",
            'city' => "Phoenix",
            'latitude' => 33.434277,
            'longitude' => -112.011581,
            'elevation' => 1121.51,
            'has_land_runway' => true,
            'has_water_runway' => false,
            'has_helipad' => false,
            'size' => 5,
            'transition_altitude' => 18000,
            'is_not_in_vanilla_fsx' => false,
            'is_not_in_vanilla_p3d' => false,
            'is_not_in_vanilla_xplane' => false,
            'is_not_in_vanilla_fs2020' => false,
            'is_closed' => false,
            'is_valid' => true,
            'mag_var' => 12.0,
            'is_addon' => false,
            'random_seed' => 777459557,
            'last_random_seed_generation' => "2021-06-30T19:30:02.017",
            'is_military' => false,
            'has_lights' => true,
            'airport_source' => 0,
            'last_very_short_request_date' => "2021-01-04T05:11:27.647",
            'last_small_trip_request_date' => "2021-02-11T06:23:41.207",
            'last_medium_trip_request_date' => "2021-02-11T06:24:36.937",
            'last_short_haul_request_date' => "2021-01-02T21:52:46.857",
            'last_medium_haul_request_date' => "2021-01-04T21:43:25.58",
            'last_long_haul_request_date' => "2020-12-31T04:16:45.48",
            'display_name' => "KPHX (Phoenix Sky Harbor Intl)",
            'utc_time_open_in_hours_since_midnight' => 10.5,
            'utc_time_close_in_hours_since_midnight' => 31.0
        ];
    }
}
