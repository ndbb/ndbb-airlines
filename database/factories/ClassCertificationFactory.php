<?php

namespace Database\Factories;

use App\Models\ClassCertification;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClassCertificationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ClassCertification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => '05996b83-3d4f-4ab4-b10f-ffef47a6dd2d',
            'employee_uuid' => 'a7c09cb5-3e3c-450b-bee2-21852ed92ca4',
            'aircraft_class_uuid' => '602a66ee-62d9-4cd6-95bb-bd03fa782fa8',
            'last_validation' => '2020-08-14T11:00:29.377',
            'comments' => ''
        ];
    }
}
