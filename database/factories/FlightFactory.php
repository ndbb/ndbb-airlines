<?php

namespace Database\Factories;

use App\Models\Flight;
use Illuminate\Database\Eloquent\Factories\Factory;

class FlightFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Flight::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => 'c640a906-2149-4475-848a-63ac7bdf00d6',
            'aircraft_uuid' => 'db8beb44-5ee4-413a-99fe-2d9a0ddbd945',
            'company_uuid' => 'a611948e-8cf8-4c75-9f43-c0388dcc65f8',
            'departure_airport_uuid' => '5b752b5c-93e3-4d0e-bd10-1314974eee9e',
            'arrival_intended_airport_uuid' => 'b8c8301a-3193-4fc3-ac54-c435aa4a38f4',
            'arrival_actual_airport_uuid' => '722599ba-7e96-4453-a5ac-e7d1e1254f01',
            'registered' => true,
            'category' => 0,
            'result_comments' => 'Airframe condition: Normal wear and tear: -0.05%\r\nPilot aircraft usage: -0.03%\r\n : 0.1%\nEngine #1 : 0.1%\nEngine #2 : 0.1%',
            'start_time' => '2021-07-01T02:39:46.08',
            'engine_on_time' => '2021-07-01T02:39:46.2',
            'engine_off_time' => '2021-07-01T03:16:12.627',
            'airborne_time' => '2021-07-01T02:43:16.41',
            'landed_time' => '2021-07-01T03:13:57.543',
            'intended_flight_level' => 200,
            'passengers' => 0,
            'cargo' => 0,
            'added_fuel_qty' => 0,
            'is_ai' => true,
            'vertical_speed_at_touchdown_mps' => 0.0,
            'maxg_force' => 0.0,
            'ming_force' => 0.0,
            'max_bank' => 0.0,
            'max_pitch' => 0.0,
            'has_stalled' => false,
            'has_overspeeded' => false,
            'engine1_status' => 0,
            'engine2_status' => 0,
            'engine3_status' => 0,
            'engine4_status' => 0,
            'engine5_status' => 0,
            'engine6_status' => 0,
            'xp_flight' => 0,
            'xp_flight_bonus' => 0.0,
            'xp_missions' => 6,
            'cargos_total_weight' => 1409.0,
            'pax_count' => 0,
            'aircraft_current_fob' => 662.07019650595691,
            'aircraft_current_altitude' => -1.0,
            'actual_cruise_altitude' => 0.0,
            'actual_consumption_at_cruise_level_in_lbsperhour' => 0.0,
            'actual_total_fuel_consumption_inlbs' => 1709.9108291626242,
            'actual_consumption_at_cruise_level_in_galperhour' => 0.0,
            'actual_total_fuel_consumptioningal' => 0.0,
            'actual_tas_at_cruise_level' => 0.0,
            'actual__cruise_time_in_minutes' => 0.0,
            'actual_pressure_altitude' => 0.0,
            'register_state' => 9,
            'wrong_fuel_detected' => false,
            'wrong_weight_detected' => false,
            'time_offset' => 0.0,
            'can_resume_or_abort' => false,
            'engine_on_real_time' => '2021-07-01T02:39:46.2',
            'engine_off_real_time' => '2021-07-01T03:16:12.627'
        ];
    }
}
