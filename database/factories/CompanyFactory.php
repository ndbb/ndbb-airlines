<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => 'a611948e-8cf8-4c75-9f43-c0388dcc65f8',
            'world_id' => 'ad3ec8a4-246e-4abb-84a9-9dbc43bb6ae6',
            'name' => 'NDBoost Airlines',
            'airline' => 'NDBB',
            'last_connected' => '2021-06-30T22:54:31.117',
            'last_report_date' => '2021-06-29T15:40:41.017',
            'reputation' => 0.48551840922402484,
            'creation_date' => '2021-06-29T15:40:41.017',
            'difficulty_level' => 1,
            'level' => 2,
            'xp' => 354,
            'transport_employee_instant' => true,
            'transport_player_instant' => true,
            'force_time_in_simulator' => true,
            'use_small_airports' => false,
            'use_only_vanilla_airports' => false,
            'enable_skill_tree' => false,
            'checkride_level' => 2,
            'enable_landing_penalities' => true,
            'enable_employees_flight_duty_and_sleep' => true,
            'aircraft_rent_level' => 2,
            'enable_cargos_and_charters_loading_time' => false,
            'in_survival' => false,
            'pay_bonus_factor' => 0.9,
            'enable_sim_failures' => true,
            'disable_seats_config_check' => false,
            'realistic_sim_procedures' => true,
        ];
    }
}
