<?php

namespace Database\Factories;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => "a7c09cb5-3e3c-450b-bee2-21852ed92ca4",
            'psuedo_name' => "Tyyne Timonen",
            'company_uuid' => "a611948e-8cf8-4c75-9f43-c0388dcc65f8",
            'flight_hours_total_before_hiring' => 355.00928501685064,
            'flight_hours_in_company' => 17.073012499999997,
            'weight' => 189.0,
            'birth_date' => "2000-07-18T08:02:06.933",
            'fatigue' => 0.0,
            'punctuality' => 0.55,
            'comfort' => 0.32,
            'happiness' => 100.0,
            'per_flight_hour_wages' => 150.00,
            'weekly_garanted_salary' => 1010.00,
            'per_fligh_thour_salary' => 69.00,
            'category' => 1,
            'status' => 0,
            'last_status_change' => "2021-07-01T16:05:30.317",
            'current_total_flight_hours_in_duty' => 5.6238883333333334,
            'freelance_since' => "2021-06-30T03:19:08.143",
            'freelance_until' => "2021-07-03T03:19:08.143",
            'last_payment_date' => "2021-06-30T03:19:08.143",
            'is_online' => false,
            'flight_hours_grand_total' => 372.08229751685064,
            'home_airport_uuid' => 'b8c8301a-3193-4fc3-ac54-c435aa4a38f4',
            'current_airport_uuid' => 'b8c8301a-3193-4fc3-ac54-c435aa4a38f4',
        ];
    }
}

