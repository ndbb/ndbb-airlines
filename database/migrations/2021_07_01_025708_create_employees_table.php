<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->string('psuedo_name');
            $table->integer('company_id');
            $table->float('flight_hours_total_before_hiring');
            $table->float('flight_hours_in_company');
            $table->float('weight');
            $table->dateTime('birth_date');
            $table->float('fatigue');
            $table->float('punctuality');
            $table->float('comfort');
            $table->float('happiness');
            $table->integer('home_airport_id');
            $table->integer('current_airport_id')->nullable();
            $table->float('per_flight_hour_wages');
            $table->float('weekly_garanted_salary');
            $table->float('per_fligh_thour_salary');
            $table->tinyInteger('category');
            $table->tinyInteger('status');
            $table->dateTime('last_status_change');
            $table->float('current_total_flight_hours_in_duty');
            $table->dateTime('freelance_since')->nullable();
            $table->dateTime('freelance_until')->nullable();
            $table->dateTime('last_payment_date')->nullable();
            $table->boolean('is_online');
            $table->float('flight_hours_grand_total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
