<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->integer('aircraft_id');
            $table->integer('company_id');
            $table->integer('departure_airport_id')->nullable();
            $table->integer('arrival_intended_airport_id')->nullable();
            $table->integer('arrival_actual_airport_id')->nullable();
            $table->boolean('registered');
            $table->integer('category');
            $table->text('result_comments')->nullable();
            $table->dateTime('start_time');
            $table->dateTime('engine_on_time')->nullable();
            $table->dateTime('engine_off_time')->nullable();
            $table->dateTime('airborne_time')->nullable();
            $table->dateTime('landed_time')->nullable();
            $table->integer('intended_flight_level');
            $table->integer('passengers');
            $table->float('cargo');
            $table->float('added_fuel_qty');
            $table->boolean('is_ai');
            $table->float('vertical_speed_at_touchdown_mps');
            $table->float('maxg_force');
            $table->float('ming_force');
            $table->float('max_bank');
            $table->float('max_pitch');
            $table->boolean('has_stalled');
            $table->boolean('has_overspeeded');
            $table->integer('engine1_status');
            $table->integer('engine2_status');
            $table->integer('engine3_status');
            $table->integer('engine4_status');
            $table->integer('engine5_status');
            $table->integer('engine6_status');
            $table->float('xp_flight');
            $table->float('xp_flight_bonus');
            $table->integer('xp_missions');
            $table->float('cargos_total_weight');
            $table->integer('pax_count');
            $table->float('aircraft_current_fob');
            $table->float('aircraft_current_altitude');
            $table->float('actual_cruise_altitude');
            $table->float('actual_consumption_at_cruise_level_in_lbsperhour');
            $table->float('actual_total_fuel_consumption_inlbs');
            $table->float('actual_consumption_at_cruise_level_in_galperhour');
            $table->float('actual_total_fuel_consumptioningal')->nullable();
            $table->float('actual_tas_at_cruise_level');
            $table->float('actual__cruise_time_in_minutes');
            $table->float('actual_pressure_altitude');
            $table->integer('register_state');
            $table->boolean('wrong_fuel_detected');
            $table->boolean('wrong_weight_detected');
            $table->bigInteger('time_offset')->nullable();
            $table->boolean('can_resume_or_abort');
            $table->dateTime('engine_on_real_time')->nullable();
            $table->dateTime('engine_off_real_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flights');
    }
}
