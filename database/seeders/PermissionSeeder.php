<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'owner' => [
                'onair.refreshcompanydetails',
                'onair.refreshcompanyfleet',
                'onair.refreshcompanyemployees',
                'onair.refreshcompanyflights',
                'onair.refreshcompanyfBOs',
                'onair.refreshcompanycashFlow',
            ],
            'manage-users' => [
                'user.list',
                'user.detail',
                'user.edit',
                'user.delete',
                'user.create',
            ],
            'employee' => [
                'flight.list',
                'flight.detail',
                'company.detail',
            ]
        ];

        // create the admin role
        $adminRole = Role::firstOrCreate(['name' => 'admin']);
        $userRole = Role::firstOrCreate([ 'name' => 'user' ]);

        foreach ($roles as $name => $role) {
            $Role = Role::firstOrCreate([ 'name' => $name ], [ 'name' => $name ]);

            foreach ($role as $permission) {
                $Permission = Permission::firstOrCreate([
                    'name' => $permission
                ], [ 'name' => $permission]);

                $Permission->assignRole($Role);
                $Permission->save();
            }
            $Role->save();
        }
    }
}
