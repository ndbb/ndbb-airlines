<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\World;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = [
            // [
            //     'api_key' => 'a7ef356d-c0cc-461e-94cf-941650bd81cd',
            //     'uuid' => 'bdc8e9d3-2ba4-4a32-9f6c-0f63ebe6c5e4',
            //     'world' => 'stratus',
            //     'sync_company' => true,
            //     'sync_employees' => true,
            //     'sync_fbos' => true,
            //     'sync_fleet' => true,
            //     'sync_flights' => true,
            // ],
            // [
            //     'api_key' => '4e7aeb10-8fa7-479f-a981-3ac74b68a807',
            //     'uuid' => '0061b40f-3f44-4788-95c6-1ec896d945cc',
            //     'world' => 'clear-sky',
            //     'sync_company' => false,
            //     'sync_employees' => false,
            //     'sync_fbos' => false,
            //     'sync_fleet' => false,
            //     'sync_flights' => false,
            // ],
            [
                'api_key' => 'd17ea885-aad5-429b-9297-fe2e6deca5d9',
                'uuid' => 'c3d8e51d-f2e9-4918-a286-c3f2cd5ab141',
                'world' => 'cumulus',
                'sync_company' => true,
                'sync_employees' => true,
                'sync_fbos' => true,
                'sync_fleet' => true,
                'sync_flights' => true,
            ],
            // [
            //     'api_key' => '5f049635-9a45-4377-b289-b1c953334631',
            //     'uuid' => '0c266190-5adb-463d-a6ee-3a5b0ee8b23b',
            //     'world' => 'thunder',
            //     'sync_company' => false,
            //     'sync_employees' => false,
            //     'sync_fbos' => false,
            //     'sync_fleet' => false,
            //     'sync_flights' => false,
            // ],
        ];

        $Companies = [];
        foreach ($companies as $key => $c) {
            $world = World::where('slug', $c['world'])->first();

            $company = Company::updateOrCreate([
                'uuid' => $c['uuid']
            ], [
                'api_key' => $c['api_key'],
                'uuid' => $c['uuid'],
                'world_id' => $world->id,
                'sync_company' =>  $c['sync_company'],
                'sync_employees' =>  $c['sync_employees'],
                'sync_fbos' =>  $c['sync_fbos'],
                'sync_fleet' =>  $c['sync_fleet'],
                'sync_flights' =>  $c['sync_flights'],
            ]);

            array_push($Companies, $company);
        }
    }
}
