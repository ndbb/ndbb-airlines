<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Config;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $newConfig = [
            'apiKey' => env('ONAIR_APIKEY'),
            'companyId' => env('ONAIR_COMPANYID'),
            'world' => env('ONAIR_WORLD'),
            'baseURL' => env('ONAIR_BASEURL'),
            'companyName' => env('APP_NAME'),
        ];

        $config = Config::first();

        if (!$config) {
            $config = Config::create($newConfig);
        } else {
            $config->update($newConfig);
            $config->save();
        }
    }
}
