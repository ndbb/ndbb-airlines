<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newUser = [
            'name' => 'Gordon Freeman',
            'username' => 'admin',
            'email' => 'admin@ndboost.com',
            'password' => Hash::make('passw0rd!'),
            'roles' => [
                'admin',
                'owner',
            ]
        ];

        $adminUser = User::firstOrCreate([
            'username' => $newUser['username']
        ], [
            'name' => $newUser['name'],
            'username' => $newUser['username'],
            'email' => $newUser['email'],
            'password' => $newUser['password'],
        ]);

        $adminUser->assignRole($newUser['roles']);
        $adminUser->save();

        $users = User::factory(5)->create();
        foreach ($users as $key => $user) {
            $user->assignRole('user');
            $user->save();
        }


        $employees = User::factory(5)->create();
        foreach ($employees as $key => $user) {
            $user->assignRole('employee');
            $user->save();
        }
    }
}
