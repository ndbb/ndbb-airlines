<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ConfigSeeder::class,
            WorldSeeder::class,
            // AircraftClassSeeder::class,
            // AircraftTypeSeeder::class,
            // AircraftSeeder::class,
            // AirportSeeder::class,
            CompanySeeder::class,
            // EmployeeSeeder::class,
            // ClassCertificationSeeder::class,
            // FlightSeeder::class,
            AircraftStatusSeeder::class,
            FuelTypeSeeder::class,
            PermissionSeeder::class,
            UserSeeder::class,
        ]);
    }
}
